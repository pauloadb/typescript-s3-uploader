import React, { useEffect, useMemo } from 'react';
import { LoginCallback, Security } from '@okta/okta-react';

import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js';

import { ToastContainer } from 'react-toastify';

import {
  Col, Container, Row,
} from 'react-bootstrap';

import { Route, Routes } from 'react-router-dom';
import HomePage from './pages/Home';
import NotFoundPage from './pages/NotFound';

import TopMenu from './components/TopMenu';

import config from './config';

import './App.css';
import 'react-toastify/dist/ReactToastify.css';

import ManageFiles from './pages/ManageFiles';

function App() {
  const oktaAuth = useMemo(() => new OktaAuth(config.okta), []);

  const restoreOriginalUri = async (_oktaAuth: OktaAuth, originalUri: string) => {
    window.location.replace(
      toRelativeUrl(originalUri || '/', window.location.origin),
    );
  };

  const initiateLogin = () => {
    oktaAuth.signInWithRedirect();
  };

  useEffect(() => {
    oktaAuth.start();
    return () => {
      oktaAuth.stop();
    };
  }, [oktaAuth]);

  return (
    <Security oktaAuth={oktaAuth} restoreOriginalUri={restoreOriginalUri}>
      <Container fluid className="noPadding noMargin g-0">
        <Row className="g-0">
          <Col className="noMargin">
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable={false}
              pauseOnHover
            />
            <TopMenu />
          </Col>
        </Row>
        <Row className="g-0">
          <Col className="noMargin noPadding">
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/login/callback" element={<LoginCallback />} />
              <Route path="/login/initiate" action={initiateLogin} />
              <Route path="/files" element={<ManageFiles />} />
              <Route path="*" element={<NotFoundPage />} />
            </Routes>
          </Col>
        </Row>
      </Container>
    </Security>
  );
}

export default App;
