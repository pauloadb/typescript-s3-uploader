const apiBaseUrl = process.env.REACT_APP_API_BASE_URL;
if (!apiBaseUrl) throw new Error('API URL is missing in the configuration');

const domain = process.env.REACT_APP_OKTA_DOMAIN;
if (!domain) throw new Error('Okta domain is missing in the configuration');

const clientId = process.env.REACT_APP_OKTA_CLIENT_ID;
if (!clientId) throw new Error('Okta client ID is missing in the configuration');

const config = {
  okta: {
    clientId,
    issuer: `https://${process.env.REACT_APP_OKTA_DOMAIN}/oauth2/default`,
    redirectUri: `${window.location.origin}/login/callback`,
    scopes: ['openid', 'email', 'offline_access'],
    pkce: true,
    disableHttpsCheck: process.env.REACT_APP_OKTA_TESTING_DISABLEHTTPSCHECK || false,
    services: {
      autoRenew: true,
      autoRemove: true,
      syncStorage: true,
    },
  },
  apiBaseUrl,
};

export default config;
