import React, { useState } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { useOktaAuth } from '@okta/okta-react';
import { toast } from 'react-toastify';
import UploadFileRequest from '../../api/UploadFileRequest';

type UploadFileFormProps = {
  formId: string,
  onUploadFile: () => void
};

const UploadFileForm: React.FC<UploadFileFormProps> = function UploadFileForm(
  { formId, onUploadFile },
) {
  const [sourceUrl, setSourceUrl] = useState<string | undefined>(undefined);
  const [fileName, setFileName] = useState<string | undefined>(undefined);
  const [validated, setValidated] = useState(false);
  const { oktaAuth } = useOktaAuth();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const triggerUploadFile = async () => {
    if (!await oktaAuth?.isAuthenticated()) return;

    try {
      const token = await oktaAuth.getAccessToken();
      if (!token || !sourceUrl) return;
      await new UploadFileRequest({ token, sourceUrl, fileName }).execute();

      toast('File upload triggered successfully', { position: 'top-right', draggable: false, type: 'success' });
      onUploadFile();
    } catch (err) {
      toast('Failed to trigger file upload', { position: 'top-right', draggable: false, type: 'error' });
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const form = event.currentTarget;
    if (form.checkValidity() === true && !isSubmitting) {
      setIsSubmitting(true);
      triggerUploadFile();
    }
    event.preventDefault();
    event.stopPropagation();
    setValidated(true);
  };

  return (
    <Form id={formId} noValidate validated={validated} onSubmit={handleSubmit}>
      <Row>
        <Form.Group className="mb-3" as={Col} controlId="validationSourceUrl">
          <Form.Label>Source URL</Form.Label>
          <Form.Control
            required
            type="text"
            name="sourceUrl"
            onChange={(event) => setSourceUrl(event.target.value)}
          />
          <Form.Control.Feedback type="invalid">
            Please type a valid source URL
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Row>
        <Form.Group className="mb-3" as={Col} controlId="validationFileName">
          <Form.Label>File name (Optional)</Form.Label>
          <Form.Control
            type="text"
            name="fileName"
            onChange={(event) => setFileName(event.target.value)}
          />
        </Form.Group>
      </Row>
    </Form>
  );
};

export default UploadFileForm;
