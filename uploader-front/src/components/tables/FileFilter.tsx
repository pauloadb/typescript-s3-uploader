import React from 'react';

import {
  ButtonGroup, Col, Form, FormControl, Row,
} from 'react-bootstrap';

import {
  MdAddCircle, MdDelete, MdDone, MdError, MdIncompleteCircle,
} from 'react-icons/md';
import FileStatus from '../../model/FileStatus';

import TooltipToggleButton from '../TooltipToggleButton';

export type StatusChangeHandler = (status: FileStatus | 'all', selected: boolean) => void;
export type FilterChangeHandler = (newFilter: string) => void;

export type FileStatusFilterProps = {
  onChangeStatus: StatusChangeHandler,
  onChangeFilter: FilterChangeHandler,
  selectedStatuses: Set<FileStatus>,
};

const FileFilter: React.FC<FileStatusFilterProps> = function FileStatusFilter(
  { onChangeStatus, onChangeFilter, selectedStatuses },
) {
  const isAllChecked = selectedStatuses.size === Object.values(FileStatus).length;
  const isCreatedChecked = selectedStatuses.has(FileStatus.CREATED);
  const isDeletedChecked = selectedStatuses.has(FileStatus.DELETED);
  const isFailedChecked = selectedStatuses.has(FileStatus.FAILED);
  const isUploadedChecked = selectedStatuses.has(FileStatus.UPLOADED);
  const isUploadingChecked = selectedStatuses.has(FileStatus.UPLOADING);

  return (
    <Form onSubmit={(event) => event.preventDefault()}>
      <Row>
        <Col xs={4} style={{ display: 'flex', justifyContent: 'center' }}>
          <FormControl
            placeholder="Type your search"
            aria-label="Filter"
            onChange={(event) => onChangeFilter(event.target.value)}
          />
        </Col>
        <Col style={{ display: 'flex', justifyContent: 'center' }}>
          <ButtonGroup>
            <TooltipToggleButton
              checked={selectedStatuses.size === Object.values(FileStatus).length}
              onClick={() => onChangeStatus('all', !isAllChecked)}
              id="all"
            >
              All
            </TooltipToggleButton>
            <TooltipToggleButton
              checked={isCreatedChecked}
              onClick={() => onChangeStatus(FileStatus.CREATED, !isCreatedChecked)}
              id={FileStatus.CREATED.toString()}
            >
              <MdAddCircle />
            </TooltipToggleButton>
            <TooltipToggleButton
              checked={isUploadingChecked}
              onClick={() => onChangeStatus(FileStatus.UPLOADING, !isUploadingChecked)}
              id={FileStatus.UPLOADING.toString()}
            >
              <MdIncompleteCircle />
            </TooltipToggleButton>
            <TooltipToggleButton
              checked={isUploadedChecked}
              onClick={() => onChangeStatus(FileStatus.UPLOADED, !isUploadedChecked)}
              id={FileStatus.UPLOADED.toString()}
            >
              <MdDone />
            </TooltipToggleButton>
            <TooltipToggleButton
              checked={isFailedChecked}
              onClick={() => onChangeStatus(FileStatus.FAILED, !isFailedChecked)}
              id={FileStatus.FAILED.toString()}
            >
              <MdError />
            </TooltipToggleButton>
            <TooltipToggleButton
              checked={isDeletedChecked}
              onClick={() => onChangeStatus(FileStatus.DELETED, !isDeletedChecked)}
              id={FileStatus.DELETED.toString()}
            >
              <MdDelete />
            </TooltipToggleButton>
          </ButtonGroup>
        </Col>
      </Row>
    </Form>

  );
};

export default FileFilter;
