import React from 'react';
import {
  Button,
  Col, Container, Row,
} from 'react-bootstrap';
import FileStatus from '../../model/FileStatus';

import PageNavigator, { PageChangeHandler } from '../pagination/PageNavigator';
import PageSizeSelector, { PageSizeChangeHandler } from '../pagination/PageSizeSelector';
import FileFilter, { FilterChangeHandler, StatusChangeHandler } from './FileFilter';

type FileTableToolbarProps = {
  onChangePage: PageChangeHandler,
  onChangePageSize: PageSizeChangeHandler,
  onChangeStatus: StatusChangeHandler,
  onChangeFilter: FilterChangeHandler,
  onClickUpload: () => void,
  pageNumber: number,
  pageSize: number,
  lastPage: number,
  selectedStatuses: Set<FileStatus>,
};

const FileTableToolbar: React.FC<FileTableToolbarProps> = function FileTableToolbar({
  onChangePage,
  onChangePageSize,
  onChangeStatus,
  onChangeFilter,
  onClickUpload,
  pageSize,
  pageNumber,
  lastPage,
  selectedStatuses,
}) {
  return (
    <Container>
      <Row style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Col xs={6}>
          <FileFilter
            selectedStatuses={selectedStatuses}
            onChangeStatus={onChangeStatus}
            onChangeFilter={onChangeFilter}
          />
        </Col>
        <Col>
          <PageNavigator
            pageNumber={pageNumber}
            lastPage={lastPage}
            onChangePage={onChangePage}
          />
        </Col>
        <Col xs={1}>
          <Button variant="dark" onClick={onClickUpload} id="upload-file-action">
            Upload
          </Button>
        </Col>
        <Col xs={3}>
          <PageSizeSelector
            pageSize={pageSize}
            onChange={onChangePageSize}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default FileTableToolbar;
