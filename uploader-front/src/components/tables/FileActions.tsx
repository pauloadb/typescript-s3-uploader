import { useOktaAuth } from '@okta/okta-react';
import React, { useState } from 'react';
import { MdDelete, MdDownload } from 'react-icons/md';
import { toast } from 'react-toastify';
import DeleteFileRequest from '../../api/DeleteFileRequest';
import DownloadFileRequest from '../../api/DownloadFileRequest';
import File from '../../model/File';
import FileStatus from '../../model/FileStatus';
import CustomTooltip from '../CustomTooltip';
import LoadingModal from '../modals/LoadingModal';

export type FileActionsProps = { file: File, onDelete: () => void };

const FileActions: React.FC<FileActionsProps> = function FileActions({ file, onDelete }) {
  const [isLoading, setIsLoading] = useState(false);
  const { oktaAuth } = useOktaAuth();

  const onDownloadFile = async () => {
    try {
      setIsLoading(true);
      if (!await oktaAuth.isAuthenticated()) return;
      const token = await oktaAuth.getAccessToken();
      if (!token) throw new Error('Failed to get a token');
      const result = await new DownloadFileRequest({ token, fileId: file.id }).execute();
      if (!result) throw new Error('Received empty response from server');

      const link = document.createElement('a');
      link.href = result.url;
      document.body.appendChild(link);
      link.click();
      link.parentNode?.removeChild(link);
    } catch (err) {
      toast('Failed to download file', { position: 'top-right', draggable: false, type: 'error' });
    } finally {
      setIsLoading(false);
    }
  };

  const onDeleteFile = async () => {
    try {
      setIsLoading(true);
      if (!await oktaAuth.isAuthenticated()) return;
      const token = await oktaAuth.getAccessToken();
      if (!token) throw new Error('Failed to get a token');
      await new DeleteFileRequest({ token, fileId: file.id }).execute();
      toast('Deleted file successfully', { position: 'top-right', draggable: false, type: 'success' });
      onDelete();
    } catch (err) {
      toast('Failed to delete file', { position: 'top-right', draggable: false, type: 'error' });
    } finally {
      setIsLoading(false);
    }
  };

  if (file.status === FileStatus.UPLOADED) {
    return (
      <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
        <CustomTooltip id={`${file.id}-download-tooltip`} text="Download">
          <div><MdDownload size="1.5em" onClick={() => onDownloadFile()} style={{ cursor: 'pointer' }} /></div>
        </CustomTooltip>
        <CustomTooltip id={`${file.id}-delete-tooltip`} text="Delete">
          <div><MdDelete size="1.5em" onClick={() => onDeleteFile()} style={{ cursor: 'pointer' }} /></div>
        </CustomTooltip>
        <LoadingModal show={isLoading} message="Loading..." />
      </div>
    );
  }

  return null;
};

export default FileActions;
