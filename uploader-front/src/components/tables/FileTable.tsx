import React from 'react';
import { Table } from 'react-bootstrap';
import moment from 'moment-timezone';

import SortableHeader from './SortableHeader';
import FileActions from './FileActions';
import File from '../../model/File';
import { SortInfo } from '../../model/SortInfo';

type ChangeSortingHandler = (sortInfo: SortInfo) => void;

type FileTableProps = {
  sorting: SortInfo,
  files: Array<File>,
  onChangeSorting: ChangeSortingHandler,
  onDelete: () => void
};

const FileTable: React.FC<FileTableProps> = function FileTable(
  {
    sorting, files, onChangeSorting, onDelete,
  },
) {
  return (
    <Table striped bordered hover responsive>
      <thead>
        <tr>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="id">ID</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="name">Name</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="insertedAt">Inserted At</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="deletedAt">Deleted At</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="status">Status</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="transferStartedAt">Started At</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="transferFinishedAt">Finished At</SortableHeader>
          <SortableHeader onClick={onChangeSorting} sorting={sorting} fieldName="transferFailedAt">Failed At</SortableHeader>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {files.length > 0
          ? files.map((file) => (
            <tr key={file.id}>
              <td>{file.id}</td>
              <td>{file.name}</td>
              <td>{moment(file.insertedAt).tz('UTC').format('YYYY-MM-DD')}</td>
              <td>{file.deletedAt ? moment(file.deletedAt).format('YYYY-MM-DD HH:mm') : null}</td>
              <td>{file.status}</td>
              <td>{file.transferStartedAt ? moment(file.transferStartedAt).format('YYYY-MM-DD HH:mm') : null}</td>
              <td>{file.transferFinishedAt ? moment(file.transferFinishedAt).format('YYYY-MM-DD HH:mm') : null}</td>
              <td>{file.transferFailedAt ? moment(file.transferFailedAt).format('YYYY-MM-DD HH:mm') : null}</td>
              <td>
                <FileActions file={file} onDelete={onDelete} />
              </td>
            </tr>
          )) : (
            <tr>
              <td colSpan={9}>No records.</td>
            </tr>
          )}
      </tbody>
    </Table>
  );
};

export default FileTable;
