import React, { ReactNode } from 'react';
import { MdSort, MdSortByAlpha } from 'react-icons/md';
import { SortInfo } from '../../model/SortInfo';

export type SortableHeaderProps = {
  fieldName: string,
  sorting: SortInfo,
  children: ReactNode,
  onClick: (input: SortInfo) => void,
  disabled?: boolean,
};

const SortableHeader: React.FC<SortableHeaderProps> = function SortableHeader({
  disabled = false,
  children,
  sorting,
  fieldName,
  onClick,
}) {
  const onClickHeader = () => {
    if (sorting.sortBy === fieldName) {
      onClick({ sortBy: fieldName, sortDesc: !sorting.sortDesc });
    } else {
      onClick({ sortBy: fieldName, sortDesc: false });
    }
  };

  const selected = !disabled && fieldName === sorting?.sortBy;

  const iconStyle = { marginLeft: '0.5rem', transform: '' };
  if (selected && !sorting?.sortDesc) {
    iconStyle.transform = 'scaleY(-1)';
  }

  const headerStyle = disabled ? undefined : { cursor: 'pointer' };

  let icon = null;
  if (!disabled) {
    icon = selected
      ? (<MdSort size={20} style={iconStyle} />)
      : (<MdSortByAlpha size={20} style={iconStyle} />);
  }

  return (
    <th onClick={onClickHeader} style={headerStyle}>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <span>{children}</span>
        {icon}
      </div>
    </th>
  );
};

SortableHeader.defaultProps = {
  disabled: false,
};

export default SortableHeader;
