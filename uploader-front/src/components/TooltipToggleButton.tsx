import React, { ReactNode } from 'react';
import { ToggleButton } from 'react-bootstrap';
import CustomTooltip from './CustomTooltip';

export type TooltipToggleButtonProps = {
  id: string,
  checked?: boolean,
  children?: ReactNode,
  onClick: () => void,
};

const TooltipToggleButton: React.FC<TooltipToggleButtonProps> = function TooltipToggleButton({
  id, checked, children, onClick,
}) {
  return (
    <CustomTooltip id={`${id}-tooltip`} text={id}>
      <ToggleButton
        className="mb-2"
        id={id}
        key={id}
        type="checkbox"
        checked={checked}
        variant="outline-dark"
        value={id}
        onClick={onClick}
      >
        {children}
      </ToggleButton>
    </CustomTooltip>

  );
};

TooltipToggleButton.defaultProps = {
  checked: false,
  children: undefined,
};

export default TooltipToggleButton;
