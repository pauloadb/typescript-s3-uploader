import React, { ReactElement } from 'react';
import { OverlayTrigger, Tooltip, TooltipProps } from 'react-bootstrap';

export type CustomTooltipProps = {
  id: string,
  text: string,
  children: ReactElement,
};

const CustomTooltip: React.FC<CustomTooltipProps> = function CustomTooltip({ id, text, children }) {
  const renderTooltip = (
    tooltipProps: JSX.IntrinsicAttributes & TooltipProps & React.RefAttributes<HTMLDivElement>,
  ) => (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Tooltip id={id} {...tooltipProps}>
      {text}
    </Tooltip>
  );

  return (
    <OverlayTrigger
      placement="top"
      overlay={renderTooltip}
    >
      {children}
    </OverlayTrigger>
  );
};

export default CustomTooltip;
