import React, { ReactNode } from 'react';
import {
  Button, Modal,
} from 'react-bootstrap';

type FormModalProps = {
  show?: boolean,
  size?: 'sm' | 'lg',
  title: string,
  onHide: () => void,
  children?: ReactNode,
  readOnly?: boolean,
  formId?: string,
};

const FormModal: React.FC<FormModalProps> = function FormModal(
  {
    show, size, title, onHide, children, readOnly, formId,
  },
) {
  return (
    <Modal show={show} onHide={onHide} size={size} centered>
      <Modal.Header closeButton>
        <Modal.Title>
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {children}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" type="button" onClick={onHide}>
          Close
        </Button>
        {readOnly ? null : (
          <Button variant="dark" type="submit" form={formId}>
            Save
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
};

FormModal.defaultProps = {
  show: false,
  size: undefined,
  children: undefined,
  readOnly: false,
  formId: undefined,
};

export default FormModal;
