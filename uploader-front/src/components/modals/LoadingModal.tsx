import React from 'react';
import { Modal, Spinner } from 'react-bootstrap';

import './LoadingModal.css';

type LoadingModalProps = {
  show?: boolean,
  message: string,
};

const LoadingModal: React.FC<LoadingModalProps> = function LoadingModal({ show, message }) {
  return (
    <Modal show={show} size="sm" centered className="loading-modal">
      <Modal.Body className="text-center">
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <Spinner
            as="span"
            animation="grow"
            size="sm"
            role="status"
            aria-hidden="true"
            variant="light"
            style={{ verticalAlign: 'middle' }}
          />
          <span className="text-white" style={{ verticalAlign: 'middle', paddingLeft: '1em' }}><b>{message}</b></span>
        </div>
      </Modal.Body>
    </Modal>
  );
};

LoadingModal.defaultProps = {
  show: false,
};

export default LoadingModal;
