import React from 'react';

import FormModal from './FormModal';
import UploadFileForm from '../forms/UploadFileForm';

type UploadFileModalProps = {
  show: boolean,
  onUploadFile: () => void,
  onCancel: () => void
};

const UploadFileModal: React.FC<UploadFileModalProps> = function UploadFileModal(
  { show, onUploadFile, onCancel },
) {
  return (
    <FormModal
      show={show}
      title="Upload File"
      onHide={() => { onCancel(); }}
      formId="upload-file-form"
    >
      <UploadFileForm formId="upload-file-form" onUploadFile={onUploadFile} />
    </FormModal>
  );
};

export default UploadFileModal;
