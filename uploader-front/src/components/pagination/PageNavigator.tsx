import React from 'react';

import { Pagination } from 'react-bootstrap';

import './PageNavigator.css';

export type PageChangeHandler = (pageNumber: number) => void;

export type PaginationBarProps = {
  pageNumber: number,
  lastPage: number,
  onChangePage: PageChangeHandler,
};

const PaginationBar: React.FC<PaginationBarProps> = function PaginationBar(props) {
  const { pageNumber, lastPage, onChangePage } = props;

  const renderFirst = () => {
    if (pageNumber > 1) {
      return (<Pagination.First onClick={() => onChangePage(1)} />);
    }
    return null;
  };

  const renderPrevious = () => {
    if (pageNumber > 1) {
      return (<Pagination.Prev onClick={() => onChangePage(pageNumber - 1)} />);
    }
    return null;
  };

  const renderNext = () => {
    if (pageNumber < lastPage) {
      return (<Pagination.Next onClick={() => onChangePage(pageNumber + 1)} />);
    }
    return null;
  };

  const renderLast = () => {
    if (pageNumber < lastPage) {
      return (<Pagination.Last onClick={() => onChangePage(lastPage)} />);
    }
    return null;
  };

  return (
    <Pagination style={{ justifyContent: 'center' }}>
      {renderFirst()}
      {renderPrevious()}
      <Pagination.Item>
        {pageNumber}
        &nbsp;of&nbsp;
        {lastPage}
      </Pagination.Item>
      {renderNext()}
      {renderLast()}
    </Pagination>
  );
};

export default PaginationBar;
