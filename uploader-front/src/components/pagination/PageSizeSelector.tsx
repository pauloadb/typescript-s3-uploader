import React from 'react';
import { Form, InputGroup } from 'react-bootstrap';

export type PageSizeChangeHandler = (pageSize: number) => void;

export type PageSizeSelectorProps = {
  onChange: PageSizeChangeHandler,
  pageSize: number,
};

const PageSizeSelector: React.FC<PageSizeSelectorProps> = function PageSizeSelector(props) {
  const { onChange, pageSize } = props;
  return (
    <InputGroup className="mb-3">
      <InputGroup.Text style={{ borderColor: '#212529', backgroundColor: '#212529', color: '#f8f9fa' }}>
        Results Per Page
      </InputGroup.Text>
      <Form.Select
        required
        onChange={(event) => { onChange(Number.parseInt(event.target.value, 10)); }}
        defaultValue={pageSize}
      >
        <option>10</option>
        <option>25</option>
        <option>50</option>
      </Form.Select>
    </InputGroup>
  );
};

export default PageSizeSelector;
