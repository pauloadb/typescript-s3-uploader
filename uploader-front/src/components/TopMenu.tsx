import React from 'react';
import { useOktaAuth } from '@okta/okta-react';
import {
  Container, Nav, Navbar, NavDropdown,
} from 'react-bootstrap';

function TopMenu() {
  const { oktaAuth, authState } = useOktaAuth();

  const logout = async () => {
    oktaAuth.tokenManager.clear();
    window.location.pathname = '/';
  };

  return (
    <Navbar collapseOnSelect bg="dark" variant="dark" style={{ paddingLeft: '15px', paddingRight: '15px' }} expand="md">
      <Container fluid>
        <Navbar.Brand>
          <img
            src="/logo192.png"
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="Typescript S3 uploader logo"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          {authState?.isAuthenticated ? (
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: '100px' }}
              navbarScroll
            >
              <Nav.Link active={window.location.pathname === '/files'} href="/files">Files</Nav.Link>
            </Nav>
          ) : null}
          <Nav>
            {authState?.isAuthenticated ? (
              <NavDropdown title={authState?.idToken?.claims?.email} id="navbarScrollingDropdown">
                <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
              </NavDropdown>
            ) : (
              <Nav.Link onClick={() => oktaAuth.signInWithRedirect()}>Login</Nav.Link>
            )}

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default TopMenu;
