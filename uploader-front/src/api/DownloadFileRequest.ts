import config from '../config';
import HttpRequest from './HttpRequest';

type DownloadFileInput = { fileId: number, token: string };
type DownloadFileOutput = { url: string };

export default class DownloadFileRequest
  extends HttpRequest<DownloadFileOutput> {
  constructor(input: DownloadFileInput) {
    super({
      method: 'GET',
      token: input.token,
      url: `${config.apiBaseUrl}/files/${input.fileId}/download`,
      errorMessage: 'Failed to download file',
    });
  }
}
