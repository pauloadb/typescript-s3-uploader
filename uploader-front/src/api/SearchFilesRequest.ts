import config from '../config';
import File from '../model/File';
import FileStatus from '../model/FileStatus';
import { SortInfo } from '../model/SortInfo';
import HttpRequest from './HttpRequest';

type SearchFilesInput = {
  statuses: Set<FileStatus>,
  sorting: SortInfo,
  pageNumber: number,
  pageSize: number,
  token: string,
  filter: string,
};

type SearchFilesOutput = {
  list: File[],
  total: number,
  pageNumber: number,
  pageSize: number,
  lastPage: number,
};

export default class SearchFilesRequest
  extends HttpRequest<SearchFilesOutput> {
  constructor(input: SearchFilesInput) {
    const urlParams = new URLSearchParams({
      pageNumber: `${input.pageNumber}`,
      pageSize: `${input.pageSize}`,
      filter: input.filter || '',
      sortBy: input.sorting.sortBy,
      sortOrder: input.sorting.sortDesc ? 'desc' : 'asc',
    });

    let statusesParam = '';
    input.statuses.forEach((status) => { statusesParam = `${statusesParam}&statuses[]=${status}`; });
    const url = `${config.apiBaseUrl}/files?${urlParams}${statusesParam}`;

    super({
      method: 'GET', token: input.token, url, errorMessage: 'Failed to search for files',
    });
  }
}
