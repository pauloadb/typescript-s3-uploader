export default class ApiError extends Error {
  public readonly status: number;

  public readonly data: object;

  constructor(message: string, { data, status }: { data: object, status: number }) {
    super(message);
    this.data = data;
    this.status = status;
  }
}
