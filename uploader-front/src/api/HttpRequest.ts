import ApiError from './ApiError';

export type HttpMethod = 'GET' | 'POST' | 'DELETE';

export type HttpRequestInput = {
  url: string,
  token: string,
  errorMessage?: string,
  method: HttpMethod,
  body?: object
};

export default abstract class HttpRequest<Output> {
  constructor(protected input: HttpRequestInput) {}

  public async execute(): Promise<Output | null> {
    const options = {
      method: this.input.method,
      headers: {
        Authorization: `Bearer ${this.input.token}`,
        ...(this.input.body ? ({ 'Content-Type': 'application/json' }) : {}),
      },
      ...(this.input.body ? ({ body: JSON.stringify(this.input.body) }) : {}),
    };

    const response = await fetch(`${this.input.url}`, options);

    if (response.status === 204) {
      return null;
    }

    if (response.status !== 200) {
      const data = await response.json();
      throw new ApiError(
        this.input.errorMessage || `Failed to execute ${this.input.method} for URL [${this.input.url}]`,
        { data, status: response.status },
      );
    }

    return response.json() as Output;
  }
}
