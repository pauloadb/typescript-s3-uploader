import config from '../config';
import HttpRequest from './HttpRequest';

type DeleteFileInput = { fileId: number, token: string };

export default class DeleteFileRequest
  extends HttpRequest<void> {
  constructor(input: DeleteFileInput) {
    super({
      method: 'DELETE',
      token: input.token,
      url: `${config.apiBaseUrl}/files/${input.fileId}`,
      errorMessage: 'Failed to delete file',
    });
  }
}
