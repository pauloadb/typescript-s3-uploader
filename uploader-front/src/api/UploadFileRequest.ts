import config from '../config';
import File from '../model/File';
import HttpRequest from './HttpRequest';

type UploadFileInput = {
  token: string,
  sourceUrl: string,
  fileName?: string,
};

export default class UploadFileRequest
  extends HttpRequest<File> {
  constructor(input: UploadFileInput) {
    super({
      method: 'POST',
      token: input.token,
      url: `${config.apiBaseUrl}/files`,
      body: { sourceUrl: input.sourceUrl, fileName: input.fileName },
      errorMessage: 'Failed to upload file',
    });
  }
}
