import FileStatus from './FileStatus';

export default interface File {
  id: number;
  name: string;
  insertedAt: Date;
  deletedAt: Date;
  status: FileStatus;
  transferErrorsQty: number;
  transferStartedAt: Date;
  transferFinishedAt: Date;
  transferFailedAt: Date;
}
