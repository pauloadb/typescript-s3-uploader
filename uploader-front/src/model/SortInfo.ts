export type SortInfo = {
  sortBy: string,
  sortDesc: boolean
};
