enum FileStatus {
  CREATED = 'CREATED',
  DELETED = 'DELETED',
  FAILED = 'FAILED',
  UPLOADED = 'UPLOADED',
  UPLOADING = 'UPLOADING',
}

export default FileStatus;
