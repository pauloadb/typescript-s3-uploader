import { useOktaAuth } from '@okta/okta-react';
import React, { useEffect, useState } from 'react';
import { Badge, Container, Row } from 'react-bootstrap';
import { toast } from 'react-toastify';
import SearchFilesRequest from '../api/SearchFilesRequest';
import UploadFileModal from '../components/modals/UploadFileModal';
import { StatusChangeHandler } from '../components/tables/FileFilter';
import FileTable from '../components/tables/FileTable';
import FileTableToolbar from '../components/tables/FileTableToolbar';
import File from '../model/File';
import FileStatus from '../model/FileStatus';

const ALL_STATUSES = [
  FileStatus.CREATED,
  FileStatus.DELETED,
  FileStatus.FAILED,
  FileStatus.UPLOADED,
  FileStatus.UPLOADING,
];

function ManageFiles() {
  const [sorting, setSorting] = useState({ sortBy: 'id', sortDesc: true });
  const [files, setFiles] = useState(new Array<File>());
  const [selectedStatuses, setSelectedStatuses] = useState(new Set<FileStatus>(ALL_STATUSES));
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [lastPage, setLastPage] = useState(0);
  const [showUploadModal, setShowUploadModal] = useState(false);
  const [filter, setFilter] = useState('');

  const { oktaAuth } = useOktaAuth();

  const onChangeStatus: StatusChangeHandler = (type, isChecked) => {
    if (type === 'all') {
      setSelectedStatuses(isChecked ? new Set<FileStatus>(ALL_STATUSES) : new Set<FileStatus>());
    } else {
      const newSet = new Set(selectedStatuses.values());
      if (isChecked) {
        newSet.add(type);
      } else {
        newSet.delete(type);
      }
      setSelectedStatuses(newSet);
    }
    setPageNumber(1);
  };

  const onChangeFilter = (newFilter: string) => {
    setFilter(newFilter);
    setPageNumber(1);
  };

  const reloadFiles = async () => {
    if (await oktaAuth?.isAuthenticated()) {
      try {
        const token = await oktaAuth.getAccessToken();
        if (!token) throw new Error('Failed to get a token');

        const data = await new SearchFilesRequest({
          statuses: selectedStatuses,
          sorting,
          pageNumber,
          pageSize,
          token,
          filter,
        }).execute();

        if (!data) throw new Error('Receive empty response from server');

        setFiles(data?.list);
        setLastPage(data.lastPage);
      } catch (err) {
        toast('Failed to search for files', { position: 'top-right', draggable: false, type: 'error' });
      }
    }
  };

  const onUploadFile = async () => {
    setShowUploadModal(false);
    if (pageNumber === 1) {
      await reloadFiles();
    } else setPageNumber(1);
  };

  useEffect(() => {
    reloadFiles();
  }, [sorting, pageNumber, pageSize, selectedStatuses, filter]);

  return (
    <Container style={{ paddingTop: '4em', paddingBottom: '2em' }}>
      <Row>
        {' '}
        <h2 className="text-center">
          <Badge bg="dark">Uploaded Files</Badge>
        </h2>
      </Row>
      <Row>
        <FileTableToolbar
          pageNumber={pageNumber}
          pageSize={pageSize}
          lastPage={lastPage}
          onChangePage={setPageNumber}
          onChangePageSize={setPageSize}
          onChangeStatus={onChangeStatus}
          onChangeFilter={onChangeFilter}
          selectedStatuses={selectedStatuses}
          onClickUpload={() => setShowUploadModal(true)}
        />
      </Row>
      <Row>
        <FileTable
          files={files}
          sorting={sorting}
          onChangeSorting={setSorting}
          onDelete={() => { reloadFiles(); }}
        />
        <UploadFileModal
          show={showUploadModal}
          onCancel={() => { setShowUploadModal(false); }}
          onUploadFile={() => onUploadFile()}
        />
      </Row>
    </Container>
  );
}

export default ManageFiles;
