import React from 'react';

import {
  Badge, Button, Container, Image, Row,
} from 'react-bootstrap';

import { useOktaAuth } from '@okta/okta-react';

import './Home.css';
import { Navigate } from 'react-router-dom';

function Home() {
  const { authState, oktaAuth } = useOktaAuth();

  const login = () => oktaAuth.signInWithRedirect();

  if (authState?.isAuthenticated) {
    return (
      <Navigate to="/files" />
    );
  }

  return (
    <Container
      style={{
        paddingLeft: '0px', paddingRight: '0px', paddingTop: '100px', paddingBottom: '50px',
      }}
      fluid
      className="text-center noMargin"
    >
      <Row className="g-0">
        <Container>
          <Image src="/logo192.png" />
        </Container>
        <h2>
          <Badge bg="dark">Typescript S3 Uploader</Badge>
        </h2>
        <br />
        <h4>
          Upload files from a source URL to a S3 bucket
        </h4>
        <br />
      </Row>
      <Row className="g-0">
        <Container>
          <Button size="lg" variant="success" onClick={login}>Login with Okta</Button>
        </Container>
      </Row>
    </Container>
  );
}

export default Home;
