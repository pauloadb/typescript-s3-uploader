import React from 'react';
import './NotFound.css';

function NotFound() {
  return (
    <div className="not-found">
      <h2>Page not found</h2>
      <h1>
        <span>4</span>
        <span>0</span>
        <span>4</span>
      </h1>
      <h3>We are sorry, but the page you requested was not found</h3>
    </div>
  );
}

export default NotFound;
