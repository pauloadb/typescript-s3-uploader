import GetUrlExecutor from './commands/GetUrlExecutor';
import Operation from './IOperation';
import GetUrlInput from './IGetUrlInput';

export default class GetUrlOperation implements Operation<GetUrlInput, URL> {
  constructor(public input: GetUrlInput) {}

  execute(): Promise<URL> {
    return new GetUrlExecutor(this.input).execute();
  }
}
