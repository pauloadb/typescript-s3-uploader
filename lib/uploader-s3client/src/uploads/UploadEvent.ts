enum UploadEvent {
  COMPLETED = 'completed',
  ERROR = 'error',
}

export type UploadEventRecord = Record<UploadEvent, unknown>;

export default UploadEvent;
