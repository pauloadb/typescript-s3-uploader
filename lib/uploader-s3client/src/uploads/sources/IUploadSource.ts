export default interface UploadSource<Input, Output> {
  sourceUrl: URL,
  contentLength: number,

  download(input: Input): Promise<Output>;
}
