import ByteRangedHttpSource from './ranged/ByteRangedHttpSource';
import StreamedHttpSource from './streamed/StreamedHttpSource';
import SourceFactory from './ISourceFactory';
import UploadInput from '../../IUploadInput';
import HttpClient from '../../adapters/HttpClient';

export default class HttpSourceFactory
implements SourceFactory<ByteRangedHttpSource | StreamedHttpSource> {
  private httpClient = HttpClient.getInstance();

  constructor(private uploadInput: UploadInput) {}

  async createSource(): Promise<ByteRangedHttpSource | StreamedHttpSource> {
    const { sourceUrl, partSize } = this.uploadInput;
    const headers = await this.httpClient.getHeaders(sourceUrl);

    const attrName = 'content-length';
    if (typeof headers[attrName] !== 'string' || !/^\s*\d+\s*$/.test(headers[attrName])) {
      throw new Error('Content length of the file is invalid or could not be found');
    }

    const contentLength = Number.parseInt(headers[attrName], 10);

    const acceptRanges = headers['accept-ranges'] === 'bytes';
    if (acceptRanges) {
      return new ByteRangedHttpSource(sourceUrl, contentLength, partSize);
    }

    return new StreamedHttpSource(sourceUrl, contentLength);
  }
}
