export default interface SourceFactory<SourceType> {
  createSource(): Promise<SourceType>;
}
