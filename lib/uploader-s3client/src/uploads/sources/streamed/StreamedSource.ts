import UploadSource from '../IUploadSource';

export default abstract class StreamedSource implements UploadSource<void, ReadableStream> {
  constructor(public sourceUrl: URL, public contentLength: number) {}

  abstract download(): Promise<ReadableStream<Uint8Array>>;
}
