import HttpClient from '../../../adapters/HttpClient';
import StreamedSource from './StreamedSource';

export default class StreamedHttpSource extends StreamedSource {
  private httpClient = HttpClient.getInstance();

  download(): Promise<ReadableStream<Uint8Array>> {
    return this.httpClient.download(this.sourceUrl, 'stream');
  }
}
