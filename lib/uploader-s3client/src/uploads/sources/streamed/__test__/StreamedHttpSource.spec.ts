describe('the streamed http source', () => {
  const mockHttpClient = { download: jest.fn() };
  const MockHttpClient = { getInstance: () => mockHttpClient };

  beforeEach(() => {
    jest.mock('../../../../adapters/HttpClient', () => MockHttpClient);
  });

  it('must properly download from the source', () => import('../StreamedHttpSource')
    .then(async ({ default: StreamedHttpSource }) => {
      const url = new URL('http://fake.url/fake.file');
      const source = new StreamedHttpSource(url, 123);
      await source.download();
      expect(mockHttpClient.download).toHaveBeenCalledWith(url, 'stream');
    }));
});
