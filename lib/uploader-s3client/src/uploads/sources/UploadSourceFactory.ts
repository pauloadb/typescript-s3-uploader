import UploadInput from '../../IUploadInput';
import HttpSourceFactory from './HttpSourceFactory';
import ByteRangedSource from './ranged/ByteRangedSource';
import SourceFactory from './ISourceFactory';
import StreamedSource from './streamed/StreamedSource';

export default class UploadSourceFactory
implements SourceFactory<ByteRangedSource | StreamedSource> {
  constructor(private uploadInput: UploadInput) {}

  async createSource(): Promise<ByteRangedSource | StreamedSource> {
    let uploadSource: ByteRangedSource | StreamedSource;

    if (['http:', 'https:'].includes(this.uploadInput.sourceUrl.protocol.toLowerCase())) {
      uploadSource = await new HttpSourceFactory(this.uploadInput)
        .createSource();
    } else {
      throw new Error('The source URL protocol is not supported');
    }

    return uploadSource;
  }
}
