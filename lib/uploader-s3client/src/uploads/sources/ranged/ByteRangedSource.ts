import UploadSource from '../IUploadSource';

export default abstract class ByteRangedSource implements UploadSource<number, Buffer> {
  public totalParts: number;

  constructor(public sourceUrl: URL, public contentLength: number, private partSize: number) {
    this.totalParts = Math.max(Math.ceil(this.contentLength / this.partSize), 1);
  }

  async download(partNumber: number): Promise<Buffer> {
    const remaining = this.contentLength - ((partNumber - 1) * this.partSize);
    const size = partNumber === this.totalParts ? remaining : this.partSize;

    const rangeStart = (partNumber - 1) * this.partSize;
    const rangeEnd = rangeStart + size - 1;

    return this.downloadRange(rangeStart, rangeEnd);
  }

  protected abstract downloadRange(rangeStart: number, rangeEnd: number): Promise<Buffer>;
}
