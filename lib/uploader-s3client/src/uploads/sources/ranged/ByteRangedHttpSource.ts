import HttpClient from '../../../adapters/HttpClient';
import ByteRangedSource from './ByteRangedSource';

export default class ByteRangedHttpSource extends ByteRangedSource {
  private httpClient = HttpClient.getInstance();

  protected async downloadRange(rangeStart: number, rangeEnd: number) : Promise<Buffer> {
    return this.httpClient.download(
      this.sourceUrl,
      'arraybuffer',
      { Range: `bytes=${rangeStart}-${rangeEnd}` },
    );
  }
}
