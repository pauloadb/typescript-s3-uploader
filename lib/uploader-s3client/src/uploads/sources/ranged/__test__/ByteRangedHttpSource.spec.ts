describe('the byte ranged http source', () => {
  const mockHttpClient = { download: jest.fn() };
  const MockHttpClient = { getInstance: () => mockHttpClient };

  beforeEach(() => {
    jest.mock('../../../../adapters/HttpClient', () => MockHttpClient);
  });

  it('must properly download the requested byte range', () => import('../ByteRangedHttpSource')
    .then(async ({ default: ByteRangedHttpSource }) => {
      class TestRangedHttpSource extends ByteRangedHttpSource {
        public async downloadRange(rangeStart: number, rangeEnd: number) : Promise<Buffer> {
          return super.downloadRange(rangeStart, rangeEnd);
        }
      }

      const url = new URL('http://fake.url/fake.file');
      const source = new TestRangedHttpSource(url, 10, 3);
      await source.downloadRange(0, 2);
      expect(mockHttpClient.download).toHaveBeenCalledWith(url, 'arraybuffer', { Range: 'bytes=0-2' });
    }));
});
