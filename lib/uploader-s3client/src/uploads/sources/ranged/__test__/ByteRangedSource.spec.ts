import ByteRangedSource from '../ByteRangedSource';

class TestByteRangedSource extends ByteRangedSource {
  public downloadRange = jest.fn();
}

describe('the byte ranged source', () => {
  const url = new URL('http://fake.url/fake.file');

  it.each([
    [10, 3, 4],
    [1, 2, 1],
  ])('must compute the total number of parts when size is %s and part size is %s', async (size, partSize, total) => {
    const source = new TestByteRangedSource(url, size, partSize);
    expect(source.totalParts).toBe(total);
  });

  it('must properly download a part', async () => {
    const source = new TestByteRangedSource(url, 5, 3);
    await source.download(1);
    expect(source.downloadRange).toHaveBeenCalledWith(0, 2);
    await source.download(2);
    expect(source.downloadRange).toHaveBeenCalledWith(3, 4);
  });
});
