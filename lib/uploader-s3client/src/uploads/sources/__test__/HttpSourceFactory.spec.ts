describe('the http source factory', () => {
  const INPUT = {
    bucketName: 'fake-bucket',
    key: 'fake-key',
    partSize: 10,
    queueSize: 5,
    sourceUrl: new URL('http://fake.url/fake.file'),
  };

  const mockHttpClient = { getHeaders: jest.fn() };
  const MockHttpClient = { getInstance: () => mockHttpClient };

  const MockRangedHttpSource = jest.fn();
  const mockRangedHttpSource = { id: 'ranged-http-source' };

  const MockStreamedHttpSource = jest.fn();
  const mockStreamedHttpSource = { id: 'streamed-http-source' };

  beforeEach(() => {
    jest.mock('../../../adapters/HttpClient', () => MockHttpClient);

    MockRangedHttpSource.mockReturnValue(mockRangedHttpSource);
    jest.mock('../ranged/ByteRangedHttpSource', () => MockRangedHttpSource);

    MockStreamedHttpSource.mockReturnValue(mockStreamedHttpSource);
    jest.mock('../streamed/StreamedHttpSource', () => MockStreamedHttpSource);
  });

  it.each([
    ['not found', {}],
    ['invalid', { 'content-length': 'invalid' }],
  ])('must throw an error if the content length header is %s', (_, headers) => import('../HttpSourceFactory')
    .then(async ({ default: HttpSourceFactory }) => {
      mockHttpClient.getHeaders.mockResolvedValue(headers);
      try {
        await new HttpSourceFactory(INPUT).createSource();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new Error('Content length of the file is invalid or could not be found'));
      }
    }));

  it('must create a byte ranged http source if the server supports ranges', () => import('../HttpSourceFactory')
    .then(async ({ default: HttpSourceFactory }) => {
      mockHttpClient.getHeaders.mockResolvedValue({ 'content-length': '100', 'accept-ranges': 'bytes' });
      const source = await new HttpSourceFactory(INPUT).createSource();
      expect(source).toEqual(mockRangedHttpSource);
      expect(MockRangedHttpSource).toHaveBeenCalledWith(INPUT.sourceUrl, 100, INPUT.partSize);
    }));

  it('must create a streamed http source if the server supports ranges', () => import('../HttpSourceFactory')
    .then(async ({ default: HttpSourceFactory }) => {
      mockHttpClient.getHeaders.mockResolvedValue({ 'content-length': '100' });
      const source = await new HttpSourceFactory(INPUT).createSource();
      expect(source).toEqual(mockStreamedHttpSource);
      expect(MockStreamedHttpSource).toHaveBeenCalledWith(INPUT.sourceUrl, 100);
    }));
});
