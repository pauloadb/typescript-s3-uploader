describe('the upload source factory', () => {
  const BASE_INPUT = {
    bucketName: 'fake-bucket',
    key: 'fake-key',
    partSize: 10,
    queueSize: 5,
  };

  const mockHttpSourceFactory = { createSource: jest.fn(() => ({ id: 'http-source' })) };
  const MockHttpSourceFactory = jest.fn();

  beforeEach(() => {
    MockHttpSourceFactory.mockReturnValue(mockHttpSourceFactory);
    jest.mock('../HttpSourceFactory', () => MockHttpSourceFactory);
  });

  it('must throw an error if the source protocol is not supported', () => import('../UploadSourceFactory')
    .then(async ({ default: UploadSourceFactory }) => {
      try {
        await new UploadSourceFactory({
          bucketName: 'fake-bucket',
          key: 'fake-key',
          partSize: 10,
          queueSize: 5,
          sourceUrl: new URL('ppp://fake.url/fake.file'),
        }).createSource();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new Error('The source URL protocol is not supported'));
      }
    }));

  it('must delegate the creation to the http source factory if the protocol is compatible', () => import('../UploadSourceFactory')
    .then(async ({ default: UploadSourceFactory }) => {
      const input = { ...BASE_INPUT, sourceUrl: new URL('https://fake.url/fake.file') };
      const source = await new UploadSourceFactory(input).createSource();
      expect(source).toStrictEqual({ id: 'http-source' });
      expect(MockHttpSourceFactory).toHaveBeenCalledWith(input);
    }));
});
