import { CompletedPart } from '@aws-sdk/client-s3';
import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import CompleteUploadExecutor from '../../commands/CompleteUploadExecutor';
import { UploadEventRecord } from '../UploadEvent';
import UploadInput from '../../IUploadInput';
import AbortingState from './AbortingState';
import CompletedState from './CompletedState';

export default class CompletingState implements DaemonState<UploadEventRecord> {
  constructor(
    public daemon: DaemonStateContext<UploadEventRecord>,
    private uploadInput: UploadInput,
    private uploadId: string,
    private completedParts: CompletedPart[],
  ) {}

  async execute(): Promise<void> {
    try {
      await new CompleteUploadExecutor(this.uploadInput)
        .execute({ uploadId: this.uploadId, parts: this.completedParts });
      this.daemon.changeState(new CompletedState(this.daemon));
    } catch (err) {
      this.daemon.changeState(
        new AbortingState(this.daemon, this.uploadInput, this.uploadId, getError(err)),
      );
    }
  }
}
