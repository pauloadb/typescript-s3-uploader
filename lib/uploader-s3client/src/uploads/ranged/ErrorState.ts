/* eslint-disable max-classes-per-file */
import { DaemonState, DaemonStateContext } from 'uploader-utils';
import UploadEvent, { UploadEventRecord } from '../UploadEvent';

export default class ErrorState implements DaemonState<UploadEventRecord> {
  constructor(
    public daemon: DaemonStateContext<UploadEventRecord>,
    private error: Error,
  ) {}

  async execute(): Promise<void> {
    this.daemon.stop();
    this.daemon.emit(UploadEvent.ERROR, this.error);
  }
}
