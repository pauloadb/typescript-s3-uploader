import { CompletedPart } from '@aws-sdk/client-s3';
import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import UploadPartExecutor from '../../commands/UploadPartExecutor';
import ByteRangedSource from '../sources/ranged/ByteRangedSource';
import { UploadEventRecord } from '../UploadEvent';
import UploadInput from '../../IUploadInput';
import AbortingState from './AbortingState';
import CompletingState from './CompletingState';

export default class UploadingPartsState implements DaemonState<UploadEventRecord> {
  private uploadExecutor: UploadPartExecutor;

  private runningCount = 0;

  private partQueue: number[] = [];

  private completedParts: CompletedPart[] = [];

  constructor(
    public daemon: DaemonStateContext<UploadEventRecord>,
    private uploadInput: UploadInput,
    private uploadSource: ByteRangedSource,
    private uploadId: string,
  ) {
    this.uploadExecutor = new UploadPartExecutor(this.uploadInput);
    for (let partNumber = 1; partNumber <= this.uploadSource.totalParts; partNumber += 1) {
      this.partQueue.push(partNumber);
    }
  }

  private async transfer(partNumber: number): Promise<void> {
    const logger = this.uploadInput.logger?.child({ key: this.uploadInput.key, partNumber });
    try {
      logger?.info('Dowloading part from source');
      const data = await this.uploadSource.download(partNumber);
      logger?.info('Successfuly downloaded part from source');
      const eTag = await this.uploadExecutor
        .execute({ body: data, partNumber, uploadId: this.uploadId });
      this.completedParts.push({ ETag: eTag, PartNumber: partNumber });
      this.daemon.changeState(this);
    } finally {
      this.runningCount -= 1;
    }
  }

  async execute(): Promise<void> {
    if (this.runningCount === 0 && this.partQueue.length === 0) {
      this.daemon.changeState(
        new CompletingState(this.daemon, this.uploadInput, this.uploadId, this.completedParts),
      );
      return;
    }

    const promises = [];
    while (this.runningCount < this.uploadInput.queueSize && this.partQueue.length) {
      const partNumber = this.partQueue.shift();
      if (partNumber) {
        promises.push(this.transfer(partNumber));
        this.runningCount += 1;
      }
    }

    try {
      await Promise.all(promises);
    } catch (err) {
      this.daemon.changeState(
        new AbortingState(this.daemon, this.uploadInput, this.uploadId, getError(err)),
      );
    }
  }
}
