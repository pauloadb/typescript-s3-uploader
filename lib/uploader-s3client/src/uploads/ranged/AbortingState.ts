import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import AbortUploadExecutor from '../../commands/AbortUploadExecutor';
import { UploadEventRecord } from '../UploadEvent';
import UploadInput from '../../IUploadInput';

export default class AbortingState implements DaemonState<UploadEventRecord> {
  constructor(
    public daemon: DaemonStateContext<UploadEventRecord>,
    private input: UploadInput,
    private uploadId: string,
    private error: Error,
  ) {}

  async execute(): Promise<void> {
    try {
      await new AbortUploadExecutor(this.input).execute({ uploadId: this.uploadId });
    } finally {
      this.daemon.changeState(this.daemon.createErrorState(getError(this.error)));
    }
  }
}
