import ByteRangedSource from '../../sources/ranged/ByteRangedSource';
import mockUploadInput from './mockUploadInput';

class MockRangedSource extends ByteRangedSource { protected downloadRange = jest.fn(); }

describe('the ranged upload daemon', () => {
  const SOURCE = new MockRangedSource(
    new URL(mockUploadInput.sourceUrl),
    100000,
    mockUploadInput.partSize,
  );

  const MockInitialState = jest.fn();
  const mockInitialState = { id: 'fake-initial-state' };

  const MockErrorState = jest.fn();
  const mockErrorState = { id: 'fake-error-state' };

  beforeEach(() => {
    jest.resetAllMocks();
    MockInitialState.mockReturnValue(mockInitialState);
    jest.mock('../InitialState', () => MockInitialState);

    MockErrorState.mockReturnValue(mockErrorState);
    jest.mock('../ErrorState', () => MockErrorState);
  });

  it('must properly create the initial state', () => import('../RangedUploadDaemon').then(({ default: RangedUploadDaemon }) => {
    const daemon = new RangedUploadDaemon(SOURCE, mockUploadInput);
    const state = daemon.createInitialState();
    expect(MockInitialState).toHaveBeenCalledWith(daemon, mockUploadInput, SOURCE);
    expect(state).toStrictEqual(mockInitialState);
  }));

  it('must properly create the error state', () => import('../RangedUploadDaemon').then(({ default: RangedUploadDaemon }) => {
    const error = new Error('Whoops');
    const daemon = new RangedUploadDaemon(SOURCE, mockUploadInput);
    const state = daemon.createErrorState(error);
    expect(MockErrorState).toHaveBeenCalledWith(daemon, error);
    expect(state).toStrictEqual(mockErrorState);
  }));
});
