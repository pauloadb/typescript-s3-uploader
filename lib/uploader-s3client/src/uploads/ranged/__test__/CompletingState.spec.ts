import mockContext from './mockContext';
import mockUploadInput from './mockUploadInput';

describe('the completing state', () => {
  const PARTS = [{ ETag: 'abc' }, { ETag: 'cde' }];

  const MockCompleUploadExecutor = jest.fn();
  const mockCompleUploadExecutor = { execute: jest.fn() };

  const MockCompletedState = jest.fn();
  const mockCompletedState = { id: 'fake-completed-state' };

  const MockAbortingState = jest.fn();
  const mockAbortingState = { id: 'fake-aborting-state' };

  beforeEach(() => {
    mockContext.createErrorState.mockReturnValue({ id: 'fake-error-state' });
    MockCompleUploadExecutor.mockReturnValue(mockCompleUploadExecutor);
    jest.mock('../../../commands/CompleteUploadExecutor', () => MockCompleUploadExecutor);

    MockCompletedState.mockReturnValue(mockCompletedState);
    jest.mock('../CompletedState', () => MockCompletedState);

    MockAbortingState.mockReturnValue(mockAbortingState);
    jest.mock('../AbortingState', () => MockAbortingState);
  });

  it('must properly create the complete upload executor', () => import('../CompletingState').then(async ({ default: CompletingState }) => {
    await new CompletingState(mockContext, mockUploadInput, 'xyz', PARTS).execute();
    expect(MockCompleUploadExecutor).toHaveBeenCalledWith(mockUploadInput);
  }));

  it('must properly execute the complete upload executor', () => import('../CompletingState').then(async ({ default: CompletingState }) => {
    await new CompletingState(mockContext, mockUploadInput, 'xyz', PARTS).execute();
    expect(mockCompleUploadExecutor.execute).toHaveBeenCalledWith({ uploadId: 'xyz', parts: PARTS });
  }));

  it('must change the context state to completed in case of success', () => import('../CompletingState').then(async ({ default: CompletingState }) => {
    await new CompletingState(mockContext, mockUploadInput, 'xyz', PARTS).execute();
    expect(mockContext.changeState).toHaveBeenCalledWith(mockCompletedState);
    expect(MockCompletedState).toHaveBeenCalledWith(mockContext);
  }));

  it('must properly change the state to aborting in case of failure', () => import('../CompletingState').then(async ({ default: CompletingState }) => {
    const error = new Error('Whoops');
    mockCompleUploadExecutor.execute.mockRejectedValue(error);
    await new CompletingState(mockContext, mockUploadInput, 'xyz', PARTS).execute();
    expect(mockContext.changeState).toHaveBeenCalledWith(mockAbortingState);
    expect(MockAbortingState).toHaveBeenCalledWith(mockContext, mockUploadInput, 'xyz', error);
  }));
});
