import ByteRangedSource from '../../sources/ranged/ByteRangedSource';
import mockContext from './mockContext';
import mockUploadInput from './mockUploadInput';

class MockRangedSource extends ByteRangedSource {
  public downloadRange = jest.fn();

  public download = jest.fn();
}

describe('the uploading parts state', () => {
  const INPUT = { ...mockUploadInput, queueSize: 2, partSize: 1 * 1024 * 1024 };

  const SOURCE = new MockRangedSource(
    new URL(mockUploadInput.sourceUrl),
    5 * 1024 * 1024,
    INPUT.partSize,
  );

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  const MockUploadExecutor = jest.fn();
  const mockUploadExecutor = { execute: jest.fn() };

  const MockCompletingState = jest.fn();
  const mockCompletingState = { id: 'fake-completing-state' };

  const MockAbortingState = jest.fn();
  const mockAbortingState = { id: 'fake-aborting-state' };

  beforeEach(() => {
    mockLogger.child.mockReturnValue(mockLogger);

    MockUploadExecutor.mockReturnValue(mockUploadExecutor);
    jest.mock('../../../commands/UploadPartExecutor', () => MockUploadExecutor);

    MockCompletingState.mockReturnValue(mockCompletingState);
    jest.mock('../CompletingState', () => MockCompletingState);

    MockAbortingState.mockReturnValue(mockAbortingState);
    jest.mock('../AbortingState', () => MockAbortingState);
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must change the state to aborting if some transfer error happens', () => import('../UploadingPartsState')
      .then(async ({ default: UploadingPartsState }) => {
        const input = { ...INPUT, logger };
        const error = new Error('Whoops');
        SOURCE.download.mockRejectedValue(error);
        await new UploadingPartsState(mockContext, input, SOURCE, 'xyz').execute();
        expect(mockContext.changeState).toHaveBeenCalledWith(mockAbortingState);
        expect(mockContext.changeState).toHaveBeenCalledTimes(1);
        expect(MockAbortingState).toHaveBeenCalledWith(mockContext, input, 'xyz', error);
      }));

    it('must transfer at most queue size parts at a time', () => import('../UploadingPartsState')
      .then(async ({ default: UploadingPartsState }) => {
        SOURCE.download.mockImplementation(async (partNumber: number) => Buffer.from(`partnumber-${partNumber}`));
        mockUploadExecutor.execute.mockImplementation(async (partNumber: number) => `etag-partnumber-${partNumber}`);
        const state = new UploadingPartsState(mockContext, { ...INPUT, logger }, SOURCE, 'xyz');
        await state.execute();
        expect(SOURCE.download).toHaveBeenCalledWith(1);
        expect(SOURCE.download).toHaveBeenCalledWith(2);
        expect(SOURCE.download).toHaveBeenCalledTimes(2);
        await state.execute();
        expect(SOURCE.download).toHaveBeenCalledWith(3);
        expect(SOURCE.download).toHaveBeenCalledWith(4);
        expect(SOURCE.download).toHaveBeenCalledTimes(4);
        await state.execute();
        expect(SOURCE.download).toHaveBeenCalledWith(5);
        expect(SOURCE.download).toHaveBeenCalledTimes(5);
      }));

    it('must change the state to completing if all parts transfers succeeds', () => import('../UploadingPartsState')
      .then(async ({ default: UploadingPartsState }) => {
        SOURCE.download.mockImplementation(async (partNumber: number) => Buffer.from(`partnumber-${partNumber}`));
        mockUploadExecutor.execute.mockImplementation(async ({ partNumber }: { partNumber: number }) => `etag-partnumber-${partNumber}`);
        const state = new UploadingPartsState(mockContext, { ...INPUT, logger }, SOURCE, 'xyz');
        await state.execute();
        await state.execute();
        await state.execute();
        await state.execute();
        expect(mockContext.changeState).toHaveBeenCalledWith(mockCompletingState);
        expect(MockCompletingState).toHaveBeenCalledWith(mockContext, { ...INPUT, logger }, 'xyz', [
          { ETag: 'etag-partnumber-1', PartNumber: 1 },
          { ETag: 'etag-partnumber-2', PartNumber: 2 },
          { ETag: 'etag-partnumber-3', PartNumber: 3 },
          { ETag: 'etag-partnumber-4', PartNumber: 4 },
          { ETag: 'etag-partnumber-5', PartNumber: 5 },
        ]);
      }));
  });
});
