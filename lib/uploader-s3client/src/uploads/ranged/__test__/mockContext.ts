const mockContext = {
  changeState: jest.fn(),
  createInitialState: jest.fn(),
  createErrorState: jest.fn(),
  emit: jest.fn(),
  off: jest.fn(),
  on: jest.fn(),
  start: jest.fn(),
  stop: jest.fn(),
  isRunning: jest.fn(),
};

export default mockContext;
