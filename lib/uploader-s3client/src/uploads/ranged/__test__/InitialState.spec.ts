import ByteRangedSource from '../../sources/ranged/ByteRangedSource';
import mockUploadInput from './mockUploadInput';
import mockContext from './mockContext';

class MockRangedSource extends ByteRangedSource { protected downloadRange = jest.fn(); }

describe('the initial state', () => {
  const SOURCE = new MockRangedSource(new URL('http://fake.url/fake.file'), 123, 456);

  const MockCreateUploadExecutor = jest.fn();
  const mockCreateUploadExecutor = { execute: jest.fn() };

  const MockUploadingState = jest.fn();
  const mockUploadingState = { id: 'fake-uploading-state' };

  const mockErrorState = { id: 'fake-aborting-state' };

  beforeEach(() => {
    mockContext.createErrorState.mockReturnValue(mockErrorState);

    MockCreateUploadExecutor.mockReturnValue(mockCreateUploadExecutor);
    jest.mock('../../../commands/CreateUploadExecutor', () => MockCreateUploadExecutor);

    MockUploadingState.mockReturnValue(mockUploadingState);
    jest.mock('../UploadingPartsState', () => MockUploadingState);
  });

  it('must properly create the create upload executor', () => import('../InitialState').then(async ({ default: InitialState }) => {
    await new InitialState(mockContext, mockUploadInput, SOURCE).execute();
    expect(MockCreateUploadExecutor).toHaveBeenCalledWith(mockUploadInput);
  }));

  it('must properly execute the complete upload executor', () => import('../InitialState').then(async ({ default: InitialState }) => {
    await new InitialState(mockContext, mockUploadInput, SOURCE).execute();
    expect(mockCreateUploadExecutor.execute).toHaveBeenCalled();
  }));

  it('must change the context state to uploading parts in case of success', () => import('../InitialState').then(async ({ default: InitialState }) => {
    mockCreateUploadExecutor.execute.mockResolvedValue('abc123');
    await new InitialState(mockContext, mockUploadInput, SOURCE).execute();
    expect(mockContext.changeState).toHaveBeenCalledWith(mockUploadingState);
    expect(MockUploadingState).toHaveBeenCalledWith(mockContext, mockUploadInput, SOURCE, 'abc123');
  }));

  it('must properly change the state to error in case of failure', () => import('../InitialState').then(async ({ default: InitialState }) => {
    const error = new Error('Whoops');
    mockCreateUploadExecutor.execute.mockRejectedValue(error);
    await new InitialState(mockContext, mockUploadInput, SOURCE).execute();
    expect(mockContext.changeState).toHaveBeenCalledWith(mockErrorState);
    expect(mockContext.createErrorState).toHaveBeenCalledWith(error);
  }));
});
