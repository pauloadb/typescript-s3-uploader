import UploadEvent from '../../UploadEvent';
import mockContext from './mockContext';

describe('the completed state', () => {
  it('must stop the daemon', () => import('../CompletedState').then(async ({ default: CompletedState }) => {
    await new CompletedState(mockContext).execute();
    expect(mockContext.stop).toHaveBeenCalled();
  }));

  it('must emit a completed event', () => import('../CompletedState').then(async ({ default: CompletedState }) => {
    await new CompletedState(mockContext).execute();
    expect(mockContext.emit).toHaveBeenCalledWith(UploadEvent.COMPLETED);
  }));
});
