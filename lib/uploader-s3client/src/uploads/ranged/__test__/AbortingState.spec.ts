import mockContext from './mockContext';
import mockUploadInput from './mockUploadInput';

describe('the aborting state', () => {
  const MockAbortUploadExecutor = jest.fn();
  const mockAbortUploadExecutor = { execute: jest.fn() };

  beforeEach(() => {
    mockContext.createErrorState.mockReturnValue({ id: 'fake-error-state' });
    MockAbortUploadExecutor.mockReturnValue(mockAbortUploadExecutor);
    jest.mock('../../../commands/AbortUploadExecutor', () => MockAbortUploadExecutor);
  });

  it('must properly create the abort upload executor', () => import('../AbortingState').then(async ({ default: AbortingState }) => {
    await new AbortingState(mockContext, mockUploadInput, 'xyz', new Error('Whoops')).execute();
    expect(MockAbortUploadExecutor).toHaveBeenCalledWith(mockUploadInput);
  }));

  it('must properly execute the abort upload executor', () => import('../AbortingState').then(async ({ default: AbortingState }) => {
    await new AbortingState(mockContext, mockUploadInput, 'xyz', new Error('Whoops')).execute();
    expect(mockAbortUploadExecutor.execute).toHaveBeenCalledWith({ uploadId: 'xyz' });
  }));

  it('must change the context state to error', () => import('../AbortingState').then(async ({ default: AbortingState }) => {
    await new AbortingState(mockContext, mockUploadInput, 'xyz', new Error('Whoops')).execute();
    expect(mockContext.changeState).toHaveBeenCalledWith({ id: 'fake-error-state' });
  }));

  it('must properly create the error state', () => import('../AbortingState').then(async ({ default: AbortingState }) => {
    await new AbortingState(mockContext, mockUploadInput, 'xyz', new Error('Whoops')).execute();
    expect(mockContext.createErrorState).toHaveBeenCalledWith(new Error('Whoops'));
  }));
});
