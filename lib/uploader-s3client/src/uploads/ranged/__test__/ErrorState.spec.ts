import UploadEvent from '../../UploadEvent';
import mockContext from './mockContext';

describe('the error state', () => {
  it('must stop the daemon', () => import('../ErrorState').then(async ({ default: ErrorState }) => {
    await new ErrorState(mockContext, new Error('Whoops')).execute();
    expect(mockContext.stop).toHaveBeenCalled();
  }));

  it('must emit a error event', () => import('../ErrorState').then(async ({ default: ErrorState }) => {
    const error = new Error('Whoops');
    await new ErrorState(mockContext, error).execute();
    expect(mockContext.emit).toHaveBeenCalledWith(UploadEvent.ERROR, error);
  }));
});
