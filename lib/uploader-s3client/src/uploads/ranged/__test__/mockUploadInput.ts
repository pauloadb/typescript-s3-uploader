import UploadInput from '../../../IUploadInput';

const mockUploadInput: UploadInput = {
  sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
  queueSize: 10,
  partSize: 50 * 1024 * 1024,
  key: 'fake-key',
  bucketName: 'fake-bucket',
};

export default mockUploadInput;
