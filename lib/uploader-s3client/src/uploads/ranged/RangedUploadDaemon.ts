import { DaemonState, DaemonStateContext, StateBasedDaemon } from 'uploader-utils';
import ByteRangedSource from '../sources/ranged/ByteRangedSource';
import { UploadEventRecord } from '../UploadEvent';
import UploadInput from '../../IUploadInput';
import ErrorState from './ErrorState';
import InitialState from './InitialState';

export default class RangedUploadDaemon<T extends ByteRangedSource>
  extends StateBasedDaemon<UploadEventRecord>
  implements DaemonStateContext<UploadEventRecord> {
  constructor(private uploadSource: T, private input: UploadInput) {
    super();
  }

  createInitialState(): DaemonState<UploadEventRecord> {
    return new InitialState(this, this.input, this.uploadSource);
  }

  createErrorState(error: Error): DaemonState<UploadEventRecord> {
    return new ErrorState(this, error);
  }
}
