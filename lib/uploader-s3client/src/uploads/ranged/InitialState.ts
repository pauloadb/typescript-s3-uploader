import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import CreateUploadExecutor from '../../commands/CreateUploadExecutor';
import ByteRangedSource from '../sources/ranged/ByteRangedSource';
import { UploadEventRecord } from '../UploadEvent';
import UploadInput from '../../IUploadInput';
import UploadingPartsState from './UploadingPartsState';

export default class InitialState implements DaemonState<UploadEventRecord> {
  constructor(
    public daemon: DaemonStateContext<UploadEventRecord>,
    private uploadInput: UploadInput,
    private uploadSource: ByteRangedSource,
  ) {
  }

  async execute(): Promise<void> {
    try {
      const uploadId = await new CreateUploadExecutor(this.uploadInput).execute();
      this.daemon.changeState(new UploadingPartsState(
        this.daemon,
        this.uploadInput,
        this.uploadSource,
        uploadId,
      ));
    } catch (err) {
      this.daemon.changeState(this.daemon.createErrorState(getError(err)));
    }
  }
}
