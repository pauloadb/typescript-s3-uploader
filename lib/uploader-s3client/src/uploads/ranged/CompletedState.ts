/* eslint-disable max-classes-per-file */
import { DaemonState, DaemonStateContext } from 'uploader-utils';
import UploadEvent, { UploadEventRecord } from '../UploadEvent';

export default class CompletedState implements DaemonState<UploadEventRecord> {
  constructor(public daemon: DaemonStateContext<UploadEventRecord>) {}

  async execute(): Promise<void> {
    this.daemon.stop();
    this.daemon.emit(UploadEvent.COMPLETED);
  }
}
