import UploadExecutor from './commands/UploadExecutor';
import Operation from './IOperation';
import RangedUploadDaemon from './uploads/ranged/RangedUploadDaemon';
import ByteRangedSource from './uploads/sources/ranged/ByteRangedSource';
import StreamedSource from './uploads/sources/streamed/StreamedSource';
import UploadSourceFactory from './uploads/sources/UploadSourceFactory';

import UploadEvent from './uploads/UploadEvent';
import UploadInput from './IUploadInput';

export default class UploadOperation implements Operation<UploadInput, void> {
  constructor(public input: UploadInput) {}

  async execute(): Promise<void> {
    const uploadSource = await new UploadSourceFactory(this.input).createSource();
    if (uploadSource instanceof ByteRangedSource) {
      await new Promise<void>((resolve, reject) => {
        const daemon = new RangedUploadDaemon(uploadSource, this.input);
        daemon.on(UploadEvent.COMPLETED, () => { resolve(); });
        daemon.on(UploadEvent.ERROR, (err) => {
          reject(err);
        });
        daemon.start();
      });
    } else if (uploadSource instanceof StreamedSource) {
      const readableStream = await uploadSource.download();
      await new UploadExecutor(this.input).execute(readableStream);
    }
  }
}
