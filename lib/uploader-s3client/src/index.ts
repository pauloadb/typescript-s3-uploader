import UploadInput from './IUploadInput';
import UploadOperation from './UploadOperation';
import GetUrlInput from './IGetUrlInput';
import GetUrlOperation from './GetUrlOperation';
import OperationInput from './IOperationInput';
import DeleteFileOperation from './DeleteFileOperation';

export {
  UploadInput,
  UploadOperation,
  GetUrlInput,
  GetUrlOperation,
  OperationInput,
  DeleteFileOperation,
};
