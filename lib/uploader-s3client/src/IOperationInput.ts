import Logger from './adapters/ILogger';

export default interface OperationInput {
  key: string,
  bucketName: string,
  endpointUrl?: string,
  logger?: Logger,
}
