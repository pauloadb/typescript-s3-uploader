import { AbortMultipartUploadCommand } from '@aws-sdk/client-s3';
import MyS3Client from '../adapters/MyS3Client';
import UploadInput from '../IUploadInput';
import CommandExecutor from './ICommandExecutor';

export interface AbortUploadInput { uploadId: string }

export default class AbortUploadExecutor implements CommandExecutor<AbortUploadInput, void> {
  constructor(private uploadInput: UploadInput) {}

  async execute(input: AbortUploadInput): Promise<void> {
    const {
      logger, key, bucketName, endpointUrl,
    } = this.uploadInput;

    const childLogger = logger?.child({ key, uploadId: input.uploadId });
    childLogger?.info('Aborting upload');

    try {
      await MyS3Client.getInstance(endpointUrl).send(
        new AbortMultipartUploadCommand({ Bucket: bucketName, Key: key, UploadId: input.uploadId }),
      );
      childLogger?.info('Successfully aborted upload');
    } catch (err) {
      childLogger?.error('Failed to abort upload', { error: err });
    }
  }
}
