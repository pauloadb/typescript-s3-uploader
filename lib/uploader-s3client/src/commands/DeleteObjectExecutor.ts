import { DeleteObjectCommand } from '@aws-sdk/client-s3';
import OperationInput from '../IOperationInput';
import MyS3Client from '../adapters/MyS3Client';
import CommandExecutor from './ICommandExecutor';

export default class DeleteObjectExecutor implements CommandExecutor<void, void> {
  constructor(private input: OperationInput) {}

  async execute(): Promise<void> {
    const {
      bucketName, key, logger, endpointUrl,
    } = this.input;

    const childLogger = logger?.child({ key });
    childLogger?.info('Deleting object');

    try {
      await MyS3Client.getInstance(endpointUrl).send(
        new DeleteObjectCommand({ Bucket: bucketName, Key: key }),
      );
      childLogger?.info('Successfully deleted object');
    } catch (err) {
      childLogger?.error('Failed to delete object', { cause: err });
      throw err;
    }
  }
}
