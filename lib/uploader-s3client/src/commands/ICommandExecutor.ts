export default interface CommandExecutor<Input, Output> {
  execute(input: Input): Promise<Output>;
}
