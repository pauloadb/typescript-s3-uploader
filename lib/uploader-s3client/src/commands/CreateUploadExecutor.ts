import { CreateMultipartUploadCommand } from '@aws-sdk/client-s3';
import MyS3Client from '../adapters/MyS3Client';
import UploadInput from '../IUploadInput';
import CommandExecutor from './ICommandExecutor';

export default class CreateUploadExecutor implements CommandExecutor<void, string> {
  constructor(private uploadInput: UploadInput) {}

  async execute(): Promise<string> {
    const { bucketName, key, logger } = this.uploadInput;
    const childLogger = logger?.child({ key });
    childLogger?.info('Creating multipart upload');

    try {
      const data = await MyS3Client.getInstance(this.uploadInput.endpointUrl).send(
        new CreateMultipartUploadCommand({ Bucket: bucketName, Key: key }),
      );
      if (!data.UploadId) throw new Error('Create upload response has no upload ID');
      childLogger?.info('Successfully created multipart upload', { uploadId: data.UploadId });
      return data.UploadId;
    } catch (err) {
      childLogger?.error('Failed to create multipart upload', { error: err });
      throw err;
    }
  }
}
