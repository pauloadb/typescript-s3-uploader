import { Upload } from '@aws-sdk/lib-storage';
import MyS3Client from '../adapters/MyS3Client';
import UploadInput from '../IUploadInput';
import CommandExecutor from './ICommandExecutor';

export default class UploadExecutor implements CommandExecutor<ReadableStream, void> {
  constructor(private uploadInput: UploadInput) {}

  async execute(body: ReadableStream): Promise<void> {
    const {
      key, bucketName, partSize, queueSize, logger,
    } = this.uploadInput;
    const childLogger = logger?.child({ key });
    childLogger?.info('Starting streamed multipart upload');

    const params = { Bucket: bucketName, Key: key, Body: body };
    try {
      await new Upload({
        client: MyS3Client.getInstance(this.uploadInput.endpointUrl),
        queueSize,
        partSize,
        leavePartsOnError: false,
        params,
      }).done();
      childLogger?.info('Successfully finished streamed multipart upload');
    } catch (err) {
      childLogger?.error('Failed to finish streamed multipart upload', { error: err });
      throw err;
    }
  }
}
