import { GetObjectCommand } from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import MyS3Client from '../adapters/MyS3Client';
import GetUrlInput from '../IGetUrlInput';
import CommandExecutor from './ICommandExecutor';

export default class GetUrlExecutor implements CommandExecutor<void, URL> {
  constructor(private input: GetUrlInput) {}

  async execute(): Promise<URL> {
    const {
      bucketName, key, logger, endpointUrl, fileName, expiresInSeconds,
    } = this.input;
    const childLogger = logger?.child({ key });
    childLogger?.info('Getting presigned URL');

    try {
      const url = await getSignedUrl(
        MyS3Client.getInstance(endpointUrl),
        new GetObjectCommand({
          Bucket: bucketName,
          Key: key,
          ResponseContentDisposition: `attachment; filename="${fileName}"`,
        }),
        { expiresIn: expiresInSeconds },
      );
      childLogger?.info('Successfully got the presigned URL', { url });
      return new URL(url);
    } catch (err) {
      childLogger?.error('Failed to get the presigned URL', { error: err });
      throw err;
    }
  }
}
