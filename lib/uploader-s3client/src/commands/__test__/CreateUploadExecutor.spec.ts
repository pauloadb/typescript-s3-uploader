describe('the create upload command executor', () => {
  const UPLOAD_INPUT = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  const mockMyS3Client = { send: jest.fn() };

  const MockCreateUploadCommand = jest.fn(() => ({ id: 'fake-create-command' }));

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ CreateMultipartUploadCommand: MockCreateUploadCommand }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the create upload command', () => import('../CreateUploadExecutor').then(async ({ default: CreateUploadExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ UploadId: '123' });
      await new CreateUploadExecutor({ ...UPLOAD_INPUT, logger }).execute();
      expect(MockCreateUploadCommand).toHaveBeenCalledWith({
        Bucket: UPLOAD_INPUT.bucketName,
        Key: UPLOAD_INPUT.key,
      });
    }));

    it('must send the command to the S3 client', () => import('../CreateUploadExecutor').then(async ({ default: CreateUploadExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ UploadId: '123' });
      await new CreateUploadExecutor({ ...UPLOAD_INPUT, logger }).execute();
      expect(mockMyS3Client.send).toHaveBeenCalledWith(new MockCreateUploadCommand());
    }));

    it('must throw en error is the command response has no upload ID', () => import('../CreateUploadExecutor').then(async ({ default: CreateUploadExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ });
      try {
        await new CreateUploadExecutor({ ...UPLOAD_INPUT, logger }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new Error('Create upload response has no upload ID'));
      }
    }));

    it('must throw some error that may occur', () => import('../CreateUploadExecutor').then(async ({ default: CreateUploadExecutor }) => {
      const error = new Error('Whoops');
      mockMyS3Client.send.mockRejectedValue(error);
      try {
        await new CreateUploadExecutor({ ...UPLOAD_INPUT, logger }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
  });
});
