import OperationInput from '../../IOperationInput';

describe('the delete object command executor', () => {
  const INPUT: OperationInput = {
    key: 'fake-key',
    bucketName: 'fake-bucket',
    endpointUrl: 'fake-endpoint-url',
  };

  const mockMyS3Client = { send: jest.fn() };
  const MockDeleteObjectCommand = jest.fn(() => ({ id: 'fake-delete-command' }));

  const mockLogger = {
    child: jest.fn(),
    info: jest.fn(),
    error: jest.fn(),
    debug: jest.fn(),
    warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ DeleteObjectCommand: MockDeleteObjectCommand }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the delete object command', () => import('../DeleteObjectExecutor').then(async ({ default: DeleteObjectExecutor }) => {
      await new DeleteObjectExecutor({ ...INPUT, logger }).execute();
      expect(MockDeleteObjectCommand).toHaveBeenCalledWith({
        Bucket: INPUT.bucketName,
        Key: INPUT.key,
      });
    }));

    it('must properly execute the delete object command', () => import('../DeleteObjectExecutor').then(async ({ default: DeleteObjectExecutor }) => {
      await new DeleteObjectExecutor({ ...INPUT, logger }).execute();
      expect(mockMyS3Client.send).toHaveBeenCalledWith(new MockDeleteObjectCommand());
    }));

    it('must throw some error that may occur', () => import('../DeleteObjectExecutor').then(async ({ default: DeleteObjectExecutor }) => {
      const error = new Error('Whoops');
      mockMyS3Client.send.mockRejectedValue(error);
      try {
        await new DeleteObjectExecutor({ ...INPUT, logger }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
  });
});
