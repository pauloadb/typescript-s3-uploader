describe('the upload part command executor', () => {
  const UPLOAD_INPUT = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  const UPLOAD_PART_INPUT = {
    uploadId: 'abc123',
    partNumber: 123,
    body: Buffer.from('test'),
  };

  const mockMyS3Client = { send: jest.fn() };

  const MockUploadPartCommand = jest.fn(() => ({ id: 'fake-uploadpart-command' }));

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ UploadPartCommand: MockUploadPartCommand }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the upload part command', () => import('../UploadPartExecutor').then(async ({ default: UploadPartExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ ETag: '123' });
      await new UploadPartExecutor({ ...UPLOAD_INPUT, logger }).execute(UPLOAD_PART_INPUT);
      expect(MockUploadPartCommand).toHaveBeenCalledWith({
        Bucket: UPLOAD_INPUT.bucketName,
        Key: UPLOAD_INPUT.key,
        UploadId: UPLOAD_PART_INPUT.uploadId,
        PartNumber: UPLOAD_PART_INPUT.partNumber,
        Body: UPLOAD_PART_INPUT.body,
      });
    }));

    it('must send the command to the S3 client', () => import('../UploadPartExecutor').then(async ({ default: UploadPartExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ ETag: '123' });
      await new UploadPartExecutor({ ...UPLOAD_INPUT, logger }).execute(UPLOAD_PART_INPUT);
      expect(mockMyS3Client.send).toHaveBeenCalledWith(new MockUploadPartCommand());
    }));

    it('must throw en error is the command response has no upload ID', () => import('../UploadPartExecutor').then(async ({ default: UploadPartExecutor }) => {
      mockMyS3Client.send.mockResolvedValue({ });
      try {
        await new UploadPartExecutor({ ...UPLOAD_INPUT, logger }).execute(UPLOAD_PART_INPUT);
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new Error('Upload part response has no ETag'));
      }
    }));

    it('must throw some error that may occur', () => import('../UploadPartExecutor').then(async ({ default: UploadPartExecutor }) => {
      const error = new Error('Whoops');
      mockMyS3Client.send.mockRejectedValue(error);
      try {
        await new UploadPartExecutor({ ...UPLOAD_INPUT, logger }).execute(UPLOAD_PART_INPUT);
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
  });
});
