import GetUrlInput from '../../IGetUrlInput';

describe('the get URL command executor', () => {
  const GET_URL_INPUT: GetUrlInput = {
    key: 'fake-key',
    bucketName: 'fake-bucket',
    endpointUrl: 'fake-endpoint-url',
    fileName: 'fake.file',
    expiresInSeconds: 60 * 60,
  };

  const mockMyS3Client = { send: jest.fn() };
  const MockGetObjectCommand = jest.fn(() => ({ id: 'fake-getobject-command' }));
  const mockGetSignedUrl = jest.fn();

  const mockLogger = {
    child: jest.fn(),
    info: jest.fn(),
    error: jest.fn(),
    debug: jest.fn(),
    warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ GetObjectCommand: MockGetObjectCommand }));
    jest.mock('@aws-sdk/s3-request-presigner', () => ({ getSignedUrl: mockGetSignedUrl }));
    mockGetSignedUrl.mockResolvedValue('http://fake-url.com');
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the get object command', () => import('../GetUrlExecutor').then(async ({ default: GetUrlExecutor }) => {
      await new GetUrlExecutor({ ...GET_URL_INPUT, logger }).execute();
      expect(MockGetObjectCommand).toHaveBeenCalledWith({
        Bucket: GET_URL_INPUT.bucketName,
        Key: GET_URL_INPUT.key,
        ResponseContentDisposition: `attachment; filename="${GET_URL_INPUT.fileName}"`,
      });
    }));

    it('must properly execute the get URL command', () => import('../GetUrlExecutor').then(async ({ default: GetUrlExecutor }) => {
      await new GetUrlExecutor({ ...GET_URL_INPUT, logger }).execute();
      expect(mockGetSignedUrl).toHaveBeenCalledWith(
        mockMyS3Client,
        new MockGetObjectCommand(),
        { expiresIn: GET_URL_INPUT.expiresInSeconds },
      );
    }));

    it('must return the URL to the object', () => import('../GetUrlExecutor').then(async ({ default: GetUrlExecutor }) => {
      const result = await new GetUrlExecutor({ ...GET_URL_INPUT, logger }).execute();
      expect(result).toStrictEqual(new URL('http://fake-url.com'));
    }));

    it('must throw some error that may occur', () => import('../GetUrlExecutor').then(async ({ default: GetUrlExecutor }) => {
      const error = new Error('Whoops');
      mockGetSignedUrl.mockRejectedValue(error);
      try {
        await new GetUrlExecutor({ ...GET_URL_INPUT, logger }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
  });
});
