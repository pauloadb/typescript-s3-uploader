describe('the complete upload command executor', () => {
  const UPLOAD_INPUT = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  const EXECUTOR_INPUT = { uploadId: 'abc123', parts: [{ ETag: '123', PartNumber: 1 }, { ETag: '456', PartNumber: 2 }] };

  const mockMyS3Client = { send: jest.fn() };

  const MockCompleteUploadCommand = jest.fn(() => ({ id: 'fake-complete-command' }));

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ CompleteMultipartUploadCommand: MockCompleteUploadCommand }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the complete upload command', () => import('../CompleteUploadExecutor').then(async ({ default: CompleteUploadExecutor }) => {
      await new CompleteUploadExecutor({ ...UPLOAD_INPUT, logger }).execute(EXECUTOR_INPUT);
      expect(MockCompleteUploadCommand).toHaveBeenCalledWith({
        Bucket: UPLOAD_INPUT.bucketName,
        Key: UPLOAD_INPUT.key,
        UploadId: EXECUTOR_INPUT.uploadId,
        MultipartUpload: { Parts: EXECUTOR_INPUT.parts },
      });
    }));

    it('must send the command to the S3 client', () => import('../CompleteUploadExecutor').then(async ({ default: CompleteUploadExecutor }) => {
      await new CompleteUploadExecutor({ ...UPLOAD_INPUT, logger }).execute(EXECUTOR_INPUT);
      expect(mockMyS3Client.send).toHaveBeenCalledWith(new MockCompleteUploadCommand());
    }));

    it('must throw some error that may occur', () => import('../CompleteUploadExecutor').then(async ({ default: CompleteUploadExecutor }) => {
      const error = new Error('Whoops');
      mockMyS3Client.send.mockRejectedValue(error);
      try {
        await new CompleteUploadExecutor({ ...UPLOAD_INPUT, logger }).execute(EXECUTOR_INPUT);
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
  });
});
