describe('the abort upload command executor', () => {
  const INPUT = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  const mockMyS3Client = { send: jest.fn() };

  const MockAbortUploadCommand = jest.fn(() => ({ id: 'fake-abort-command' }));

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
    jest.mock('@aws-sdk/client-s3', () => ({ AbortMultipartUploadCommand: MockAbortUploadCommand }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the abort upload command', () => import('../AbortUploadExecutor').then(async ({ default: AbortUploadExecutor }) => {
      await new AbortUploadExecutor({ ...INPUT, logger }).execute({ uploadId: 'abc123' });
      expect(MockAbortUploadCommand).toHaveBeenCalledWith({ Bucket: INPUT.bucketName, Key: INPUT.key, UploadId: 'abc123' });
    }));

    it('must send the command to the S3 client', () => import('../AbortUploadExecutor').then(async ({ default: AbortUploadExecutor }) => {
      await new AbortUploadExecutor({ ...INPUT, logger }).execute({ uploadId: 'abc123' });
      expect(mockMyS3Client.send).toHaveBeenCalledWith(new MockAbortUploadCommand());
    }));

    it('must not throw some error that may occur', () => import('../AbortUploadExecutor').then(async ({ default: AbortUploadExecutor }) => {
      const error = new Error('Whoops');
      mockMyS3Client.send.mockRejectedValue(error);
      await new AbortUploadExecutor({ ...INPUT, logger }).execute({ uploadId: 'abc123' });
    }));
  });
});
