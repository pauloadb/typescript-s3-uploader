import UploadInput from '../../IUploadInput';

describe('the upload command executor', () => {
  const UPLOAD_INPUT: UploadInput = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  const mockReadableStream = {
    locked: false,
    cancel: jest.fn(),
    getReader: jest.fn(),
    pipeThrough: jest.fn(),
    pipeTo: jest.fn(),
    tee: jest.fn(),
  };

  const mockLogger = {
    child: jest.fn(),
    info: jest.fn(),
    error: jest.fn(),
    debug: jest.fn(),
    warn: jest.fn(),
  };

  const mockUpload = { done: jest.fn() };
  const mockMyS3Client = { send: jest.fn() };
  const MockUpload = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
    MockUpload.mockReturnValue(mockUpload);
    mockLogger.child.mockReturnValue(mockLogger);
    jest.mock('@aws-sdk/lib-storage', () => ({ Upload: MockUpload }));
    jest.mock('../../adapters/MyS3Client', () => ({ getInstance: () => mockMyS3Client }));
  });

  describe.each([
    ['provided', mockLogger],
    ['not provided', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the upload command', () => import('../UploadExecutor')
      .then(async ({ default: UploadExecutor }) => {
        await new UploadExecutor({ ...UPLOAD_INPUT, logger }).execute(mockReadableStream);
        expect(MockUpload).toHaveBeenCalledWith({
          client: mockMyS3Client,
          queueSize: UPLOAD_INPUT.queueSize,
          partSize: UPLOAD_INPUT.partSize,
          leavePartsOnError: false,
          params: {
            Bucket: UPLOAD_INPUT.bucketName,
            Key: UPLOAD_INPUT.key,
            Body: mockReadableStream,
          },
        });
      }));

    it('must properly execute the upload command', () => import('../UploadExecutor')
      .then(async ({ default: UploadExecutor }) => {
        await new UploadExecutor({ ...UPLOAD_INPUT, logger }).execute(mockReadableStream);
        expect(mockUpload.done).toHaveBeenCalled();
      }));

    it('must throw some error that may occur', () => import('../UploadExecutor')
      .then(async ({ default: UploadExecutor }) => {
        const error = new Error('Whoops');
        mockUpload.done.mockRejectedValue(error);
        try {
          await new UploadExecutor({ ...UPLOAD_INPUT, logger }).execute(mockReadableStream);
          throw new Error('Test failed');
        } catch (err) {
          expect(err).toStrictEqual(error);
        }
      }));
  });
});
