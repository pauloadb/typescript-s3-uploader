import { CompletedPart, CompleteMultipartUploadCommand } from '@aws-sdk/client-s3';
import MyS3Client from '../adapters/MyS3Client';
import UploadInput from '../IUploadInput';
import CommandExecutor from './ICommandExecutor';

export interface CompleteUploadInput {
  uploadId: string,
  parts: CompletedPart[]
}

export default class CompleteUploadExecutor implements CommandExecutor<CompleteUploadInput, void> {
  constructor(private uploadInput: UploadInput) {}

  async execute(input: CompleteUploadInput): Promise<void> {
    const {
      logger, key, endpointUrl, bucketName,
    } = this.uploadInput;

    const childLogger = logger?.child({ key, uploadId: input.uploadId });
    childLogger?.info('Completing upload');

    try {
      await MyS3Client.getInstance(endpointUrl).send(new CompleteMultipartUploadCommand({
        Bucket: bucketName,
        Key: key,
        UploadId: input.uploadId,
        MultipartUpload: { Parts: input.parts },
      }));
      childLogger?.info('`Successfully completed upload');
    } catch (err) {
      childLogger?.error('`Failed to complete upload', { error: err });
      throw err;
    }
  }
}
