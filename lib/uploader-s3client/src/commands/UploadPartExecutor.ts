import { UploadPartCommand } from '@aws-sdk/client-s3';
import MyS3Client from '../adapters/MyS3Client';
import UploadInput from '../IUploadInput';
import CommandExecutor from './ICommandExecutor';

export interface UploadPartInput {
  uploadId: string,
  partNumber: number,
  body: Buffer,
}

export default class UploadPartExecutor implements CommandExecutor<UploadPartInput, string> {
  constructor(private uploadInput: UploadInput) {}

  async execute(input: UploadPartInput): Promise<string> {
    const {
      key, bucketName, endpointUrl, logger,
    } = this.uploadInput;

    const { partNumber, uploadId, body } = input;

    const childLogger = logger?.child({ key, partNumber, uploadId });
    childLogger?.info('Uploading part');

    try {
      const data = await MyS3Client.getInstance(endpointUrl)
        .send(new UploadPartCommand({
          Bucket: bucketName,
          Key: key,
          UploadId: uploadId,
          PartNumber: partNumber,
          Body: body,
        }));

      if (!data.ETag) throw new Error('Upload part response has no ETag');
      childLogger?.info('Successfully uploaded part', { eTag: data.ETag });
      return data.ETag;
    } catch (err) {
      childLogger?.info('Failed to upload part', { error: err });
      throw err;
    }
  }
}
