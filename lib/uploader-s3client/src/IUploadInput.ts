import OperationInput from './IOperationInput';

export default interface UploadInput extends OperationInput {
  sourceUrl: URL,
  queueSize: number,
  partSize: number,
}
