import axios, { AxiosInstance } from 'axios';
import axiosRetry from 'axios-retry';
import { getError } from 'uploader-utils';

export default class HttpClient {
  private static instance: HttpClient;

  private axiosInstance: AxiosInstance;

  private constructor() {
    this.axiosInstance = axios.create({
      timeout: 1000 * 60 * 60,
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });
    axiosRetry(this.axiosInstance, {
      retries: 3,
      retryDelay: (retryCount = 0) => (retryCount === 0
        ? retryCount
        : (2 ** (retryCount - 1)) * 1000),
    });
  }

  async download<T = ReadableStream | Buffer>(
    url: URL,
    responseType: 'stream' | 'arraybuffer',
    headers?: { [key: string]: string },
  ): Promise<T> {
    try {
      const response = await this.axiosInstance.get<T>(
        url.toString(),
        { responseType, headers },
      );
      return response.data;
    } catch (err) {
      throw new Error(`Http client failed to download from URL ${url.toString()}: ${getError(err).message}`);
    }
  }

  async getHeaders(url: URL): Promise<{ [key: string]: unknown }> {
    try {
      const response = await this.axiosInstance.head(url.toString());
      return response.headers;
    } catch (err) {
      throw new Error(`Failed to get headers from URL ${url.toString()}: ${getError(err).message}`);
    }
  }

  static getInstance() {
    if (!HttpClient.instance) {
      HttpClient.instance = new HttpClient();
    }
    return HttpClient.instance;
  }
}
