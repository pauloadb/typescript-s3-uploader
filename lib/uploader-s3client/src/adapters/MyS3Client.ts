import { S3Client } from '@aws-sdk/client-s3';

export default class MyS3Client extends S3Client {
  private static instance: MyS3Client;

  private constructor(endpointUrl?: string) {
    super({
      apiVersion: '2006-03-01',
      maxAttempts: 5,
      endpoint: endpointUrl,
      forcePathStyle: true,
    });
  }

  static getInstance(endpointUrl?: string) {
    if (!this.instance) {
      this.instance = new MyS3Client(endpointUrl);
    }
    return this.instance;
  }
}
