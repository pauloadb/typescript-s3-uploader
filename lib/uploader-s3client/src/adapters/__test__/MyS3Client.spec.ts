describe('my s3 client', () => {
  const MockS3Client = jest.fn();

  beforeEach(() => {
    jest.mock('@aws-sdk/client-s3', () => ({ S3Client: MockS3Client }));
  });

  it('must properly initialize the s3 client', () => import('../MyS3Client')
    .then(({ default: MyS3Client }) => {
      const endpointUrl = 'fake-endpoint-url';
      MyS3Client.getInstance(endpointUrl);
      expect(MockS3Client).toHaveBeenCalledWith({
        apiVersion: '2006-03-01',
        maxAttempts: 5,
        endpoint: endpointUrl,
        forcePathStyle: true,
      });
    }));
});
