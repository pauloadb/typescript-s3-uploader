describe('the http client', () => {
  const mockAxiosInstance = { get: jest.fn(), head: jest.fn() };
  const mockAxios = { create: jest.fn() };
  const mockAxiosRetry = jest.fn();

  beforeEach(() => {
    jest.resetModules();

    mockAxios.create.mockReturnValue(mockAxiosInstance);
    jest.mock('axios', () => mockAxios);
    jest.mock('axios-retry', () => mockAxiosRetry);
  });

  it('must properly intialize the axios instance', () => import('../HttpClient').then(({ default: HttpClient }) => {
    HttpClient.getInstance();
    expect(mockAxios.create).toHaveBeenCalledWith({
      timeout: 1000 * 60 * 60,
      maxContentLength: Infinity,
      maxBodyLength: Infinity,
    });
    expect(mockAxiosRetry).toHaveBeenCalledWith(mockAxiosInstance, {
      retries: 3,
      retryDelay: expect.any(Function),
    });
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const { retryDelay }: { retryDelay: (n: number) => number } = mockAxiosRetry.mock.calls[0][1];
    expect(retryDelay(0)).toBe(0);
    expect(retryDelay(1)).toBe(1000);
    expect(retryDelay(2)).toBe(2000);
    expect(retryDelay(3)).toBe(4000);
  }));

  describe('when downloading', () => {
    it('must return the %s when the axios operation succeeds', () => import('../HttpClient')
      .then(async ({ default: HttpClient }) => {
        const url = new URL('http://fake.url/fake.file');
        const headers = { fake: 'value' };
        const stream = { id: 'fake-stream' };
        mockAxiosInstance.get.mockResolvedValue({ data: stream });
        const result = await HttpClient.getInstance().download(url, 'stream', headers);
        expect(mockAxiosInstance.get).toHaveBeenCalledWith(url.toString(), { responseType: 'stream', headers });
        expect(result).toStrictEqual(stream);
      }));

    it('must throw an error when the axios operation fails', () => import('../HttpClient')
      .then(async ({ default: HttpClient }) => {
        const error = new Error('Whoops');
        const url = new URL('http://fake.url/fake.file');
        mockAxiosInstance.get.mockRejectedValue(error);
        try {
          await HttpClient.getInstance().download(url, 'stream', { fake: 'value' });
          throw Error('Test failed');
        } catch (err) {
          expect(err).toStrictEqual(new Error(`Http client failed to download from URL ${url.toString()}: ${error.message}`));
        }
      }));
  });

  describe('when getting headers', () => {
    it('must return the headers when the axios operation succeeds', () => import('../HttpClient')
      .then(async ({ default: HttpClient }) => {
        const url = new URL('http://fake.url/fake.file');
        const headers = { fake: 'value' };
        mockAxiosInstance.head.mockResolvedValue({ headers });
        const result = await HttpClient.getInstance().getHeaders(url);
        expect(mockAxiosInstance.head).toHaveBeenCalledWith(url.toString());
        expect(result).toStrictEqual(headers);
      }));

    it('must throw an error when the axios operation fails', () => import('../HttpClient')
      .then(async ({ default: HttpClient }) => {
        const error = new Error('Whoops');
        const url = new URL('http://fake.url/fake.file');
        mockAxiosInstance.head.mockRejectedValue(error);
        try {
          await HttpClient.getInstance().getHeaders(url);
          throw Error('Test failed');
        } catch (err) {
          expect(err).toStrictEqual(new Error(`Failed to get headers from URL ${url.toString()}: ${error.message}`));
        }
      }));
  });
});
