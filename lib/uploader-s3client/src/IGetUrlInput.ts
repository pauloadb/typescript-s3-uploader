import OperationInput from './IOperationInput';

export default interface GetUrlInput extends OperationInput {
  fileName: string,
  expiresInSeconds: number,
}
