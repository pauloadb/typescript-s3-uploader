import ByteRangedSource from '../uploads/sources/ranged/ByteRangedSource';
import StreamedSource from '../uploads/sources/streamed/StreamedSource';
import UploadEvent from '../uploads/UploadEvent';
import UploadInput from '../IUploadInput';

class MockByteRangedSource extends ByteRangedSource { protected downloadRange = jest.fn(); }
class MockStreamedSource extends StreamedSource { public download = jest.fn(); }

describe('the upload operation', () => {
  const mockUploadSourceFactory = { createSource: jest.fn() };
  const MockUploadSourceFactory = jest.fn();

  const mockRangedDaemon = { on: jest.fn(), start: jest.fn() };
  const MockRangedDaemon = jest.fn();

  const mockUploadExecutor = { execute: jest.fn() };
  const MockUploadExecutor = jest.fn();

  const mockInput: UploadInput = {
    sourceUrl: new URL('http://fake-url.com/fake-file.fake'),
    queueSize: 10,
    partSize: 50 * 1024 * 1024,
    key: 'fake-key',
    bucketName: 'fake-bucket',
  };

  beforeEach(() => {
    MockUploadSourceFactory.mockReturnValue(mockUploadSourceFactory);
    jest.mock('../uploads/sources/UploadSourceFactory', () => MockUploadSourceFactory);

    MockRangedDaemon.mockReturnValue(mockRangedDaemon);
    jest.mock('../uploads/ranged/RangedUploadDaemon', () => MockRangedDaemon);

    MockUploadExecutor.mockReturnValue(mockUploadExecutor);
    jest.mock('../commands/UploadExecutor', () => MockUploadExecutor);
  });

  it('must upload when the source is byte ranged', () => import('../UploadOperation')
    .then(async ({ default: UploadOperation }) => {
      mockUploadSourceFactory.createSource.mockResolvedValue(
        new MockByteRangedSource(new URL('http://fake.url/fake.file'), 10000, 1000),
      );
      mockRangedDaemon.on.mockImplementation((event: UploadEvent, cb: () => void) => {
        if (event === UploadEvent.COMPLETED) cb();
      });
      await new UploadOperation(mockInput).execute();
      expect(MockRangedDaemon).toHaveBeenCalled();
    }));

  it('must upload when the source is streamed', () => import('../UploadOperation')
    .then(async ({ default: UploadOperation }) => {
      const source = new MockStreamedSource(new URL('http://fake.url/fake.file'), 10000);
      source.download.mockResolvedValue({ id: 'fake-stream' });
      mockUploadSourceFactory.createSource.mockResolvedValue(source);
      await new UploadOperation(mockInput).execute();
      expect(mockUploadExecutor.execute).toHaveBeenCalledWith({ id: 'fake-stream' });
    }));

  it('must reject the promise when source is ranged and some error occurs', () => import('../UploadOperation')
    .then(async ({ default: UploadOperation }) => {
      mockUploadSourceFactory.createSource.mockResolvedValue(
        new MockByteRangedSource(new URL('http://fake.url/fake.file'), 10000, 1000),
      );
      const error = new Error('Whoops');
      mockRangedDaemon.on.mockImplementation((event: UploadEvent, cb: (err: Error) => void) => {
        if (event === UploadEvent.ERROR) cb(error);
      });

      try {
        await new UploadOperation(mockInput).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(error);
      }
    }));
});
