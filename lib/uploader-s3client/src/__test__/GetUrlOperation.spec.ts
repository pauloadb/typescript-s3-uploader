describe('the get url operation', () => {
  const mockGetUrlExecutor = { execute: jest.fn() };
  const MockGetUrlExecutor = jest.fn();

  beforeEach(() => {
    MockGetUrlExecutor.mockReturnValue(mockGetUrlExecutor);
    jest.mock('../commands/GetUrlExecutor', () => MockGetUrlExecutor);
  });

  it('must properly execute the underlying command', () => import('../GetUrlOperation')
    .then(async ({ default: GetUrlOperation }) => {
      const input = {
        bucketName: 'fake-bucket', fileName: 'file.fake', key: 'fake-key', expiresInSeconds: 3600,
      };
      await new GetUrlOperation(input).execute();
      expect(MockGetUrlExecutor).toBeCalledWith(input);
      expect(mockGetUrlExecutor.execute).toHaveBeenCalled();
    }));
});
