describe('the delete file operation', () => {
  const mockDeleteFileExecutor = { execute: jest.fn() };
  const MockDeleteObjectExecutor = jest.fn();

  beforeEach(() => {
    MockDeleteObjectExecutor.mockReturnValue(mockDeleteFileExecutor);
    jest.mock('../commands/DeleteObjectExecutor', () => MockDeleteObjectExecutor);
  });

  it('must properly execute the underlying command', () => import('../DeleteFileOperation')
    .then(async ({ default: DeleteFileOperation }) => {
      const input = { bucketName: 'fake-bucket', key: 'fake-key' };
      await new DeleteFileOperation(input).execute();
      expect(MockDeleteObjectExecutor).toBeCalledWith(input);
      expect(mockDeleteFileExecutor.execute).toHaveBeenCalled();
    }));
});
