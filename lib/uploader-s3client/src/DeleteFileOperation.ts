import DeleteObjectExecutor from './commands/DeleteObjectExecutor';
import Operation from './IOperation';
import OperationInput from './IOperationInput';

export default class DeleteFileOperation implements Operation<OperationInput, void> {
  constructor(public input: OperationInput) {}

  async execute(): Promise<void> {
    await new DeleteObjectExecutor(this.input).execute();
  }
}
