import OperationInput from './IOperationInput';

export default interface Operation<I extends OperationInput, O> {
  input: I;

  execute(): Promise<O>;
}
