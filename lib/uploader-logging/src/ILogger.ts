export default interface Logger {
  info(message: string, ...meta: unknown[]): void;
  debug(message: string, ...meta: unknown[]): void;
  error(message: string | Error, ...meta: unknown[]): void;
  warn(message: string, ...meta: unknown[]): void;
  http(message: string, ...meta: unknown[]): void;
  child(options: object): Logger;
}
