enum LogLevel {
  WARNING = 'warn',
  DEBUG = 'debug',
  INFO = 'info',
  ERROR = 'error',
  HTTP = 'http',
}

export default LogLevel;
