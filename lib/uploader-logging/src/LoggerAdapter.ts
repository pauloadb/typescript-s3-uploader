import path from 'path';
import winston from 'winston';
import 'winston-daily-rotate-file';
import Logger from './ILogger';
import LogLevel from './LogLevel';

const BASE_FORMATS = [
  winston.format.errors({ stack: true }),
  winston.format.timestamp(),
  winston.format.json(),
];

const ROTATE_OPTS = {
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '5mb',
  maxFiles: '10d',
  utc: true,
  createSymlink: true,
};

const getRotateConfig = (
  filePath: string,
  level: string | undefined = undefined,
) => ({
  filename: filePath,
  ...ROTATE_OPTS,
  symlinkName: path.basename(filePath),
  level,
});

class LoggerAdapter implements Logger {
  private logger: Logger;

  constructor(minimumLevel: LogLevel, errorFile: string, outputFile: string, skipConsole: boolean) {
    const winstonLogger = winston.createLogger({
      level: minimumLevel,
      levels: {
        error: 0, warn: 1, info: 2, http: 3, debug: 4,
      },
      format: winston.format.combine(...BASE_FORMATS),
      transports: [
        new winston.transports.DailyRotateFile(getRotateConfig(errorFile, 'error')),
        new winston.transports.DailyRotateFile(getRotateConfig(outputFile)),
      ],
    });

    if (!skipConsole) {
      winstonLogger.add(new winston.transports.Console({
        format: winston.format.combine(...BASE_FORMATS, winston.format.prettyPrint()),
      }));
    }

    this.logger = winstonLogger;
  }

  info(message: string, ...meta: unknown[]): void {
    this.logger.info(message, meta);
  }

  debug(message: string, ...meta: unknown[]): void {
    this.logger.debug(message, meta);
  }

  error(message: string | Error, ...meta: unknown[]): void {
    this.logger.error(message, meta);
  }

  warn(message: string, ...meta: unknown[]): void {
    this.logger.warn(message, meta);
  }

  http(message: string, ...meta: unknown[]): void {
    this.logger.http(message, meta);
  }

  child(options: object): Logger {
    return this.logger.child(options);
  }
}

export default LoggerAdapter;
