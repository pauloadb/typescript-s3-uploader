import LogLevel from '../LogLevel';

describe('the logger adapter', () => {
  const mockLogger = {
    add: jest.fn(),
    info: jest.fn(),
    debug: jest.fn(),
    error: jest.fn(),
    warn: jest.fn(),
    http: jest.fn(),
    child: jest.fn(),
  };

  const mockWinston = {
    format: {
      errors: jest.fn(),
      timestamp: jest.fn(),
      json: jest.fn(),
      combine: jest.fn(),
      prettyPrint: jest.fn(),
    },
    createLogger: jest.fn(),
    transports: { DailyRotateFile: jest.fn(), Console: jest.fn() },
  };

  beforeEach(() => {
    jest.resetModules();
    mockWinston.createLogger.mockReturnValue(mockLogger);
    mockWinston.format.errors.mockReturnValue({ id: 'fake-error-format' });
    mockWinston.format.timestamp.mockReturnValue({ id: 'fake-timestamp-format' });
    mockWinston.format.json.mockReturnValue({ id: 'fake-json-format' });
    mockWinston.format.combine.mockImplementation((...formats: unknown[]) => ({ id: 'fake-combined-format', formats }));
    mockWinston.format.prettyPrint.mockReturnValue({ id: 'fake-pretty-format' });
    mockWinston.transports.Console.mockReturnValue({ id: 'fake-console-transport' });
    mockWinston.transports.DailyRotateFile.mockReturnValue({ id: 'fake-rotate-file-transport' });
    jest.mock('winston', () => (mockWinston));
    jest.mock('winston-daily-rotate-file', () => jest.fn());
  });

  it.each([
    ['must', true],
    ['must not', false],
  ])('must properly create the logger when it %s skip console', (_, skipConsole) => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', skipConsole);
      expect(mockWinston.createLogger).toHaveBeenCalledWith({
        level: LogLevel.HTTP,
        levels: {
          error: 0, warn: 1, info: 2, http: 3, debug: 4,
        },
        format: {
          id: 'fake-combined-format',
          formats: [
            { id: 'fake-error-format' },
            { id: 'fake-timestamp-format' },
            { id: 'fake-json-format' },
          ],
        },
        transports: [
          { id: 'fake-rotate-file-transport' },
          { id: 'fake-rotate-file-transport' },
        ],
      });

      expect(mockWinston.transports.DailyRotateFile).toHaveBeenNthCalledWith(1, {
        createSymlink: true,
        datePattern: 'YYYY-MM-DD',
        filename: '/path/errors.log',
        level: 'error',
        maxFiles: '10d',
        maxSize: '5mb',
        symlinkName: 'errors.log',
        utc: true,
        zippedArchive: true,
      });

      expect(mockWinston.transports.DailyRotateFile).toHaveBeenNthCalledWith(2, {
        createSymlink: true,
        datePattern: 'YYYY-MM-DD',
        filename: '/path/output.log',
        maxFiles: '10d',
        maxSize: '5mb',
        symlinkName: 'output.log',
        utc: true,
        zippedArchive: true,
      });

      if (skipConsole) {
        expect(mockLogger.add).not.toHaveBeenCalled();
      } else {
        expect(mockLogger.add).toHaveBeenCalledWith({ id: 'fake-console-transport' });
        expect(mockWinston.transports.Console).toHaveBeenCalledWith({ format: { formats: [{ id: 'fake-error-format' }, { id: 'fake-timestamp-format' }, { id: 'fake-json-format' }, { id: 'fake-pretty-format' }], id: 'fake-combined-format' } });
      }
    }));

  it('must properly proxy the info operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.info('This is the message', 1, 'abc');
      expect(mockLogger.info).toHaveBeenCalledWith('This is the message', [1, 'abc']);
    }));

  it('must properly proxy the debug operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.debug('This is the message', 1, 'abc');
      expect(mockLogger.debug).toHaveBeenCalledWith('This is the message', [1, 'abc']);
    }));

  it('must properly proxy the error operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.error('This is the message', 1, 'abc');
      expect(mockLogger.error).toHaveBeenCalledWith('This is the message', [1, 'abc']);
    }));

  it('must properly proxy the warn operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.warn('This is the message', 1, 'abc');
      expect(mockLogger.warn).toHaveBeenCalledWith('This is the message', [1, 'abc']);
    }));

  it('must properly proxy the http operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.http('This is the message', 1, 'abc');
      expect(mockLogger.http).toHaveBeenCalledWith('This is the message', [1, 'abc']);
    }));

  it('must properly proxy the child operation to the winston logger', () => import('../LoggerAdapter')
    .then(({ default: LoggerAdapter }) => {
      const logger = new LoggerAdapter(LogLevel.HTTP, '/path/errors.log', '/path/output.log', true);
      logger.child({ opt1: 'val1', opt2: 123 });
      expect(mockLogger.child).toHaveBeenCalledWith({ opt1: 'val1', opt2: 123 });
    }));
});
