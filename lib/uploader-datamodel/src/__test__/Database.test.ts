describe('the database adapter', () => {
  beforeEach(() => {
    jest.mock('@prisma/client');
  });

  it('must return the database instance', () => import('../Database').then(({ default: Database }) => {
    expect(Database.getInstance()).not.toBeNull();
  }));
});
