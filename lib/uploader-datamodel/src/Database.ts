import { PrismaClient } from '@prisma/client';

class Database extends PrismaClient {
  private static instance: Database;

  private constructor() {
    super();
  }

  public static getInstance() : Database {
    if (!this.instance) {
      this.instance = new Database();
    }
    return this.instance;
  }
}

export default Database;
