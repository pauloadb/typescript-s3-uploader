import Database from './Database';

export * from '@prisma/client';
export default Database;
