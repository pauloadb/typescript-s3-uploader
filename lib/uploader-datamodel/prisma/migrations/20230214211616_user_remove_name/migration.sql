/*
  Warnings:

  - You are about to drop the column `name` on the `app_user` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "app_user" DROP COLUMN "name";
