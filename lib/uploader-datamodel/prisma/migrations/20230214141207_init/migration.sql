-- CreateEnum
CREATE TYPE "FileStatus" AS ENUM ('CREATED', 'DELETED', 'FAILED', 'UPLOADED', 'UPLOADING');

-- CreateTable
CREATE TABLE "app_user" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(100) NOT NULL,
    "inserted_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "app_user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "file" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "source_url" VARCHAR(255) NOT NULL,
    "s3_object_key" TEXT NOT NULL,
    "inserted_at" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted_at" TIMESTAMP(3),
    "status" "FileStatus" NOT NULL,
    "transfer_errors_qty" INTEGER NOT NULL DEFAULT 0,
    "transfer_started_at" TIMESTAMP(3),
    "transfer_finished_at" TIMESTAMP(3),
    "transfer_failed_at" TIMESTAMP(3),
    "owner_id" INTEGER NOT NULL,

    CONSTRAINT "file_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_unique" ON "app_user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "file_s3_object_key_unique" ON "file"("s3_object_key");

-- AddForeignKey
ALTER TABLE "file" ADD CONSTRAINT "file_owner_id_fkey" FOREIGN KEY ("owner_id") REFERENCES "app_user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
