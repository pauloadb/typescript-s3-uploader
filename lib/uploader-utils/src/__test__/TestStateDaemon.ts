import StateBasedDaemon from '../StateBasedDaemon';

export enum MockEvent { ERROR = 'error', OTHER = 'other' }
export type MockEventRecord = Record<MockEvent, unknown>;

export default class TestStateDaemon extends StateBasedDaemon<MockEventRecord> {
  createErrorState = jest.fn();

  createInitialState = jest.fn();

  public execute(): void {
    super.execute();
  }

  public start = jest.fn();

  public stop = jest.fn();

  public isRunning = jest.fn();
}
