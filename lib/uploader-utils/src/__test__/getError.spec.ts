import getError from '../getError';

describe('the get error utility', () => {
  it.each([
    [new Error('Whoops')],
    [{ message: 'Whoops' }],
  ])('must return an error when the argument is errorish', (arg) => {
    expect(getError(arg)).toStrictEqual(new Error('Whoops'));
  });

  it.each([
    [123],
    [{ id: 1 }],
    ['abc'],
  ])('must return an error when the argument is not errorish', (arg) => {
    expect(getError(arg)).toStrictEqual(new Error('Unknown error'));
  });
});
