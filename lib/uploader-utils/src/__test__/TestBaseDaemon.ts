import BaseDaemon from '../BaseDaemon';
import { EventRecord } from '../MyEventEmitter';

export default class MyBaseDaemon extends BaseDaemon<EventRecord> {
  public execute = jest.fn();
}
