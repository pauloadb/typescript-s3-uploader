describe('the base daemon', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  it('must properly update the running flag when starting', () => import('./TestBaseDaemon').then(({ default: TestBaseDaemon }) => {
    const daemon = new TestBaseDaemon();
    expect(daemon.isRunning()).toBe(false);
    daemon.start();
    expect(daemon.isRunning()).toBe(true);
  }));

  it('must do nothing if asked to start and already running', () => import('./TestBaseDaemon').then(({ default: TestBaseDaemon }) => {
    const daemon = new TestBaseDaemon();
    daemon.start();
    jest.advanceTimersToNextTimer();
    expect(daemon.execute).toHaveBeenCalledTimes(1);
    daemon.start();
    jest.advanceTimersToNextTimer();
    expect(daemon.execute).toHaveBeenCalledTimes(1);
  }));

  it('must execute on next tick after start', () => import('./TestBaseDaemon').then(({ default: TestBaseDaemon }) => {
    const daemon = new TestBaseDaemon();
    daemon.start();
    expect(daemon.execute).not.toHaveBeenCalled();
    jest.advanceTimersToNextTimer();
    expect(daemon.execute).toHaveBeenCalled();
  }));

  it('must set the running flag to false when stopping', () => import('./TestBaseDaemon').then(({ default: TestBaseDaemon }) => {
    const daemon = new TestBaseDaemon();
    daemon.start();
    expect(daemon.isRunning()).toBe(true);
    daemon.stop();
    expect(daemon.isRunning()).toBe(false);
  }));
});
