enum MockEvent { ERROR = 'error' }
type MockEventRecord = Record<MockEvent, unknown>;

describe('my event emitter', () => {
  const mockEventEmitter = { on: jest.fn(), off: jest.fn(), emit: jest.fn() };
  const MockEventEmitter = jest.fn();

  beforeEach(() => {
    MockEventEmitter.mockReturnValue(mockEventEmitter);
    jest.mock('events', () => MockEventEmitter);
  });

  it('must proxy the on calls to the event emitter', () => import('../MyEventEmitter')
    .then(({ default: MyEventEmitter }) => {
      const emitter = new MyEventEmitter<MockEventRecord>();
      const mockHandler = jest.fn();
      emitter.on(MockEvent.ERROR, mockHandler);
      expect(mockEventEmitter.on).toHaveBeenCalledWith(MockEvent.ERROR, mockHandler);
    }));

  it('must proxy the off calls to the event emitter', () => import('../MyEventEmitter')
    .then(({ default: MyEventEmitter }) => {
      const emitter = new MyEventEmitter<MockEventRecord>();
      const mockHandler = jest.fn();
      emitter.off(MockEvent.ERROR, mockHandler);
      expect(mockEventEmitter.off).toHaveBeenCalledWith(MockEvent.ERROR, mockHandler);
    }));

  it('must proxy the emit calls to the event emitter', () => import('../MyEventEmitter')
    .then(({ default: MyEventEmitter }) => {
      const emitter = new MyEventEmitter<MockEventRecord>();
      const error = new Error('Whoops');
      emitter.emit(MockEvent.ERROR, error);
      expect(mockEventEmitter.emit).toHaveBeenCalledWith(MockEvent.ERROR, error);
    }));
});
