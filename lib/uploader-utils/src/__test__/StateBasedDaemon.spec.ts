const INITIAL = { id: 'initial-state', execute: jest.fn() };
const ERROR = { id: 'error-state', execute: jest.fn() };

describe('the state based daemon', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    jest.resetAllMocks();
    jest.mock('../BaseDaemon');
  });

  it('must set the state to initial if it is null and is executing', () => import('./TestStateDaemon')
    .then(({ default: TestStateDaemon }) => {
      INITIAL.execute.mockImplementation(async () => {});
      const daemon = new TestStateDaemon();
      daemon.createInitialState.mockReturnValue(INITIAL);
      daemon.isRunning.mockReturnValue(true);
      daemon.execute();
      expect(INITIAL.execute).toHaveBeenCalled();
    }));

  it('must set the state to error the state execution fails', () => import('./TestStateDaemon')
    .then(async ({ default: TestStateDaemon }) => {
      const error = new Error('Whoops');
      const daemon = new TestStateDaemon();

      const errorPromise = new Promise((resolve) => { resolve(null); });
      ERROR.execute.mockImplementation(() => errorPromise);
      const initialPromise = new Promise((_, reject) => { reject(error); });
      INITIAL.execute.mockImplementation(() => initialPromise);

      daemon.createErrorState.mockReturnValue(ERROR);
      daemon.createInitialState.mockReturnValue(INITIAL);
      daemon.isRunning.mockReturnValue(true);

      daemon.execute();
      expect(INITIAL.execute).toHaveBeenCalled();
      await Promise.allSettled([initialPromise]);
      expect(daemon.createErrorState).toHaveBeenCalledWith(error);
      jest.advanceTimersToNextTimer();
      expect(ERROR.execute).toHaveBeenCalled();
    }));

  it('must not execute if it is not running', () => import('./TestStateDaemon')
    .then(({ default: TestStateDaemon }) => {
      INITIAL.execute.mockImplementation(async () => {});
      const daemon = new TestStateDaemon();
      daemon.createInitialState.mockReturnValue(INITIAL);
      daemon.isRunning.mockReturnValue(false);
      daemon.execute();
      expect(INITIAL.execute).not.toHaveBeenCalled();
    }));
});
