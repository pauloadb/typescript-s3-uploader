import BaseDaemon from './BaseDaemon';
import { DaemonState, DaemonStateContext } from './IDaemonState';
import getError from './getError';
import { EventRecord } from './MyEventEmitter';

export default abstract class StateBasedDaemon<Record extends EventRecord>
  extends BaseDaemon<Record>
  implements DaemonStateContext<Record> {
  private state: DaemonState<Record> | null = null;

  abstract createErrorState(error: Error): DaemonState<Record>;
  abstract createInitialState(): DaemonState<Record>;

  changeState(newState: DaemonState<Record>): void {
    this.state = newState;
    setImmediate(this.execute.bind(this));
  }

  protected execute(): void {
    if (!this.isRunning()) return;

    if (!this.state) {
      this.state = this.createInitialState();
    }

    this.state.execute().catch((err) => {
      this.changeState(this.createErrorState(getError(err)));
    });
  }
}
