import EventEmitter from 'events';

export type EventRecord = Record<string, unknown>;
export type EventName<T extends EventRecord> = string & keyof T;
export type EventHandler<T> = (params: T) => void;

export interface TypedEventEmitter<T extends EventRecord> {
  on<K extends EventName<T>>
  (eventName: K, fn: EventHandler<T[K]>): void;

  off<K extends EventName<T>>
  (eventName: K, fn: EventHandler<T[K]>): void;

  emit<K extends EventName<T>>
  (eventName: K, params?: T[K]): void;
}

export default class MyEventEmitter<T extends EventRecord> implements TypedEventEmitter<T> {
  protected eventEmitter = new EventEmitter();

  on<K extends EventName<T>>(eventName: K, eventHandler: EventHandler<T[K]>): void {
    this.eventEmitter.on(eventName, eventHandler);
  }

  off<K extends EventName<T>>(eventName: K, eventHandler: EventHandler<T[K]>): void {
    this.eventEmitter.off(eventName, eventHandler);
  }

  emit<K extends EventName<T>>(eventName: K, argument?: T[K]): void {
    this.eventEmitter.emit(eventName, argument);
  }
}
