type Errorish = { message: string, stack?: string };

function isErrorish(obj: unknown): obj is Errorish {
  return (typeof obj === 'object' && obj !== null && 'message' in obj);
}

export default function getError(err: unknown): Error {
  if (err instanceof Error) {
    return err;
  }

  if (isErrorish(err)) {
    const error = new Error(err.message);
    error.stack = err.stack;
    return error;
  }

  return new Error('Unknown error');
}
