import { EventRecord, TypedEventEmitter } from './MyEventEmitter';

export default interface Daemon<T extends EventRecord> extends TypedEventEmitter<T> {
  start(): void;
  stop(): void;
  isRunning(): boolean;
}
