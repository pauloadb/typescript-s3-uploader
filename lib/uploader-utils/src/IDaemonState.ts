import Daemon from './IDaemon';
import { EventRecord } from './MyEventEmitter';

export interface DaemonStateContext<E extends EventRecord> extends Daemon<E> {
  changeState(newState: DaemonState<E>): void;
  createErrorState(error: Error): DaemonState<E>;
  createInitialState(): DaemonState<E>;
}

export interface DaemonState<E extends EventRecord> {
  readonly daemon: DaemonStateContext<E>;
  execute(): Promise<void>
}
