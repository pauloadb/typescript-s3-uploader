import Daemon from './IDaemon';
import MyEventEmitter, { EventRecord } from './MyEventEmitter';

export default abstract class BaseDaemon<E extends EventRecord> extends MyEventEmitter<E>
  implements Daemon<E> {
  private isDaemonRunning = false;

  protected abstract execute(): void;

  start(): void {
    if (this.isDaemonRunning) return;
    this.isDaemonRunning = true;
    setImmediate(this.execute.bind(this));
  }

  stop(): void {
    this.isDaemonRunning = false;
  }

  isRunning(): boolean {
    return this.isDaemonRunning;
  }
}
