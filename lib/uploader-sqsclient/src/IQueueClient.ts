import {
  DeleteMessageBatchCommand,
  DeleteMessageBatchCommandOutput,
  ReceiveMessageCommand,
  ReceiveMessageCommandOutput,
  SendMessageCommand,
  SendMessageCommandOutput,
} from '@aws-sdk/client-sqs';

export default interface QueueClient {
  send<
  Command = DeleteMessageBatchCommand | ReceiveMessageCommand | SendMessageCommand,
  Output = DeleteMessageBatchCommandOutput | ReceiveMessageCommandOutput | SendMessageCommandOutput,
  >(command: Command)
  : Promise<Output>;
}
