import QueueEvent from './QueueEvent';

export type QueueEventRecord = Record<QueueEvent, Error>;
