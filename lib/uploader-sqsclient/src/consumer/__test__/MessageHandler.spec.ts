import MessageHandler from '../MessageHandler';

const BODY = { id: 'fake-id', attr1: 'fake-value' };

class ConcreteHandler extends MessageHandler<{ id: 'fake-id', attr1: 'fake-value' }> {
  public handleMessageBody = jest.fn();
}

describe('the message handler', () => {
  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  describe.each([
    ['not provided', undefined],
    ['provided', mockLogger],
  ])('and the logger is %s', (_, logger) => {
    it('must handle the message body', async () => {
      const handler = new ConcreteHandler(logger);
      await handler.handleMessage({ Body: JSON.stringify(BODY) });
      expect(handler.handleMessageBody).toHaveBeenCalledWith(BODY);
    });

    it('must skip if message has no body', async () => {
      const handler = new ConcreteHandler(logger);
      await handler.handleMessage({});
      expect(handler.handleMessageBody).not.toHaveBeenCalled();
    });
  });
});
