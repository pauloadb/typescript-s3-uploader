import MessageHandler from '../MessageHandler';

class MockMessageHandler extends MessageHandler<object> { protected handleMessageBody = jest.fn(); }

describe('the queue consumer', () => {
  const MockCustomClient = jest.fn(() => ({ id: 'fake-queue-client' }));
  const MockErrorState = jest.fn(() => ({ id: 'fake-error-state' }));
  const MockReceivingState = jest.fn(() => ({ id: 'fake-receiving-state' }));

  const INPUT = {
    messageHandler: new MockMessageHandler(),
    queueUrl: 'fake-queue-url',
    endpointUrl: 'fake-endpoint-url',
  };

  beforeEach(() => {
    jest.mock('../../CustomQueueClient', () => MockCustomClient);
    jest.mock('../states/ReceivingMessagesState', () => MockReceivingState);
    jest.mock('../states/ErrorState', () => MockErrorState);
  });

  it('must properly create the error state', () => import('../QueueConsumer').then(({ default: QueueConsumer }) => {
    const error = new Error('Whoops');
    const consumer = new QueueConsumer<{ id: 'fake-body' }>(INPUT);
    const errorState = consumer.createErrorState(error);
    expect(errorState).toStrictEqual({ id: 'fake-error-state' });
    expect(MockErrorState).toHaveBeenCalledWith(consumer, error);
  }));

  it('must properly create the initial state', () => import('../QueueConsumer').then(({ default: QueueConsumer }) => {
    const consumer = new QueueConsumer<{ id: 'fake-body' }>(INPUT);
    const initialState = consumer.createInitialState();
    expect(initialState).toStrictEqual({ id: 'fake-receiving-state' });
    expect(MockReceivingState).toHaveBeenCalledWith(consumer, INPUT, { id: 'fake-queue-client' });
  }));
});
