import { Message } from '@aws-sdk/client-sqs';

export default class MessageHandlerError extends Error {
  constructor(
    public readonly queueMessage?: Message,
    public readonly cause?: Error,
  ) {
    super('Failed to handle message');
  }
}
