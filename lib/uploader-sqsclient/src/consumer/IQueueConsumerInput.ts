import Logger from '../ILogger';
import MessageHandler from './MessageHandler';

export default interface QueueConsumerInput<MessageBody> {
  logger?: Logger,
  queueUrl: string,
  endpointUrl?: string,
  messageHandler: MessageHandler<MessageBody>
}
