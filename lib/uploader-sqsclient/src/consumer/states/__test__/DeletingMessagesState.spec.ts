import MessageHandler from '../../MessageHandler';
import MessageHandlerError from '../../MessageHandlerError';
import QueueEvent from '../../QueueEvent';

class MockMessageHandler extends MessageHandler<object> { protected handleMessageBody = jest.fn(); }

describe('the deleting messages state', () => {
  const mockContext = {
    changeState: jest.fn(),
    createInitialState: jest.fn(),
    createErrorState: jest.fn(),
    emit: jest.fn(),
    off: jest.fn(),
    on: jest.fn(),
    start: jest.fn(),
    stop: jest.fn(),
    isRunning: jest.fn(),
  };

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  const mockQueueClient = { send: jest.fn() };

  const mockDeleteMessageBatchCmd = { id: 'fake-command' };
  const MockDeleteMessageBatchCmd = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
    mockContext.createErrorState.mockImplementation((error: Error) => ({ id: 'error-state', error }));
    mockLogger.child.mockReturnValue(mockLogger);
    MockDeleteMessageBatchCmd.mockReturnValue(mockDeleteMessageBatchCmd);
    jest.mock('@aws-sdk/client-sqs', () => ({
      DeleteMessageBatchCommand: MockDeleteMessageBatchCmd,
      Message: {},
    }));
  });

  describe('when executing', () => {
    const MESSAGES = [
      { MessageId: '123', ReceiptHandle: 'ABC', Attr3: 'value1' },
      { MessageId: '456', ReceiptHandle: 'DEF', Attr3: 'value2' },
    ];

    describe.each([
      ['provided', mockLogger],
      ['not provided', undefined],
    ])('and the logger is %s', (_, logger) => {
      const INPUT = {
        messageHandler: new MockMessageHandler(),
        queueUrl: 'fake-queue-url',
        endpointUrl: 'fake-endpoint-url',
        logger,
      };

      it('must change the state to error if the underlying operation fails', () => import('../DeletingMessagesState').then(async ({ default: DeletingMessagesState }) => {
        const error = new Error('Whoops');
        mockQueueClient.send.mockRejectedValue(error);
        await new DeletingMessagesState(mockContext, INPUT, mockQueueClient, MESSAGES).execute();
        expect(mockContext.changeState).toHaveBeenCalledWith({ error, id: 'error-state' });
      }));

      describe('and the underlying operation succeeds', () => {
        it('must properly create the delete message batch command', () => import('../DeletingMessagesState').then(async ({ default: DeletingMessagesState }) => {
          mockQueueClient.send.mockReturnValue({});
          await new DeletingMessagesState(mockContext, INPUT, mockQueueClient, MESSAGES).execute();
          expect(MockDeleteMessageBatchCmd).toHaveBeenCalledWith({
            Entries: [
              { Id: '123', ReceiptHandle: 'ABC' },
              { Id: '456', ReceiptHandle: 'DEF' },
            ],
            QueueUrl: INPUT.queueUrl,
          });
        }));

        it('must emit message error events for each failed deletion', () => import('../DeletingMessagesState').then(async ({ default: DeletingMessagesState }) => {
          mockQueueClient.send.mockReturnValue({ Failed: [{ Id: MESSAGES[0].MessageId, Code: 'AABB', Message: 'Failed' }, { Id: MESSAGES[1].MessageId }] });
          await new DeletingMessagesState(mockContext, INPUT, mockQueueClient, MESSAGES).execute();
          expect(mockContext.emit).toHaveBeenCalledTimes(2);
          expect(mockContext.emit).toHaveBeenCalledWith(
            QueueEvent.MESSAGE_ERRORED,
            new MessageHandlerError(
              MESSAGES[0],
              new Error('Failed to delete message from the queue: [AABB] Failed'),
            ),
          );

          expect(mockContext.emit).toHaveBeenCalledWith(
            QueueEvent.MESSAGE_ERRORED,
            new MessageHandlerError(
              MESSAGES[1],
              new Error('Failed to delete message from the queue: [N/A]'),
            ),
          );
        }));
      });
    });
  });
});
