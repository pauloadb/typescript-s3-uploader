import MessageHandler from '../../MessageHandler';

class MockMessageHandler extends MessageHandler<object> { protected handleMessageBody = jest.fn(); }

describe('the receiving messages state', () => {
  const mockContext = {
    changeState: jest.fn(),
    createInitialState: jest.fn(),
    createErrorState: jest.fn(),
    emit: jest.fn(),
    off: jest.fn(),
    on: jest.fn(),
    start: jest.fn(),
    stop: jest.fn(),
    isRunning: jest.fn(),
  };

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  const mockQueueClient = { send: jest.fn() };

  const mockReceiveMessageCmd = { id: 'fake-command' };
  const MockReceiveMessageCmd = jest.fn();

  const mockHandlingState = { id: 'fake-handling-state' };
  const MockHandlingState = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
    mockContext.createErrorState.mockImplementation((error: Error) => ({ id: 'error-state', error }));
    mockLogger.child.mockReturnValue(mockLogger);
    MockReceiveMessageCmd.mockReturnValue(mockReceiveMessageCmd);
    jest.mock('@aws-sdk/client-sqs', () => ({ ReceiveMessageCommand: MockReceiveMessageCmd, ReceiveMessageCommandOutput: {} }));
    MockHandlingState.mockReturnValue(mockHandlingState);
    jest.mock('../HandlingMessagesState', () => MockHandlingState);
  });

  describe('when executing', () => {
    describe.each([
      ['provided', mockLogger],
      ['not provided', undefined],
    ])('and the logger is %s', (_, logger) => {
      const mockInput = {
        messageHandler: new MockMessageHandler(),
        queueUrl: 'fake-queue-url',
        endpointUrl: 'fake-endpoint-url',
        logger,
      };

      it.each([
        ['is empty', {}],
        ['has no messages', { Messages: [] }],
      ])('must change the state to current if the response %s', (__, messages) => import('../ReceivingMessagesState')
        .then(async ({ default: ReceivingMessagesState }) => {
          mockQueueClient.send.mockResolvedValue({ Messages: messages });
          const state = new ReceivingMessagesState(mockContext, mockInput, mockQueueClient);
          await state.execute();
          expect(mockContext.changeState).toHaveBeenCalledWith(state);
        }));

      it('must change the state to handling messages if response is not empty', () => import('../ReceivingMessagesState')
        .then(async ({ default: ReceivingMessagesState }) => {
          const messages = [{ id: 1 }, { id: 2 }];
          mockQueueClient.send.mockResolvedValue({ Messages: messages });
          await new ReceivingMessagesState(mockContext, mockInput, mockQueueClient).execute();
          expect(mockContext.changeState).toHaveBeenCalledWith(mockHandlingState);
          expect(MockHandlingState)
            .toHaveBeenCalledWith(mockContext, mockInput, mockQueueClient, messages);
        }));

      it('must change the state to error if the underlying operation fails', () => import('../ReceivingMessagesState')
        .then(async ({ default: ReceivingMessagesState }) => {
          const error = new Error('Whoops');
          mockQueueClient.send.mockRejectedValue(error);
          await new ReceivingMessagesState(mockContext, mockInput, mockQueueClient).execute();
          expect(mockContext.changeState).toHaveBeenCalledWith({ id: 'error-state', error });
        }));
    });
  });
});
