import MessageHandler from '../../MessageHandler';
import MessageHandlerError from '../../MessageHandlerError';
import QueueEvent from '../../QueueEvent';

class MockMessageHandler extends MessageHandler<object> {
  public handleMessage = jest.fn();

  protected handleMessageBody = jest.fn();
}

describe('the handling messages state', () => {
  const MockDeletingMessagesState = jest.fn();
  const MockErrorState = jest.fn();

  const mockContext = {
    changeState: jest.fn(),
    createInitialState: jest.fn(),
    createErrorState: jest.fn(),
    emit: jest.fn(),
    off: jest.fn(),
    on: jest.fn(),
    start: jest.fn(),
    stop: jest.fn(),
    isRunning: jest.fn(),
  };

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  const mockQueueClient = { send: jest.fn() };

  beforeEach(() => {
    MockDeletingMessagesState.mockImplementation((_, __, ___, toDelete: []) => ({ id: 'deleting-state', toDelete }));
    mockContext.createInitialState.mockReturnValue({ id: 'initial-state' });
    MockErrorState.mockReturnValue({ id: 'error-state' });
    jest.mock('../DeletingMessagesState', () => MockDeletingMessagesState);
    jest.mock('../ErrorState', () => MockDeletingMessagesState);
  });

  describe('when executing', () => {
    const messages = [{}, { Body: '{ a: 1 }' }, { Body: '{ a: 2 }' }];
    const error = new Error('Whoops');

    describe.each([
      ['provided', mockLogger],
      ['not provided', undefined],
    ])('and the logger is %s', (_, logger) => {
      const mockInput = {
        messageHandler: new MockMessageHandler(),
        queueUrl: 'fake-queue-url',
        endpointUrl: 'fake-endpoint-url',
        logger,
      };

      describe('and handling some messages succeeded', () => {
        beforeEach(() => import('../HandlingMessagesState').then(async ({ default: HandlingMessagesState }) => {
          mockInput.messageHandler.handleMessage
            .mockImplementationOnce(async () => {})
            .mockImplementationOnce(async () => { throw error; });
          await new HandlingMessagesState(
            mockContext,
            mockInput,
            mockQueueClient,
            messages,
          ).execute();
        }));

        it('must handle all valid messages', () => {
          expect(mockInput.messageHandler.handleMessage).toHaveBeenCalledWith(messages[1]);
          expect(mockInput.messageHandler.handleMessage).toHaveBeenCalledWith(messages[2]);
        });

        it('must emit message error events for each message', () => {
          expect(mockContext.emit).toHaveBeenCalledWith(
            QueueEvent.MESSAGE_ERRORED,
            new MessageHandlerError(messages[1], error),
          );
          expect(mockContext.emit).toHaveBeenCalledWith(
            QueueEvent.MESSAGE_ERRORED,
            new MessageHandlerError(messages[2], error),
          );
        });

        it('must properly create the deleting messages state', () => {
          expect(MockDeletingMessagesState)
            .toHaveBeenCalledWith(mockContext, mockInput, mockQueueClient, [messages[1]]);
        });

        it('must change the state to deleting successful messages', () => {
          expect(mockContext.changeState).toHaveBeenCalledWith({ id: 'deleting-state', toDelete: [messages[1]] });
        });
      });

      describe('and handling all messages fails', () => {
        beforeEach(() => import('../HandlingMessagesState').then(async ({ default: HandlingMessagesState }) => {
          mockInput.messageHandler.handleMessage
            .mockRejectedValue(error);
          await new HandlingMessagesState(
            mockContext,
            mockInput,
            mockQueueClient,
            messages,
          ).execute();
        }));

        it('must change the state to the initial one', () => {
          expect(mockContext.changeState).toHaveBeenCalledWith({ id: 'initial-state' });
        });
      });
    });
  });
});
