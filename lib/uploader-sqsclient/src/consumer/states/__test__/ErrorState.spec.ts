import QueueEvent from '../../QueueEvent';

describe('the error state', () => {
  const mockContext = {
    changeState: jest.fn(),
    createInitialState: jest.fn(),
    createErrorState: jest.fn(),
    emit: jest.fn(),
    off: jest.fn(),
    on: jest.fn(),
    start: jest.fn(),
    stop: jest.fn(),
    isRunning: jest.fn(),
  };

  beforeEach(() => {
    mockContext.createInitialState.mockImplementation(() => ({ id: 'initial-state' }));
  });

  describe('when executing', () => {
    const ERROR = new Error('Whoops');

    beforeEach(() => import('../ErrorState').then(async ({ default: ErrorState }) => {
      await new ErrorState(mockContext, ERROR).execute();
    }));

    it('must emit an error event', () => {
      expect(mockContext.emit).toHaveBeenCalledWith(QueueEvent.ERROR, ERROR);
    });

    it('must make daemon switch back to initial state', () => {
      expect(mockContext.changeState).toHaveBeenCalledWith({ id: 'initial-state' });
    });
  });
});
