import { DaemonState, DaemonStateContext } from 'uploader-utils';
import QueueEvent from '../QueueEvent';
import { QueueEventRecord } from '../IQueueEventRecord';

export default class ErrorState implements DaemonState<QueueEventRecord> {
  constructor(
    public daemon: DaemonStateContext<QueueEventRecord>,
    private error: Error,
  ) {}

  async execute(): Promise<void> {
    this.daemon.emit(QueueEvent.ERROR, this.error);
    this.daemon.changeState(this.daemon.createInitialState());
  }
}
