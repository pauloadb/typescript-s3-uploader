import { ReceiveMessageCommand, ReceiveMessageCommandOutput } from '@aws-sdk/client-sqs';
import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import HandlingMessagesState from './HandlingMessagesState';
import QueueConsumerInput from '../IQueueConsumerInput';
import { QueueEventRecord } from '../IQueueEventRecord';
import QueueClient from '../../IQueueClient';

export default class ReceivingMessagesState<MessageBody> implements DaemonState<QueueEventRecord> {
  constructor(
    public daemon: DaemonStateContext<QueueEventRecord>,
    private input: QueueConsumerInput<MessageBody>,
    private queueClient: QueueClient,
  ) {
  }

  async execute(): Promise<void> {
    try {
      this.input.logger?.info('Trying to get new messages from the SQS queue...');
      const data: ReceiveMessageCommandOutput = await this.queueClient.send(
        new ReceiveMessageCommand({
          MaxNumberOfMessages: 1,
          QueueUrl: this.input.queueUrl,
          WaitTimeSeconds: 10,
        }),
      );
      this.input.logger?.info(`Received ${data.Messages?.length || 0} message(s) from the SQS queue`);

      if (!data.Messages || !data.Messages.length) {
        this.daemon.changeState(this);
      } else {
        this.daemon.changeState(new HandlingMessagesState(
          this.daemon,
          this.input,
          this.queueClient,
          data.Messages,
        ));
      }
    } catch (err) {
      const error = getError(err);
      this.input.logger?.error('Failed to receive new messages', { cause: error });
      this.daemon.changeState(this.daemon.createErrorState(error));
    }
  }
}
