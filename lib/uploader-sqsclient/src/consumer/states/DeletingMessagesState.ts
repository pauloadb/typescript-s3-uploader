import { DeleteMessageBatchCommand, DeleteMessageBatchCommandOutput, Message } from '@aws-sdk/client-sqs';
import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import QueueClient from '../../IQueueClient';
import MessageHandlerError from '../MessageHandlerError';
import QueueConsumerInput from '../IQueueConsumerInput';
import QueueEvent from '../QueueEvent';
import { QueueEventRecord } from '../IQueueEventRecord';

export default class DeletingMessagesState<MessageBody> implements DaemonState<QueueEventRecord> {
  constructor(
    public daemon: DaemonStateContext<QueueEventRecord>,
    private input: QueueConsumerInput<MessageBody>,
    private queueClient: QueueClient,
    private messages: Message[],
  ) {}

  async execute(): Promise<void> {
    const childLogger = this.input.logger?.child({
      queueUrl: this.input.queueUrl, messages: this.messages,
    });

    try {
      childLogger?.info('Trying to delete message batch from the SQS queue...');
      const result: DeleteMessageBatchCommandOutput = await this.queueClient.send(
        new DeleteMessageBatchCommand({
          QueueUrl: this.input.queueUrl,
          Entries: this.messages.map((message) => ({
            Id: message.MessageId, ReceiptHandle: message.ReceiptHandle,
          })),
        }),
      );
      result.Failed?.forEach((failed) => {
        this.daemon.emit(
          QueueEvent.MESSAGE_ERRORED,
          new MessageHandlerError(
            this.messages.find((message) => message.MessageId === failed.Id),
            new Error(`Failed to delete message from the queue: [${failed.Code || 'N/A'}] ${failed.Message || ''}`),
          ),
        );
      });
      childLogger?.info('Successfully deleted message batch from the queue');
      this.daemon.changeState(this.daemon.createInitialState());
    } catch (err) {
      const error = getError(err);
      childLogger?.error('Failed to delete message batch', { cause: error });
      this.daemon.changeState(this.daemon.createErrorState(error));
    }
  }
}
