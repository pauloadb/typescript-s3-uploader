import { Message } from '@aws-sdk/client-sqs';
import { DaemonState, DaemonStateContext, getError } from 'uploader-utils';
import QueueClient from '../../IQueueClient';
import MessageHandlerError from '../MessageHandlerError';
import QueueConsumerInput from '../IQueueConsumerInput';
import QueueEvent from '../QueueEvent';
import { QueueEventRecord } from '../IQueueEventRecord';
import DeletingMessagesState from './DeletingMessagesState';

export default class HandlingMessagesState<MessageBody> implements DaemonState<QueueEventRecord> {
  constructor(
    public daemon: DaemonStateContext<QueueEventRecord>,
    private input: QueueConsumerInput<MessageBody>,
    private queueClient: QueueClient,
    private messages: Message[],
  ) {}

  async execute(): Promise<void> {
    this.input.logger?.info('Handling received messages');
    const promises = new Array<Promise<void>>();
    const toDelete: Message[] = [];
    this.messages.forEach((message) => {
      if (!message.Body) return;
      promises.push(
        this.input.messageHandler.handleMessage(message)
          .then(() => { toDelete.push(message); })
          .catch((err) => {
            this.daemon.emit(
              QueueEvent.MESSAGE_ERRORED,
              new MessageHandlerError(message, getError(err)),
            );
            this.input.logger?.error('Failed to handle message', { cause: getError(err), message });
          }),
      );
    });

    await Promise.all(promises);

    if (toDelete.length) {
      this.daemon.changeState(
        new DeletingMessagesState(this.daemon, this.input, this.queueClient, toDelete),
      );
    } else {
      this.daemon.changeState(this.daemon.createInitialState());
    }
  }
}
