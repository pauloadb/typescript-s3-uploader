import { DaemonState, StateBasedDaemon } from 'uploader-utils';
import QueueClient from '../IQueueClient';
import CustomQueueClient from '../CustomQueueClient';
import QueueConsumerInput from './IQueueConsumerInput';
import { QueueEventRecord } from './IQueueEventRecord';
import ErrorState from './states/ErrorState';
import ReceivingMessagesState from './states/ReceivingMessagesState';

export default class QueueConsumer<MessageBody> extends StateBasedDaemon<QueueEventRecord> {
  private queueClient: QueueClient;

  constructor(private input: QueueConsumerInput<MessageBody>) {
    super();
    this.queueClient = new CustomQueueClient(input.endpointUrl);
  }

  createErrorState(error: Error): DaemonState<QueueEventRecord> {
    return new ErrorState(this, error);
  }

  createInitialState(): DaemonState<QueueEventRecord> {
    return new ReceivingMessagesState(this, this.input, this.queueClient);
  }
}
