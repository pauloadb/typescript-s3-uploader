import { Message } from '@aws-sdk/client-sqs';
import Logger from '../ILogger';

abstract class MessageHandler<MessageBody> {
  constructor(protected logger?: Logger) {}

  protected abstract handleMessageBody(body: MessageBody): Promise<void>;

  async handleMessage(message: Message): Promise<void> {
    if (!(message.Body)) {
      this.logger?.warn('Message has no body and wont be handled', { message });
      return;
    }
    const messageBody = JSON.parse(message.Body) as MessageBody;
    await this.handleMessageBody(messageBody);
  }
}

export default MessageHandler;
