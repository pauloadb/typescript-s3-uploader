enum QueueEvent {
  MESSAGE_ERRORED = 'message_errored',
  ERROR = 'error',
}

export default QueueEvent;
