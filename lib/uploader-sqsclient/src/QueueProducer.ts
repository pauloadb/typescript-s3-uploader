import { SendMessageCommand } from '@aws-sdk/client-sqs';
import CustomQueueClient from './CustomQueueClient';
import Logger from './ILogger';
import OutgoingMessage from './messages/IOutgoingMessage';
import QueueClient from './IQueueClient';

export default class QueueProducer {
  private queueClient: QueueClient;

  constructor(private logger?: Logger, endpointUrl?: string) {
    this.queueClient = new CustomQueueClient(endpointUrl);
  }

  async sendMessage<BodyType>(queueUrl: string, message: OutgoingMessage<BodyType>): Promise<void> {
    const childLogger = this.logger?.child({ queueUrl, message });
    try {
      childLogger?.info('Sending message to the queue');
      await this.queueClient.send(new SendMessageCommand({
        MessageBody: JSON.stringify(message.body),
        MessageDeduplicationId: message.deduplicationId,
        MessageGroupId: message.groupId,
        QueueUrl: queueUrl,
      }));
      childLogger?.info('Successfully sent message to the queue');
    } catch (err) {
      childLogger?.error('Failed to send message to the queue', { cause: err });
      throw err;
    }
  }
}
