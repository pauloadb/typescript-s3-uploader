import UploadFileMessage from '../UploadFileMessage';

describe('the upload file message', () => {
  it('must properly initialize its attributes', () => {
    const message = new UploadFileMessage(123);
    expect(message.groupId).toBe('123');
    expect(message.deduplicationId).toBe('123');
    expect(message.body).toStrictEqual({ fileId: 123 });
  });
});
