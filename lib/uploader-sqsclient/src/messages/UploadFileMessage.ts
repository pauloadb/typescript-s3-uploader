import OutgoingMessage from './IOutgoingMessage';
import UploadFileBody from './IUploadFileBody';

export default class UploadFileMessage implements OutgoingMessage<UploadFileBody> {
  groupId: string;

  deduplicationId: string;

  body: UploadFileBody;

  constructor(fileId: number) {
    this.groupId = `${fileId}`;
    this.deduplicationId = `${fileId}`;
    this.body = { fileId };
  }
}
