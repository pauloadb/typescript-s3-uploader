export default interface OutgoingMessage<BodyType> {
  readonly groupId: string;
  readonly deduplicationId: string;
  readonly body: BodyType
}
