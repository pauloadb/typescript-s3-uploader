import { SQSClient } from '@aws-sdk/client-sqs';
import QueueClient from './IQueueClient';

export default class CustomQueueClient extends SQSClient implements QueueClient {
  constructor(endpointUrl?: string) {
    super({ apiVersion: '2012-11-05', maxAttempts: 5, endpoint: endpointUrl });
  }
}
