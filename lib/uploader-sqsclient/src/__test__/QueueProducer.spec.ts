describe('the queue producer', () => {
  const ENDPOINT_URL = 'fake-endpoint';
  const QUEUE_URL = 'fake-queue-url';
  const MESSAGE_BODY = { id: 'fake-message-body' };

  const mockSendMessageCmd = { id: 'fake-command' };
  const MockSendMessageCmd = jest.fn();

  const mockCustomClient = { send: jest.fn() };
  const MockCustomClient = jest.fn();

  const mockLogger = {
    child: jest.fn(), info: jest.fn(), error: jest.fn(), debug: jest.fn(), warn: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
    mockLogger.child.mockReturnValue(mockLogger);
    MockSendMessageCmd.mockReturnValue(mockSendMessageCmd);
    MockCustomClient.mockReturnValue(mockCustomClient);
    jest.mock('@aws-sdk/client-sqs', () => ({ SendMessageCommand: MockSendMessageCmd }));
    jest.mock('../CustomQueueClient', () => MockCustomClient);
  });

  describe.each([
    ['defined', mockLogger],
    ['undefined', undefined],
  ])('when the logger is %s', (_, logger) => {
    it('must properly create the custom queue client', () => import('../QueueProducer').then(({ default: QueueProducer }) => {
      // eslint-disable-next-line no-new
      new QueueProducer(logger, ENDPOINT_URL);
      expect(MockCustomClient).toHaveBeenCalled();
    }));

    describe('and it is sending a message to the queue', () => {
      const MESSAGE = { groupId: 'fake-group-id', deduplicationId: 'fake-dedup-id', body: MESSAGE_BODY };

      it('must properly create the send message command', () => import('../QueueProducer').then(async ({ default: QueueProducer }) => {
        const producer = new QueueProducer(logger, ENDPOINT_URL);
        await producer.sendMessage(QUEUE_URL, MESSAGE);
        expect(MockSendMessageCmd).toHaveBeenCalledWith({
          MessageBody: JSON.stringify(MESSAGE_BODY),
          MessageDeduplicationId: MESSAGE.deduplicationId,
          MessageGroupId: MESSAGE.groupId,
          QueueUrl: QUEUE_URL,
        });
      }));

      it('must throw the error if some happens', () => import('../QueueProducer').then(async ({ default: QueueProducer }) => {
        const producer = new QueueProducer(logger, ENDPOINT_URL);
        const error = new Error('Whoops');
        mockCustomClient.send.mockRejectedValue(error);
        try {
          await producer.sendMessage(QUEUE_URL, MESSAGE);
          throw new Error('Test failed');
        } catch (err) {
          expect(err).toStrictEqual(error);
        }
      }));
    });
  });
});
