describe('the custom queue client', () => {
  const MockSqsClient = jest.fn();

  beforeEach(() => {
    jest.mock('@aws-sdk/client-sqs', () => ({ SQSClient: MockSqsClient }));
  });

  it('must properly call the superclass constructor', () => import('../CustomQueueClient').then(({ default: CustomQueueClient }) => {
    const endpointUrl = 'fake-endpoint-url';
    // eslint-disable-next-line no-new
    new CustomQueueClient(endpointUrl);
    expect(MockSqsClient).toHaveBeenCalledWith({ apiVersion: '2012-11-05', maxAttempts: 5, endpoint: endpointUrl });
  }));
});
