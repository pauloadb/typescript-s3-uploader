# typescript-s3-uploader

## Motivation

The motivation behind this project was for me to learn and practice a bit of Typescript using it in a real world application, besides sharing some advanced code for uploading files to S3 buckets.

## Components

- **uploader-api** - API that allows the upload of files to a S3 bucket and later search and download it (protected using Okta OpenID Connect)
- **uploader-daemon** - Queue consuming daemon that processes the file upload requests
- **uploader-front** - React SPA providing UI for the API features

## Libraries

- **Data Model** - Persistence data model and database migrations management using Prisma ORM
- **Logging** - Common logging functionality
- **SQS Client** - Common client logic for AWS SQS queue provider
- **S3 Client** - Common client logic for AWS S3 storage provider

## Main Third-Party Dependencies

- [Node.js](https://nodejs.org/en/)
- [Typescript](https://www.typescriptlang.org/)
- [Turborepo](https://turbo.build/repo)
- [Express.js](https://expressjs.com/)
- [Prisma ORM](https://www.prisma.io/)
- [Axios](https://github.com/axios/axios)
- [AWS SDK for JavaScript](https://aws.amazon.com/sdk-for-javascript/)
- [React](https://reactjs.org/)
- [React Bootstrap](https://react-bootstrap.github.io/)
- [Winston](https://github.com/winstonjs/winston)
- [Jest](https://jestjs.io/)

## How It Works

When a request is sent to the upload file API endpoint with the source URL, the application will create a file record in the database and send a message to the upload SQS queue.

Later, the daemon will consume messages from the SQS queue and transfer the files to the S3 bucket from their source URLs. In case the download server supports partial downloads (byte ranges), the daemon will transfer the file in a multipart download and upload manner. Otherwise, the upload will still be multipart, but based on a streamed source. The last approach is less efficient, since the data related to a given part will only be loaded (and available for upload) once all previous parts have been read, due to the sequential stream data flow.

It's also worth mentioning that the application provides fault tolerance in different levels: HTTP client (when downloading the file); AWS SDK commands (including the ones related to the multi part upload); SQS queue message processing (queue messages are retried 3 times before they are sent to the dead-letter queue).

Finally, Both API and front-end are protected using Okta OpenID Connect authentication and the upload file functionality will only allow a user to manipulate its own files.

## Installing Dependencies

To install the project dependencies for all components, simply run (at the project root folder):

```bash
yarn install
```

## Infrastructure

The S3 uploader ecosystem requires three infrastructure services: AWS SQS as the queue provider, AWS S3 as the cloud storage and PostgreSQL as the DBMS.

### Running Locally

To run the infrastructure locally, you will need the following tools:

- [Docker](https://www.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

To run the required AWS infrastructure locally without costs, the application uses [Localstack](https://localstack.cloud/), along with `Docker` and `Docker Compose`. 

After [installing the dependencies](#installing-dependencies), just execute the following command from the project root directory to start all the required services:

```bash
yarn run turbo run infra:start
```

And that's it. All the required services should be up and running now.

To stop the infrastructure, simply run:

```bash
yarn run turbo run infra:stop
```

Finally, to remove it:

```bash
yarn run turbo run infra:remove
```

### SQS Queue

The upload SQS queue needs to be configured with the following settings to optimize the application flow:

| FIFO | Default Visibility Timeout | Content-Based Deduplication | Retention Period | Dead-Letter Queue | Maximum Receive Count |
|---|---|---|---|---|---|
true | 4,020 seconds | true | 604,800 | true | 3 |

For the local infrastructure, the queue is already created with proper settings.

### PostgreSQL

To access the PGAdmin instance deployed as part of the local infrastructure, please navigate to the URL [http://localhost:5433](http://localhost:5433) and type the following information:

- Username: `user@domain.com`
- Password: `password`

When later it asks for the password for the user `uploader_db_user`, simply type `password` once again and press enter.

## Running The Application

To run the application, first make sure that you have [installed the project dependencies](#installing-dependencies) and also that the [infra was properly started](#running-locally).

Next, let's deploy the database schema and migrations to the database server by running the following command (please give it a few seconds after starting the infra so the database server will be ready to accept connections):

```bash
yarn run turbo run db:deploy
```

Finally, let's build the docker images and run the containers for them by running:

```bash
yarn run turbo run docker:run
```

Notice that the docker build command may take some time to finish depending on your internet bandwidth.

To remove the application containers, simply execute:

```bash
yarn run turbo run docker:remove
```

## Exploring from the UI

To access the front-end application of this ecosystem, simply open a browser window and navigate to [http://localhost:3001](http://localhost:3001). You will see the following page:

![home page](https://drive.google.com/uc?export=view&id=1qQhMrCTrMVOsiI-YIY1ue9qg_JN37sCM)

### Authenticating

If you click on `Login With Okta`, you will be redirected to the external Okta authentication page:

![login page](https://drive.google.com/uc?export=view&id=1EkTbMSDGylDubDPv0af_IhDohu4X_bCx)

There are two sets of credentials that you can use to authenticate (I created two test users to demonstrate the isolation between user files):

**Login:** `contato+1@paulobichara.org` **Password:** `firstpassword`

**Login:** `contato+2@paulobichara.org` **Password:** `secondpassword`

### Uploading a New File

After authenticating, you will be redirected to the page that will allow you to manage file uploads:

![manage files page](https://drive.google.com/uc?export=view&id=1nRLBobIxxNSAOhzXsMGbq3eqcuF3JOQb)

To upload a new file to the S3 bucket, first click in the `Upload` button. The following dialog will be displayed:

![new file upload dialog](https://drive.google.com/uc?export=view&id=1HyOAozpZJoa0n4hz9eqZxD3wEBjFXCZa)

In the `Source URL` field, please type the source URL for the intended file (for now, the uploader only supports HTTP and HTTPS URLs). If the `File name` field value is not provided, the file name will be extracted from the source URL. After typing the URL and clicking in the `Save` button, the file record will be created in the database and its upload will be triggered:

![file created](https://drive.google.com/uc?export=view&id=1ioSyAubgelm2i8zmWe-1R9aDjUWaftz7)

### Downloading a File

After some time (depending on the file size you are uploading), refresh the page and you will see the file status change to `UPLOADED` and the file will now be available for download:

![file uploaded](https://drive.google.com/uc?export=view&id=1HrDeZRSzIFWua1hIr6k5JxdKampRVfN9)

To download the file, simply click in the download icon under the `Actions` column of the table.

### Deleting a File

Later, if you want to delete that file from the bucket, simply click in the delete icon (also under the `Actions` column of the table):

![file deleted](https://drive.google.com/uc?export=view&id=1rzaa0jR6AavrdxWTMecZAzSDfYmWZ8Kg)

