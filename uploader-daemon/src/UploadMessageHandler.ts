import Database, { FileStatus } from 'uploader-datamodel';
import { UploadOperation } from 'uploader-s3client';
import { MessageHandler, UploadFileBody } from 'uploader-sqsclient';
import Logger from './adapters/Logger';
import Configuration from './config/Configuration';

export default class UploadMessageHandler extends MessageHandler<UploadFileBody> {
  private database: Database;

  private config: Configuration;

  constructor() {
    super(Logger.getInstance());
    this.database = Database.getInstance();
    this.config = Configuration.getInstance();
  }

  protected async handleMessageBody(messageBody: UploadFileBody): Promise<void> {
    const file = await this.database.file.update({
      where: { id: messageBody.fileId },
      data: {
        transferStartedAt: new Date(),
        transferFailedAt: null,
        status: FileStatus.UPLOADING,
      },
    });

    try {
      await new UploadOperation({
        ...this.config.aws.s3,
        key: file.s3ObjectKey,
        sourceUrl: new URL(file.sourceUrl),
        endpointUrl: this.config.aws.endpointUrl,
        logger: this.logger,
      }).execute();

      await this.database.file.update({
        where: { id: file.id },
        data: { transferFinishedAt: new Date(), status: FileStatus.UPLOADED },
      });
    } catch (err) {
      await this.database.file.update({
        where: { id: file.id },
        data: {
          transferErrorsQty: { increment: 1 },
          transferFailedAt: new Date(),
          status: file.transferErrorsQty >= 2 ? FileStatus.FAILED : FileStatus.UPLOADING,
        },
      });
      throw err;
    }
  }
}
