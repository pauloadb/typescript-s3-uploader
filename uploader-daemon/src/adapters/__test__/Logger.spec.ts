import { LogLevel } from 'uploader-logging';
import Environment from '../../Environment';

describe('the logger adapter', () => {
  const mockConfig = {
    logging: {
      errorFile: '/path/error.log',
      outputFile: '/path/output.log',
      skipConsole: true,
    },
  };

  const MockLoggerAdapter = jest.fn();

  beforeEach(() => {
    jest.resetModules();
    jest.mock('uploader-logging', () => ({ LoggerAdapter: MockLoggerAdapter, LogLevel }));
  });

  describe.each([
    [Environment.Development, LogLevel.INFO],
    [Environment.Production, LogLevel.WARNING],
  ])('when the environment is %s', (environment, minLevel) => {
    beforeEach(() => {
      jest.mock('../../config/Configuration', () => ({ getInstance: () => ({ ...mockConfig, environment }) }));
    });

    it('must properly create the adapter instance', () => import('../Logger')
      .then(async ({ default: Logger }) => {
        Logger.getInstance();
        expect(MockLoggerAdapter).toHaveBeenCalledWith(
          minLevel,
          mockConfig.logging.errorFile,
          mockConfig.logging.outputFile,
          mockConfig.logging.skipConsole,
        );
      }));
  });
});
