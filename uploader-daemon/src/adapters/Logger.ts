import { LoggerAdapter, LogLevel } from 'uploader-logging';
import Configuration from '../config/Configuration';
import Environment from '../Environment';

const config = Configuration.getInstance();

export default class Logger extends LoggerAdapter {
  private static instance: Logger;

  private constructor() {
    super(
      config.environment === Environment.Production ? LogLevel.WARNING : LogLevel.INFO,
      config.logging.errorFile,
      config.logging.outputFile,
      config.logging.skipConsole,
    );
  }

  static getInstance(): Logger {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }
    return Logger.instance;
  }
}
