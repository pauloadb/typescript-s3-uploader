import { FileStatus } from 'uploader-datamodel';

describe('the upload message handler', () => {
  const NOW = new Date().getTime();
  const FILE = { id: 123, s3ObjectKey: 'fake-key', sourceUrl: 'http://fake.url/fake.file' };

  const mockDatabase = { file: { update: jest.fn() } };

  const mockConfig = {
    aws: {
      s3: {
        bucketName: 'fake-bucket',
        queueSize: 7,
        partSize: 10 * 1024 * 1024,
      },
      endpointUrl: 'fake-endpoint-url',
    },
  };

  const mockUploadOperation = { execute: jest.fn() };
  const MockUploadOperation = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
    jest.useFakeTimers();
    jest.setSystemTime(NOW);

    jest.mock('uploader-datamodel', () => ({ __esModule: true, default: { getInstance: () => mockDatabase }, FileStatus }));
    jest.mock('../config/Configuration', () => ({ getInstance: () => mockConfig }));
    jest.mock('../adapters/Logger');

    MockUploadOperation.mockReturnValue(mockUploadOperation);
    jest.mock('uploader-s3client', () => ({ UploadOperation: MockUploadOperation }));
  });

  it('must update the file data when the transfer starts', () => import('../UploadMessageHandler')
    .then(async ({ default: UploadMessageHandler }) => {
      mockDatabase.file.update.mockImplementation(async () => {
        jest.advanceTimersByTime(5000);
        return FILE;
      });
      await new UploadMessageHandler().handleMessage({ Body: '{"fileId":123}' });
      expect(mockDatabase.file.update).toHaveBeenNthCalledWith(1, {
        where: { id: 123 },
        data: {
          transferStartedAt: new Date(NOW),
          transferFailedAt: null,
          status: FileStatus.UPLOADING,
        },
      });
    }));

  it('must update the file data when the transfer finishes successfully', () => import('../UploadMessageHandler')
    .then(async ({ default: UploadMessageHandler }) => {
      mockDatabase.file.update.mockImplementation(async () => {
        jest.advanceTimersByTime(5000);
        return FILE;
      });
      await new UploadMessageHandler().handleMessage({ Body: '{"fileId":123}' });
      expect(mockDatabase.file.update).toHaveBeenNthCalledWith(2, {
        where: { id: FILE.id },
        data: { transferFinishedAt: new Date(NOW + 5000), status: FileStatus.UPLOADED },
      });
    }));

  it.each([
    [0, FileStatus.UPLOADING],
    [2, FileStatus.FAILED],
  ])('must update the file data and throw error when the transfer fails', (transferErrorsQty, status) => import('../UploadMessageHandler')
    .then(async ({ default: UploadMessageHandler }) => {
      mockDatabase.file.update.mockImplementation(async () => {
        jest.advanceTimersByTime(5000);
        return { ...FILE, transferErrorsQty };
      });

      const error = new Error('Whoops');
      mockUploadOperation.execute.mockRejectedValue(error);
      try {
        await new UploadMessageHandler().handleMessage({ Body: '{"fileId":123}' });
        throw new Error('Test failed');
      } catch (err) {
        expect(mockDatabase.file.update).toHaveBeenNthCalledWith(2, {
          where: { id: FILE.id },
          data: {
            transferErrorsQty: { increment: 1 },
            transferFailedAt: new Date(NOW + 5000),
            status,
          },
        });
      }
    }));

  it('must transfer the file', () => import('../UploadMessageHandler')
    .then(async ({ default: UploadMessageHandler }) => {
      mockDatabase.file.update.mockImplementation(async () => FILE);
      await new UploadMessageHandler().handleMessage({ Body: '{"fileId":123}' });
      expect(MockUploadOperation).toHaveBeenCalledWith({
        ...mockConfig.aws.s3,
        key: FILE.s3ObjectKey,
        sourceUrl: new URL(FILE.sourceUrl),
        endpointUrl: mockConfig.aws.endpointUrl,
      });
      expect(mockUploadOperation.execute).toHaveBeenCalled();
    }));
});
