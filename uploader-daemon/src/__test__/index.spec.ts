import UploadFileBody, { QueueEvent } from 'uploader-sqsclient';

describe('the index file', () => {
  const mockConfig = {
    aws: {
      endpointUrl: 'fake-endpoint-url',
      sqs: { uploadQueueUrl: 'fake-queue-url' },
    },
  };

  const mockUploadHandler = { id: 'fake-upload-handler' };
  const MockUploadHandler = jest.fn();

  const mockQueueConsumer = {
    start: jest.fn(),
    stop: jest.fn(),
    isRunning: jest.fn(),
    on: jest.fn(),
  };
  const MockQueueConsumer = jest.fn();

  beforeEach(() => {
    jest.resetModules();

    MockQueueConsumer.mockReturnValue(mockQueueConsumer);
    jest.mock('uploader-sqsclient', () => ({
      __esModule: true, QueueConsumer: MockQueueConsumer, QueueEvent, UploadFileBody,
    }));

    jest.mock('../config/Configuration', () => ({ getInstance: () => mockConfig }));
    jest.mock('../adapters/Logger');

    MockUploadHandler.mockReturnValue(mockUploadHandler);
    jest.mock('../UploadMessageHandler', () => MockUploadHandler);
  });

  it('must create and start the queue consumer', () => import('../index').then(() => {
    expect(MockQueueConsumer).toHaveBeenCalledWith({
      queueUrl: mockConfig.aws.sqs.uploadQueueUrl,
      messageHandler: mockUploadHandler,
      endpointUrl: mockConfig.aws.endpointUrl,
    });
  }));
});
