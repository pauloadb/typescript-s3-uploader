import dotenv from 'dotenv';
import Environment from '../Environment';
import AwsConfiguration from './AwsConfiguration';
import LoggingConfiguration from './LoggingConfiguration';

dotenv.config();

export default class Configuration {
  private static instance: Configuration;

  readonly environment: Environment;

  readonly logging = new LoggingConfiguration();

  readonly aws = new AwsConfiguration();

  private constructor() {
    if (!process.env.DATABASE_URL) {
      throw new Error('Database URL is required and was not provided');
    }

    this.environment = process.env.NODE_ENV
      && Object.values<string>(Environment).includes(process.env.NODE_ENV)
      ? process.env.NODE_ENV as Environment
      : Environment.Development;
  }

  static getInstance() {
    if (!this.instance) {
      this.instance = new Configuration();
    }
    return this.instance;
  }
}
