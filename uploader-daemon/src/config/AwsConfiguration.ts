export interface SqsOptions {
  uploadQueueUrl: string
}

export interface S3Options {
  bucketName: string,
  partSize: number,
  queueSize: number,
}

export default class AwsConfiguration {
  endpointUrl?: string;

  sqs: SqsOptions;

  s3: S3Options;

  constructor() {
    if (!process.env.AWS_SQS_UPLOAD_QUEUE_URL) {
      throw new Error('The upload queue URL is required and was not provided');
    }

    if (!process.env.AWS_S3_BUCKET_NAME) {
      throw new Error('The bucket name is required and was not provided');
    }

    this.endpointUrl = process.env.AWS_ENDPOINT_URL;
    this.sqs = {
      uploadQueueUrl: process.env.AWS_SQS_UPLOAD_QUEUE_URL,
    };

    this.s3 = {
      bucketName: process.env.AWS_S3_BUCKET_NAME,
      partSize: process.env.AWS_S3_PART_SIZE
        ? Number.parseInt(process.env.AWS_S3_PART_SIZE, 10)
        : 20 * 1024 * 1024,
      queueSize: process.env.AWS_S3_QUEUE_SIZE
        ? Number.parseInt(process.env.AWS_S3_QUEUE_SIZE, 10)
        : 5,
    };
  }
}
