export default class LoggingConfiguration {
  errorFile: string;

  outputFile: string;

  skipConsole: boolean;

  constructor() {
    this.errorFile = process.env.LOGGING_ERROR_FILE || 'logs/error.log';
    this.outputFile = process.env.LOGGING_OUTPUT_FILE || 'logs/output.log';
    this.skipConsole = process.env.LOGGING_SKIP_CONSOLE === 'true';
  }
}
