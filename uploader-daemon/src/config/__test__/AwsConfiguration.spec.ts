describe('The AWS configuration class', () => {
  const VALID_ENV = {
    AWS_SQS_UPLOAD_QUEUE_URL: 'http://some-fake-url',
    AWS_S3_BUCKET_NAME: 'fake-bucket',
    AWS_ENDPOINT_URL: 'http://fake-endpoint-url',
  };

  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = { ...VALID_ENV };
    jest.resetAllMocks();
    jest.resetModules();
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it.each([
    ['AWS_SQS_UPLOAD_QUEUE_URL', 'upload queue URL'],
    ['AWS_S3_BUCKET_NAME', 'bucket name'],
  ])('must throw an error when the env var %s is not defined', (envVarName, description) => import('../AwsConfiguration').then(({ default: AwsConfiguration }) => {
    delete process.env[envVarName];
    // eslint-disable-next-line no-new
    expect(() => new AwsConfiguration()).toThrow(new Error(`The ${description} is required and was not provided`));
  }));

  it('must create a a configuration instance with a valid environment', () => import('../AwsConfiguration').then(({ default: AwsConfiguration }) => {
    expect(() => new AwsConfiguration()).not.toThrowError();
  }));

  it('must properly initialize the configuration attributes', () => import('../AwsConfiguration').then(({ default: AwsConfiguration }) => {
    const config = new AwsConfiguration();
    expect(config.endpointUrl).toBe(VALID_ENV.AWS_ENDPOINT_URL);
    expect(config.s3.bucketName).toBe(VALID_ENV.AWS_S3_BUCKET_NAME);
    expect(config.sqs.uploadQueueUrl).toBe(VALID_ENV.AWS_SQS_UPLOAD_QUEUE_URL);
  }));

  it.each([
    [undefined, 20 * 1024 * 1024],
    [50 * 1024 * 1024, 50 * 1024 * 1024],
  ])('must initialize the part size properly when the provided is %s', (partSize, finalPartSize) => import('../AwsConfiguration')
    .then(({ default: AwsConfiguration }) => {
      if (partSize) process.env.AWS_S3_PART_SIZE = `${partSize}`;
      else delete process.env.AWS_S3_PART_SIZE;
      const config = new AwsConfiguration();
      expect(config.s3.partSize).toBe(finalPartSize);
    }));

  it.each([
    [undefined, 5],
    [10, 10],
  ])('must initialize the queue size properly when the provided is %s', (queueSize, finalQueueSize) => import('../AwsConfiguration')
    .then(({ default: AwsConfiguration }) => {
      if (queueSize) process.env.AWS_S3_QUEUE_SIZE = `${queueSize}`;
      else delete process.env.AWS_S3_QUEUE_SIZE;
      const config = new AwsConfiguration();
      expect(config.s3.queueSize).toBe(finalQueueSize);
    }));
});
