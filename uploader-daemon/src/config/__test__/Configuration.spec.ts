const mockDotEnv = { config: jest.fn() };
jest.mock('dotenv', () => mockDotEnv);

const MockAwsConfig = jest.fn().mockReturnValue({ id: 'aws-config' });
jest.mock('../AwsConfiguration', () => MockAwsConfig);

const MockLoggingConfig = jest.fn().mockReturnValue({ id: 'logging-config' });
jest.mock('../LoggingConfiguration', () => MockLoggingConfig);

describe('the configuration class', () => {
  const VALID_ENV = {
    DATABASE_URL: 'url://fake-db.url',
    NODE_ENV: 'prod',
  };

  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = { ...VALID_ENV };
    jest.resetModules();
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it('must throw an error if the database URL is not provided', () => import('../Configuration').then(({ default: Configuration }) => {
    delete process.env.DATABASE_URL;
    expect(() => Configuration.getInstance()).toThrowError('Database URL is required and was not provided');
  }));

  it.each([
    ['prod', 'prod'],
    ['dev', 'dev'],
    ['invalid', 'dev'],
  ])(
    'must properly populate the instance attributes when the environment is %s',
    (env, finalEnv) => import('../Configuration').then(({ default: Configuration }) => {
      process.env.NODE_ENV = env;
      const configuration = Configuration.getInstance();
      expect(configuration.aws).toStrictEqual({ id: 'aws-config' });
      expect(configuration.aws).toStrictEqual({ id: 'aws-config' });
      expect(configuration.environment).toStrictEqual(finalEnv);
      expect(configuration.logging).toStrictEqual({ id: 'logging-config' });
    }),
  );
});
