import { QueueConsumer, UploadFileBody } from 'uploader-sqsclient';
import Logger from './adapters/Logger';
import Configuration from './config/Configuration';
import UploadMessageHandler from './UploadMessageHandler';

const config = Configuration.getInstance();
const logger = Logger.getInstance();

const consumer = new QueueConsumer<UploadFileBody>({
  queueUrl: config.aws.sqs.uploadQueueUrl,
  messageHandler: new UploadMessageHandler(),
  logger,
  endpointUrl: config.aws.endpointUrl,
});

consumer.start();
