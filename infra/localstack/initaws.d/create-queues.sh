#!/bin/bash

getQueueArn() {
  local queue_name=$1
  awslocal sqs get-queue-attributes \
    --attribute-name QueueArn \
    --queue-url=http://localhost:4566/000000000000/"${queue_name}" \
    |  sed 's/"QueueArn"/\n"QueueArn"/g' \
    | grep '"QueueArn"' \
    | awk -F '"QueueArn":' '{print $2}' \
    | tr -d '"' \
    | xargs
}

createDeadLetterQueue() {
  local queue_name=$1
  awslocal sqs create-queue --queue-name "${queue_name}" --attributes "FifoQueue=true"
}

createQueue() {
  local queue_name=$1
  local dlq_arn=$2
  local visibility_timeout=$3

  awslocal sqs create-queue --queue-name "${queue_name}" \
     --attributes '{
                   "RedrivePolicy": "{\"deadLetterTargetArn\":\"'"${dlq_arn}"'\",\"maxReceiveCount\":\"3\"}",
                   "VisibilityTimeout": "'"${visibility_timeout}"'",
                   "FifoQueue": "true",
                   "ContentBasedDeduplication": "true"
                   }'
}

echo "########### Setting SQS names as env variables ###########"
UPLOAD_QUEUE=upload-queue.fifo
UPLOAD_DLQ=upload-dlq.fifo

echo "########### Creating DLQ ###########"
createDeadLetterQueue "${UPLOAD_DLQ}"

echo "########### ARN for DLQ ###########"
UPLOAD_DLQ_ARN=$(getQueueArn "${UPLOAD_DLQ}")

echo "########### Creating Source Queues ###########"
createQueue "${UPLOAD_QUEUE}" "${UPLOAD_DLQ_ARN}" "4020"

echo "########### Listing Queues ###########"
awslocal sqs list-queues
