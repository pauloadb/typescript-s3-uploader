module.exports = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.ts',
    '!**/I[A-Z]*.ts',
    '!**/node_modules/**',
    '!**/vendor/**',
    '!**/build/**',
  ],
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  preset: 'ts-jest',
  testEnvironment: 'node',
  setupFiles: [
    'dotenv/config',
  ],
  moduleNameMapper: {
    uuid: require.resolve('uuid'),
  },
};
