enum Environment {
  Development = 'dev',
  Production = 'prod',
}

export default Environment;
