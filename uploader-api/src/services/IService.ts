export default interface Service<Input, Output> {
  input: Input;

  execute(): Promise<Output>
}
