import Database, { File, FileStatus } from 'uploader-datamodel';
import { getError } from 'uploader-utils';
import Logger from '../../adapters/Logger';
import ApiError from '../../ApiError';
import Service from '../IService';

export type SearchOutput = {
  list: File[],
  total: number,
  pageNumber: number,
  pageSize: number,
  lastPage: number,
};

export type SearchInput = {
  pageNumber: number,
  pageSize: number,
  sortBy: string,
  sortOrder: 'asc' | 'desc',
  filter?: string,
  statuses?: FileStatus[],
  ownerEmail: string,
};

type FileWhereClause = {
  status?: { in: FileStatus[] },
  name?: { startsWith: string },
  owner: { email: string },
};

const logger = Logger.getInstance();
const database = Database.getInstance();

export default class SearchFilesService implements Service<SearchInput, SearchOutput> {
  constructor(public input: SearchInput) {}

  private searchForFiles(where: FileWhereClause) {
    const orderBy: { [key: string]: string } = {};
    orderBy[this.input.sortBy] = this.input.sortOrder;

    return database.file.findMany({
      where,
      skip: (this.input.pageNumber - 1) * this.input.pageSize,
      take: this.input.pageSize,
      orderBy,
    });
  }

  async execute(): Promise<SearchOutput> {
    try {
      const where: FileWhereClause = { owner: { email: this.input.ownerEmail } };

      if (Array.isArray(this.input.statuses) && this.input.statuses.length) {
        where.status = { in: this.input.statuses };
      }

      if (this.input.filter) {
        where.name = { startsWith: this.input.filter };
      }

      const [total, files] = await Promise.all([
        database.file.count({ where }),
        this.searchForFiles(where),
      ]);

      logger.info(`Found ${files.length} of ${total} files`);

      return {
        list: files,
        total,
        pageNumber: this.input.pageNumber,
        pageSize: this.input.pageSize,
        lastPage: Math.ceil(total / this.input.pageSize),
      };
    } catch (err) {
      throw new ApiError('Failed to search for files', { cause: getError(err) });
    }
  }
}
