import Database, { FileStatus } from 'uploader-datamodel';
import { DeleteFileOperation } from 'uploader-s3client';
import Logger from '../../adapters/Logger';
import ApiError from '../../ApiError';
import Configuration from '../../config/Configuration';
import Service from '../IService';

const config = Configuration.getInstance();
const database = Database.getInstance();
const logger = Logger.getInstance();

type DeleteFileInput = {
  fileId: number,
  ownerEmail: string,
};

export default class DeleteFileService implements Service<DeleteFileInput, void> {
  constructor(public input: DeleteFileInput) {}

  async execute(): Promise<void> {
    const file = await database.file.findUnique({
      where: { id: this.input.fileId },
      include: { owner: true },
    });

    if (!file) {
      throw new ApiError(`Failed to find file with ID [${this.input.fileId}]`, { status: 404 });
    }

    if (file.status !== FileStatus.UPLOADED) {
      throw new ApiError(`File currently has state [${file.status}] and cannot be deleted`, { status: 400 });
    }

    if (file.owner.email !== this.input.ownerEmail) {
      throw new ApiError('Access forbidden', { status: 403 });
    }

    try {
      await new DeleteFileOperation({
        bucketName: config.aws.s3.bucketName,
        key: file.s3ObjectKey,
        endpointUrl: config.aws.endpointUrl,
        logger,
      }).execute();

      await database.file.update({
        where: { id: this.input.fileId },
        data: { deletedAt: new Date(), status: FileStatus.DELETED },
      });
    } catch (err) {
      logger.error('Failed to delete file', { cause: err });
      throw new ApiError('Failed to delete the requested file', { status: 500 });
    }
  }
}
