import { FileStatus } from 'uploader-datamodel';
import { getError } from 'uploader-utils';
import ApiError from '../../../ApiError';

describe('the upload file service', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const S3_OBJECT_KEY = '123456789';

  const mockDatabase = { file: { findMany: jest.fn(), count: jest.fn() } };
  const mockLogger = { info: jest.fn() };

  beforeEach(() => {
    jest.mock('uuid', () => ({ v1: () => S3_OBJECT_KEY }));
    jest.mock('../../../adapters/Logger', () => ({ getInstance: () => mockLogger }));
    jest.mock('uploader-datamodel', () => ({
      __esModule: true, default: ({ getInstance: () => mockDatabase }), FileStatus, File: {},
    }));
  });

  it('must throw an API error when some error happens', () => import('../SearchFilesService')
    .then(async ({ default: SearchFilesService }) => {
      mockDatabase.file.count.mockRejectedValueOnce(new Error('Whoops'));
      try {
        await new SearchFilesService({
          ownerEmail: OWNER_EMAIL, pageNumber: 3, pageSize: 10, sortBy: 'name', sortOrder: 'desc',
        }).execute();
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to search for files', { cause: getError(err) }));
      }
    }));

  describe.each([
    ['fake-filter'],
    [undefined],
  ])('when the filter is %s', (filter) => {
    describe.each([
      [[FileStatus.CREATED, FileStatus.UPLOADED]],
      [undefined],
    ])('and the statuses are %s', (statuses) => {
      beforeEach(() => import('../SearchFilesService')
        .then(async ({ default: SearchFilesService }) => {
          mockDatabase.file.count.mockResolvedValue(96);
          mockDatabase.file.findMany.mockResolvedValue({ id: 'fake-search-result' });
          await new SearchFilesService({
            ownerEmail: OWNER_EMAIL, pageNumber: 3, pageSize: 10, sortBy: 'name', sortOrder: 'desc', filter, statuses,
          }).execute();
        }));

      it('must properly count the files', () => {
        expect(mockDatabase.file.count).toHaveBeenCalledWith({
          where: {
            owner: { email: OWNER_EMAIL },
            ...(filter ? { name: { startsWith: filter } } : {}),
            ...(statuses ? { status: { in: statuses } } : {}),
          },
        });
      });

      it('must properly list the files', () => {
        expect(mockDatabase.file.findMany).toHaveBeenCalledWith({
          orderBy: { name: 'desc' },
          skip: 20,
          take: 10,
          where: {
            owner: { email: OWNER_EMAIL },
            ...(filter ? { name: { startsWith: filter } } : {}),
            ...(statuses ? { status: { in: statuses } } : {}),
          },
        });
      });
    });
  });
});
