import { File, FileStatus } from 'uploader-datamodel';
import ApiError from '../../../ApiError';

describe('the upload file service', () => {
  const FILE = {
    id: 123, sourceUrl: 'http://fake-url.com/fake.png', s3ObjectKey: 'fake-key',
  };

  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const S3_OBJECT_KEY = '123456789';

  const mockDatabase = { file: { create: jest.fn(), update: jest.fn() } };
  const mockQueueProducer = { sendMessage: jest.fn() };
  const mockConfig = { aws: { sqs: { uploadQueueUrl: 'fake-queue-url' } } };
  const mockLogger = { error: jest.fn() };

  beforeEach(() => {
    jest.mock('uuid', () => ({ v1: () => S3_OBJECT_KEY }));
    jest.mock('../../../adapters/CustomQueueProducer', () => ({ getInstance: () => mockQueueProducer }));
    jest.mock('../../../adapters/Logger', () => ({ getInstance: () => mockLogger }));
    jest.mock('../../../config/Configuration', () => ({ getInstance: () => mockConfig }));

    jest.mock('uploader-datamodel', () => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const original = jest.requireActual('uploader-datamodel');
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return {
        __esModule: true,
        ...original,
        default: { getInstance: () => mockDatabase },
      };
    });
  });

  describe('when the underlying operations succeeds', () => {
    let result: File;

    describe.each([
      ['specified', 'fake.png'],
      ['not specified', undefined],
    ])('and the file name is %s', (_, fileName) => {
      const newFile = { ...FILE, fileName };

      beforeEach(() => import('../UploadFileService')
        .then(async ({ default: UploadFileService }) => {
          mockDatabase.file.create.mockImplementation(async () => newFile);
          result = await new UploadFileService({
            sourceUrl: new URL(FILE.sourceUrl),
            fileName,
            ownerEmail: OWNER_EMAIL,
          }).execute();
        }));

      it('must return the created file', () => {
        expect(result).toStrictEqual(newFile);
      });

      it('must create a new record for the file in the database', () => {
        expect(mockDatabase.file.create).toHaveBeenCalledWith({
          data: {
            name: 'fake.png',
            owner: {
              connectOrCreate: {
                create: { email: OWNER_EMAIL },
                where: { email: OWNER_EMAIL },
              },
            },
            s3ObjectKey: S3_OBJECT_KEY,
            sourceUrl: newFile.sourceUrl,
            status: 'CREATED',
          },
        });
      });

      it('must send an upload request to the queue', () => {
        expect(mockQueueProducer.sendMessage).toHaveBeenCalledWith(
          mockConfig.aws.sqs.uploadQueueUrl,
          {
            body: {
              fileId: newFile.id,
            },
            deduplicationId: `${newFile.id}`,
            groupId: `${newFile.id}`,
          },
        );
      });
    });
  });

  it('must throw an ApiError when the file record creation fails', () => import('../UploadFileService')
    .then(async ({ default: UploadFileService }) => {
      mockDatabase.file.create.mockImplementation(async () => { throw new Error('Whoops'); });
      try {
        await new UploadFileService({ sourceUrl: new URL('http://fake-url.com'), ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to upload the requested file', { status: 500 }));
      }
    }));

  describe('when an error happens when sending the request to the queue', () => {
    beforeEach(() => {
      mockDatabase.file.create.mockResolvedValue(FILE);
      mockQueueProducer.sendMessage.mockRejectedValue(new Error('Whoops'));
    });

    it('must throw an ApiError', () => import('../UploadFileService')
      .then(async ({ default: UploadFileService }) => {
        try {
          await new UploadFileService({ sourceUrl: new URL('http://fake-url.com'), ownerEmail: OWNER_EMAIL }).execute();
          throw new Error('Test failed');
        } catch (err) {
          expect(err).toStrictEqual(new ApiError('Failed to upload the requested file', { status: 500 }));
        }
      }));

    it('must update the file status to FAILED', () => import('../UploadFileService')
      .then(async ({ default: UploadFileService }) => {
        try {
          await new UploadFileService({ sourceUrl: new URL('http://fake-url.com'), ownerEmail: OWNER_EMAIL }).execute();
        } catch (err) {
          // nothing to do here
        }

        expect(mockDatabase.file.update).toHaveBeenCalledWith({
          where: { id: FILE.id },
          data: {
            transferErrorsQty: 1,
            transferFailedAt: new Date(),
            status: FileStatus.FAILED,
          },
        });
      }));

    describe('and the file status update fails', () => {
      beforeEach(() => {
        mockDatabase.file.update.mockRejectedValue(new Error('Status Whoops'));
      });

      it('must throw an ApiError', () => import('../UploadFileService')
        .then(async ({ default: UploadFileService }) => {
          try {
            await new UploadFileService({ sourceUrl: new URL('http://fake-url.com'), ownerEmail: OWNER_EMAIL }).execute();
            throw new Error('Test failed');
          } catch (err) {
            expect(err).toStrictEqual(new ApiError('Failed to upload the requested file', { status: 500 }));
          }
        }));
    });
  });
});
