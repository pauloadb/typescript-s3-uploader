import { FileStatus } from 'uploader-datamodel';
import ApiError from '../../../ApiError';

describe('the download file service', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const FILE = {
    id: 123,
    name: 'fake.file',
    status: FileStatus.UPLOADED,
    s3ObjectKey: 'fake-key',
    owner: { email: OWNER_EMAIL },
  };

  const FILE_URL = new URL('http://fake-s3-url/fake.file');

  const mockDatabase = { file: { findUnique: jest.fn() } };
  const mockConfig = { aws: { s3: { bucketName: 'fake-bucket', presignedUrlHost: 'replaced-host' }, endpointUrl: 'fake-endpoint' } };
  const mockLogger = { error: jest.fn() };

  const MockGetUrlOperation = jest.fn();
  const mockGetUrlOperation = { execute: jest.fn() };

  beforeEach(() => {
    jest.mock('../../../adapters/Logger', () => ({ getInstance: () => mockLogger }));
    jest.mock('../../../config/Configuration', () => ({ getInstance: () => mockConfig }));

    MockGetUrlOperation.mockReturnValue(mockGetUrlOperation);
    jest.mock('uploader-s3client', () => ({ GetUrlOperation: MockGetUrlOperation }));
    jest.mock('uploader-datamodel', () => ({ __esModule: true, default: ({ getInstance: () => mockDatabase }), FileStatus }));
  });

  it('must throw an error if the file is not found', () => import('../DownloadFileService')
    .then(async ({ default: DownloadFileService }) => {
      try {
        await new DownloadFileService({ fileId: 123, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to find file with ID [123]', { status: 404 }));
      }
    }));

  it('must throw an error if the file is not fully uploaded', () => import('../DownloadFileService')
    .then(async ({ default: DownloadFileService }) => {
      mockDatabase.file.findUnique.mockImplementation(async () => ({ status: 'fake' }));
      try {
        await new DownloadFileService({ fileId: 123, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('File currently has state [fake] and cannot be downloaded', { status: 400 }));
      }
    }));

  describe('when the underlying operations succeeds', () => {
    let result: URL;

    beforeEach(() => import('../DownloadFileService')
      .then(async ({ default: DownloadFileService }) => {
        mockDatabase.file.findUnique.mockImplementation(async () => FILE);
        mockGetUrlOperation.execute.mockReturnValue(FILE_URL);
        result = await new DownloadFileService(
          { fileId: FILE.id, ownerEmail: OWNER_EMAIL },
        ).execute();
      }));

    it('must return the file URL', () => {
      const url = new URL(FILE_URL.toString());
      url.host = mockConfig.aws.s3.presignedUrlHost;
      expect(result).toStrictEqual(url);
    });

    it('must properly execute the get URL operation', () => {
      expect(MockGetUrlOperation).toHaveBeenCalledWith({
        bucketName: mockConfig.aws.s3.bucketName,
        fileName: FILE.name,
        key: FILE.s3ObjectKey,
        endpointUrl: mockConfig.aws.endpointUrl,
        expiresInSeconds: 3600,
        logger: mockLogger,
      });
    });
  });

  it('must throw an ApiError when some error happens', () => import('../DownloadFileService')
    .then(async ({ default: DownloadFileService }) => {
      mockGetUrlOperation.execute.mockRejectedValue(new Error('Whoops'));
      mockDatabase.file.findUnique.mockImplementation(async () => FILE);
      try {
        await new DownloadFileService({ fileId: FILE.id, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed!');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to get URL for the requested file', { status: 500 }));
      }
    }));

  it('must throw an ApiError when the user requesting the file is not the owner', () => import('../DownloadFileService')
    .then(async ({ default: DownloadFileService }) => {
      mockDatabase.file.findUnique.mockImplementation(async () => ({ ...FILE, owner: { email: 'other.email.fake' } }));
      try {
        await new DownloadFileService({ fileId: FILE.id, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed!');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Access forbidden', { status: 403 }));
      }
    }));
});
