import { FileStatus } from 'uploader-datamodel';
import ApiError from '../../../ApiError';

describe('the delete file service', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const FILE = {
    id: 123,
    name: 'fake.file',
    status: FileStatus.UPLOADED,
    s3ObjectKey: 'fake-key',
    owner: { email: OWNER_EMAIL },
  };

  const mockDatabase = { file: { findUnique: jest.fn(), update: jest.fn() } };
  const mockConfig = { aws: { s3: { bucketName: 'fake-bucket' }, endpointUrl: 'fake-endpoint' } };
  const mockLogger = { error: jest.fn() };

  const MockDeleteFileOperation = jest.fn();
  const mockDeleteFileOperation = { execute: jest.fn() };

  beforeEach(() => {
    jest.mock('../../../adapters/Logger', () => ({ getInstance: () => mockLogger }));
    jest.mock('../../../config/Configuration', () => ({ getInstance: () => mockConfig }));

    MockDeleteFileOperation.mockReturnValue(mockDeleteFileOperation);
    jest.mock('uploader-s3client', () => ({ DeleteFileOperation: MockDeleteFileOperation }));
    jest.mock('uploader-datamodel', () => ({ __esModule: true, default: ({ getInstance: () => mockDatabase }), FileStatus }));
  });

  it('must throw an error if the file is not found', () => import('../DeleteFileService')
    .then(async ({ default: DeleteFileService }) => {
      try {
        await new DeleteFileService({ fileId: 123, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to find file with ID [123]', { status: 404 }));
      }
    }));

  it('must throw an error if the file is not fully uploaded', () => import('../DeleteFileService')
    .then(async ({ default: DeleteFileService }) => {
      mockDatabase.file.findUnique.mockImplementation(async () => ({ status: 'fake' }));
      try {
        await new DeleteFileService({ fileId: 123, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('File currently has state [fake] and cannot be deleted', { status: 400 }));
      }
    }));

  describe('when the underlying operations succeeds', () => {
    const NOW = new Date();

    beforeEach(() => import('../DeleteFileService')
      .then(async ({ default: DeleteFileService }) => {
        jest.useFakeTimers();
        jest.setSystemTime(NOW);
        mockDatabase.file.findUnique.mockImplementation(async () => FILE);
        await new DeleteFileService(
          { fileId: FILE.id, ownerEmail: OWNER_EMAIL },
        ).execute();
      }));

    it('must properly execute the underlying operation', () => {
      expect(MockDeleteFileOperation).toHaveBeenCalledWith({
        bucketName: mockConfig.aws.s3.bucketName,
        key: FILE.s3ObjectKey,
        endpointUrl: mockConfig.aws.endpointUrl,
        logger: mockLogger,
      });
    });

    it('must update the database record', () => {
      expect(mockDatabase.file.update).toHaveBeenCalledWith({
        where: { id: FILE.id },
        data: { deletedAt: NOW, status: FileStatus.DELETED },
      });
    });
  });

  it('must throw an ApiError when some error happens', () => import('../DeleteFileService')
    .then(async ({ default: DeleteFileService }) => {
      mockDeleteFileOperation.execute.mockRejectedValue(new Error('Whoops'));
      mockDatabase.file.findUnique.mockImplementation(async () => FILE);
      try {
        await new DeleteFileService({ fileId: FILE.id, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed!');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Failed to delete the requested file', { status: 500 }));
      }
    }));

  it('must throw an ApiError when the user requesting the file is not the owner', () => import('../DeleteFileService')
    .then(async ({ default: DeleteFileService }) => {
      mockDatabase.file.findUnique.mockImplementation(async () => ({ ...FILE, owner: { email: 'other.email.fake' } }));
      try {
        await new DeleteFileService({ fileId: FILE.id, ownerEmail: OWNER_EMAIL }).execute();
        throw new Error('Test failed!');
      } catch (err) {
        expect(err).toStrictEqual(new ApiError('Access forbidden', { status: 403 }));
      }
    }));
});
