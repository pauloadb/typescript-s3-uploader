import path from 'path';
import Database, { File, FileStatus } from 'uploader-datamodel';
import { UploadFileMessage } from 'uploader-sqsclient';
import { v1 as uuidv1 } from 'uuid';
import CustomQueueProducer from '../../adapters/CustomQueueProducer';
import Logger from '../../adapters/Logger';
import ApiError from '../../ApiError';
import Configuration from '../../config/Configuration';
import Service from '../IService';

const config = Configuration.getInstance();
const database = Database.getInstance();
const queueProducer = CustomQueueProducer.getInstance();
const logger = Logger.getInstance();

type UploadInput = {
  fileName?: string;
  sourceUrl: URL;
  ownerEmail: string;
};

export default class UploadFileService implements Service<UploadInput, File> {
  constructor(public input: UploadInput) {}

  async execute(): Promise<File> {
    const { sourceUrl, fileName, ownerEmail } = this.input;

    let createdFile: File | undefined;
    try {
      createdFile = await database.file.create({
        data: {
          name: fileName || path.basename(sourceUrl.pathname),
          sourceUrl: sourceUrl.toString(),
          status: FileStatus.CREATED,
          s3ObjectKey: uuidv1(),
          owner: {
            connectOrCreate: {
              where: { email: ownerEmail },
              create: { email: ownerEmail },
            },
          },
        },
      });

      await queueProducer.sendMessage(
        config.aws.sqs.uploadQueueUrl,
        new UploadFileMessage(createdFile.id),
      );
    } catch (err) {
      if (createdFile?.id) {
        try {
          await database.file.update({
            where: { id: createdFile.id },
            data: {
              transferErrorsQty: 1,
              transferFailedAt: new Date(),
              status: FileStatus.FAILED,
            },
          });
        } catch (statusErr) {
          logger.error('Failed to update the created file status to FAILED', { cause: statusErr });
        }
      }

      logger.error('Failed to upload file', { cause: err });
      throw new ApiError('Failed to upload the requested file', { status: 500 });
    }

    return createdFile;
  }
}
