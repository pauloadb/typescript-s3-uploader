import Database, { FileStatus } from 'uploader-datamodel';
import { GetUrlOperation } from 'uploader-s3client';
import Logger from '../../adapters/Logger';
import ApiError from '../../ApiError';
import Configuration from '../../config/Configuration';
import Service from '../IService';

const config = Configuration.getInstance();
const database = Database.getInstance();
const logger = Logger.getInstance();

type DownloadInput = {
  fileId: number,
  ownerEmail: string,
};

export default class DownloadFileService implements Service<DownloadInput, URL> {
  constructor(public input: DownloadInput) {}

  async execute(): Promise<URL> {
    const file = await database.file.findUnique({
      where: { id: this.input.fileId },
      include: { owner: true },
    });

    if (!file) {
      throw new ApiError(`Failed to find file with ID [${this.input.fileId}]`, { status: 404 });
    }

    if (file.status !== FileStatus.UPLOADED) {
      throw new ApiError(`File currently has state [${file.status}] and cannot be downloaded`, { status: 400 });
    }

    if (file.owner.email !== this.input.ownerEmail) {
      throw new ApiError('Access forbidden', { status: 403 });
    }

    try {
      const url = await new GetUrlOperation({
        bucketName: config.aws.s3.bucketName,
        fileName: file.name,
        key: file.s3ObjectKey,
        endpointUrl: config.aws.endpointUrl,
        expiresInSeconds: 3600,
        logger,
      }).execute();

      if (config.aws.s3.presignedUrlHost) {
        // this is needed due to an open issue in localstack
        // https://github.com/localstack/localstack/issues/6301
        url.host = config.aws.s3.presignedUrlHost;
      }

      return url;
    } catch (err) {
      logger.error('Failed to retrieve file URL', { cause: err });
      throw new ApiError('Failed to get URL for the requested file', { status: 500 });
    }
  }
}
