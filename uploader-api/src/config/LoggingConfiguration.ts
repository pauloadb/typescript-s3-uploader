export default class LoggingConfiguration {
  private static instance: LoggingConfiguration;

  readonly errorFile: string;

  readonly outputFile: string;

  readonly skipConsole: boolean;

  private constructor() {
    this.errorFile = process.env.LOGGING_ERROR_FILE || 'logs/error.log';
    this.outputFile = process.env.LOGGING_OUTPUT_FILE || 'logs/output.log';
    this.skipConsole = process.env.LOGGING_SKIP_CONSOLE === 'true';
  }

  static getInstance() {
    if (!LoggingConfiguration.instance) {
      LoggingConfiguration.instance = new LoggingConfiguration();
    }
    return LoggingConfiguration.instance;
  }
}
