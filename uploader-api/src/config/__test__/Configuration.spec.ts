describe('the configuration class', () => {
  const VALID_ENV = {
    DATABASE_URL: 'url://fake-db.url',
    NODE_ENV: 'prod',
  };

  const mockDotEnv = { config: jest.fn() };

  const MockAwsConfig = { getInstance: jest.fn().mockReturnValue({ id: 'aws-config' }) };

  const MockLoggingConfig = { getInstance: jest.fn().mockReturnValue({ id: 'logging-config' }) };
  const MockOktaConfig = { getInstance: jest.fn().mockReturnValue({ id: 'okta-config' }) };

  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = { ...VALID_ENV };

    jest.resetModules();
    jest.mock('dotenv', () => mockDotEnv);
    jest.mock('../AwsConfiguration', () => MockAwsConfig);
    jest.mock('../LoggingConfiguration', () => MockLoggingConfig);
    jest.mock('../OktaConfiguration', () => MockOktaConfig);
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it('must throw an error if the database URL is not provided', () => import('../Configuration').then((module) => {
    const Configuration = module.default;
    delete process.env.DATABASE_URL;
    expect(() => Configuration.getInstance()).toThrowError('Database URL is required and was not provided');
  }));

  it.each([
    ['prod', 'prod'],
    ['dev', 'dev'],
    ['invalid', 'dev'],
  ])(
    'must properly populate the instance attributes when the environment is %s',
    (env, finalEnv) => import('../Configuration').then((module) => {
      const Configuration = module.default;
      process.env.NODE_ENV = env;
      const configuration = Configuration.getInstance();
      expect(configuration.aws).toStrictEqual({ id: 'aws-config' });
      expect(configuration.aws).toStrictEqual({ id: 'aws-config' });
      expect(configuration.environment).toStrictEqual(finalEnv);
      expect(configuration.logging).toStrictEqual({ id: 'logging-config' });
    }),
  );
});
