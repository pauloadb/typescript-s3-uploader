describe('The Okta configuration class', () => {
  const VALID_ENV = {
    OKTA_DOMAIN: 'fake.okta.domain',
  };

  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = { ...VALID_ENV };
    jest.resetModules();
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it.each([
    ['OKTA_DOMAIN', 'Okta domain'],
  ])('must throw an error when the env var %s is not defined', (envVarName, description) => import('../OktaConfiguration').then(({ default: OktaConfiguration }) => {
    delete process.env[envVarName];
    expect(() => OktaConfiguration.getInstance()).toThrow(new Error(`The ${description} is required and was not provided`));
  }));

  it('must create a a configuration instance with a valid environment', () => import('../OktaConfiguration').then(({ default: OktaConfiguration }) => {
    expect(() => OktaConfiguration.getInstance()).not.toThrowError();
  }));

  it('must properly initialize the configuration attributes', () => import('../OktaConfiguration').then(({ default: OktaConfiguration }) => {
    const config = OktaConfiguration.getInstance();
    expect(config.domain).toBe(VALID_ENV.OKTA_DOMAIN);
  }));
});
