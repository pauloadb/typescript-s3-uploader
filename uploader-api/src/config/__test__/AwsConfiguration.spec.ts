describe('The AWS configuration class', () => {
  const VALID_ENV = {
    AWS_SQS_UPLOAD_QUEUE_URL: 'http://some-fake-url',
    AWS_S3_BUCKET_NAME: 'fake-bucket',
    AWS_S3_PRESIGNED_URL_HOST: 'replaced-host',
    AWS_ENDPOINT_URL: 'http://fake-endpoint-url',
  };

  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = { ...VALID_ENV };
    jest.resetModules();
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it.each([
    ['AWS_SQS_UPLOAD_QUEUE_URL', 'upload queue URL'],
    ['AWS_S3_BUCKET_NAME', 'bucket name'],
  ])('must throw an error when the env var %s is not defined', (envVarName, description) => import('../AwsConfiguration').then((module) => {
    const AwsConfiguration = module.default;
    delete process.env[envVarName];
    expect(() => AwsConfiguration.getInstance()).toThrow(new Error(`The ${description} is required and was not provided`));
  }));

  it('must create a a configuration instance with a valid environment', () => import('../AwsConfiguration').then((module) => {
    const AwsConfiguration = module.default;
    expect(() => AwsConfiguration.getInstance()).not.toThrowError();
  }));

  it('must properly initialize the configuration attributes', () => import('../AwsConfiguration').then((module) => {
    const AwsConfiguration = module.default;
    const config = AwsConfiguration.getInstance();
    expect(config.endpointUrl).toBe(VALID_ENV.AWS_ENDPOINT_URL);
    expect(config.s3.bucketName).toBe(VALID_ENV.AWS_S3_BUCKET_NAME);
    expect(config.s3.presignedUrlHost).toBe(VALID_ENV.AWS_S3_PRESIGNED_URL_HOST);
    expect(config.sqs.uploadQueueUrl).toBe(VALID_ENV.AWS_SQS_UPLOAD_QUEUE_URL);
  }));
});
