describe('The AWS configuration class', () => {
  let oldEnv: NodeJS.ProcessEnv;

  beforeEach(() => {
    oldEnv = process.env;
    process.env = {};
    jest.resetModules();
  });

  afterEach(() => {
    process.env = oldEnv;
  });

  it.each([
    [undefined, 'logs/error.log', undefined, 'logs/output.log', undefined, false],
    ['/path/error.log', '/path/error.log', '/path/output.log', '/path/output.log', 'true', true],
  ])('properly initialize the instance attributes', (errorFile, finalErrorFile, outputFile, finalOutputFile, skipConsole, finalSkipConsole) => import('../LoggingConfiguration').then((module) => {
    const LoggingConfiguration = module.default;
    if (errorFile !== undefined) process.env.LOGGING_ERROR_FILE = errorFile;
    if (outputFile !== undefined) process.env.LOGGING_OUTPUT_FILE = outputFile;
    if (skipConsole !== undefined) process.env.LOGGING_SKIP_CONSOLE = skipConsole;
    const loggingConfig = LoggingConfiguration.getInstance();
    expect(loggingConfig.errorFile).toBe(finalErrorFile);
    expect(loggingConfig.outputFile).toBe(finalOutputFile);
    expect(loggingConfig.skipConsole).toBe(finalSkipConsole);
  }));
});
