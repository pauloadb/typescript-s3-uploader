export default class OktaConfiguration {
  private static instance: OktaConfiguration;

  readonly domain: string;

  private constructor() {
    if (!process.env.OKTA_DOMAIN) {
      throw new Error('The Okta domain is required and was not provided');
    }

    this.domain = process.env.OKTA_DOMAIN;
  }

  static getInstance() {
    if (!OktaConfiguration.instance) {
      OktaConfiguration.instance = new OktaConfiguration();
    }
    return OktaConfiguration.instance;
  }
}
