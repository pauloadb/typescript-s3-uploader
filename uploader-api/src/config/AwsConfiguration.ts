export interface SqsOptions {
  uploadQueueUrl: string
}

export interface S3Options {
  bucketName: string,
  presignedUrlHost?: string
}

export default class AwsConfiguration {
  private static instance: AwsConfiguration;

  readonly endpointUrl?: string;

  readonly sqs: SqsOptions;

  readonly s3: S3Options;

  private constructor() {
    if (!process.env.AWS_SQS_UPLOAD_QUEUE_URL) {
      throw new Error('The upload queue URL is required and was not provided');
    }

    if (!process.env.AWS_S3_BUCKET_NAME) {
      throw new Error('The bucket name is required and was not provided');
    }

    this.endpointUrl = process.env.AWS_ENDPOINT_URL;
    this.sqs = {
      uploadQueueUrl: process.env.AWS_SQS_UPLOAD_QUEUE_URL,
    };

    this.s3 = {
      bucketName: process.env.AWS_S3_BUCKET_NAME,
      presignedUrlHost: process.env.AWS_S3_PRESIGNED_URL_HOST,
    };
  }

  static getInstance() {
    if (!AwsConfiguration.instance) {
      AwsConfiguration.instance = new AwsConfiguration();
    }
    return AwsConfiguration.instance;
  }
}
