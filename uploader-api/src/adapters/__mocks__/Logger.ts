import { Logger } from 'uploader-logging';

const MockLogger = {
  getInstance: (): Logger => ({
    info: jest.fn(),
    error: jest.fn(),
    warn: jest.fn(),
    debug: jest.fn(),
    child: jest.fn(),
    http: jest.fn(),
  }),
};

export default MockLogger;
