describe('the custom queue producer adapter', () => {
  const mockConfig = { aws: { endpointUrl: 'http://fake.endpoint.url' } };
  const mockLogger = { error: jest.fn() };

  const MockQueueProducer = jest.fn();

  beforeEach(() => {
    jest.resetModules();
    jest.mock('uploader-sqsclient', () => ({ QueueProducer: MockQueueProducer }));
    jest.mock('../Logger', () => ({ getInstance: () => mockLogger }));
    jest.mock('../../config/Configuration', () => ({ getInstance: () => mockConfig }));
  });

  it('must properly create the adapter instance', () => import('../CustomQueueProducer')
    .then(async ({ default: CustomQueueProducer }) => {
      CustomQueueProducer.getInstance();
      expect(MockQueueProducer).toHaveBeenCalledWith(mockLogger, mockConfig.aws.endpointUrl);
    }));
});
