import { QueueProducer } from 'uploader-sqsclient';
import Configuration from '../config/Configuration';
import Logger from './Logger';

const config = Configuration.getInstance();

export default class CustomQueueProducer extends QueueProducer {
  private static instance: CustomQueueProducer;

  private constructor() {
    super(Logger.getInstance(), config.aws.endpointUrl);
  }

  static getInstance(): CustomQueueProducer {
    if (!CustomQueueProducer.instance) {
      CustomQueueProducer.instance = new CustomQueueProducer();
    }
    return CustomQueueProducer.instance;
  }
}
