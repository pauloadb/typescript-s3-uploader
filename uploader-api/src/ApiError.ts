interface ApiErrorOptions {
  cause?: Error;
  status?: number;
}

class ApiError extends Error {
  public cause?: Error;

  public status: number;

  constructor(public readonly message: string, readonly options: ApiErrorOptions) {
    super(message);
    this.cause = options.cause;
    this.status = options.status || 500;
  }
}

export default ApiError;
