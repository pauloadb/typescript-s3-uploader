import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import InfoRouter from './routes/info/InfoRouter';

import handleError from './middlewares/handlers/handleError';
import handleHttpLogging from './middlewares/handlers/handleHttpLogging';
import FilesRouter from './routes/files/FilesRouter';

import Configuration from './config/Configuration';
import Logger from './adapters/Logger';

const logger = Logger.getInstance();
const config = Configuration.getInstance();

export default class Server {
  protected app: express.Express = express();

  constructor() {
    this.app.use(handleHttpLogging);
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(cookieParser());
    new InfoRouter().register(this.app);
    new FilesRouter().register(this.app);
    this.app.use(handleError);
  }

  start(): void {
    logger.info(`Starting API for environment [${config.environment}]`);

    this.app.listen(3000, () => {
      logger.info('Listening on port 3000...');
    });
  }
}
