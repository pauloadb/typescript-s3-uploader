import {
  NextFunction, Request, Response, Router,
} from 'express';
import { FileStatus } from 'uploader-datamodel';
import ApiError from '../../ApiError';
import handleValidations from '../../middlewares/handlers/handleValidations';
import validateAuthentication from '../../middlewares/security/validateAuthentication';
import validateFileStatuses from '../../middlewares/validators/validateFileStatuses';
import validatePagination from '../../middlewares/validators/validatePagination';
import validateSorting from '../../middlewares/validators/validateSorting';
import SearchFilesService from '../../services/files/SearchFilesService';
import BasicRoute from '../common/BasicRoute';
import AuthenticatedRequest from '../common/IAuthenticatedRequest';

type SearchQueryParams = {
  pageNumber: string,
  pageSize: string,
  sortBy: string,
  sortOrder: 'asc' | 'desc',
  filter?: string,
  statuses?: FileStatus[]
};

export default class SearchFilesRoute extends BasicRoute {
  constructor(router: Router) {
    super(router, '/');
  }

  register(): void {
    this.router.get(
      this.path,
      validateAuthentication,
      validateFileStatuses,
      validatePagination,
      validateSorting,
      handleValidations,
      (
        req: Request<object, object, object, SearchQueryParams> & AuthenticatedRequest,
        res: Response,
        next: NextFunction,
      ): void => {
        const {
          pageNumber, pageSize, sortBy, sortOrder, filter, statuses,
        } = req.query;

        const ownerEmail = req.jwt?.sub;
        if (!ownerEmail) {
          next(new ApiError('Failed to find the user email in the request', { status: 400 }));
          return;
        }

        new SearchFilesService({
          pageNumber: Number.parseInt(pageNumber, 10),
          pageSize: Number.parseInt(pageSize, 10),
          sortBy,
          sortOrder,
          filter,
          statuses,
          ownerEmail,
        }).execute()
          .then((newFile) => {
            res.json(newFile).send();
          })
          .catch((err) => {
            next(err);
          });
      },
    );
  }
}
