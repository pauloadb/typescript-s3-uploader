import {
  NextFunction, Response, Router,
} from 'express';
import ApiError from '../../ApiError';
import handleValidations from '../../middlewares/handlers/handleValidations';
import validateAuthentication from '../../middlewares/security/validateAuthentication';
import validateFileUrl from '../../middlewares/validators/validateFileUrl';
import UploadFileService from '../../services/files/UploadFileService';
import BasicRoute from '../common/BasicRoute';
import AuthenticatedRequest from '../common/IAuthenticatedRequest';
import RequestWithBody from '../common/IRequestWithBody';

interface UploadRequestBody extends RequestWithBody, AuthenticatedRequest {
  body: { fileName?: string, sourceUrl: string }
}

class UploadFileRoute extends BasicRoute {
  constructor(router: Router) {
    super(router, '/');
  }

  register(): void {
    this.router.post(
      this.path,
      validateAuthentication,
      validateFileUrl,
      handleValidations,
      (req: UploadRequestBody, res: Response, next: NextFunction): void => {
        const { fileName, sourceUrl } = req.body;

        const ownerEmail = req.jwt?.sub;
        if (!ownerEmail) {
          next(new ApiError('Failed to find the user email in the request', { status: 400 }));
          return;
        }

        new UploadFileService({ sourceUrl: new URL(sourceUrl), fileName, ownerEmail }).execute()
          .then((newFile) => { res.json(newFile).send(); })
          .catch((err) => { next(err); });
      },
    );
  }
}

export default UploadFileRoute;
