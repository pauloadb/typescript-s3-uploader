import request from 'supertest';
import ServerWithApp from './IServerWithApp';

describe('The download file route', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const MockDownloadService = jest.fn();
  const mockDownloadService = { execute: jest.fn() };

  const mockJwtVerifier = { verifyAccessToken: jest.fn() };

  let server: ServerWithApp;

  beforeAll(() => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: { sub: OWNER_EMAIL } });
    jest.mock('@okta/jwt-verifier', () => jest.fn(() => mockJwtVerifier));

    jest.mock('../../../adapters/Logger');

    MockDownloadService.mockReturnValue(mockDownloadService);
    jest.mock('../../../services/files/DownloadFileService', () => MockDownloadService);

    return import('./TestableServer').then(({ default: TestableServer }) => {
      server = new TestableServer();
    });
  });

  it('must return the URL to the file when the service succeeds', async () => {
    mockDownloadService.execute.mockImplementation(async () => ({ id: 'fake-url' }));
    const response = await request(server.getApp())
      .get('/files/123/download')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(MockDownloadService).toHaveBeenCalledWith({ fileId: 123, ownerEmail: OWNER_EMAIL });
    expect(response.body).toStrictEqual({ url: { id: 'fake-url' } });
  });

  it('must return an error response when the service fails', async () => {
    mockDownloadService.execute.mockImplementation(async () => { throw new Error('Whoops'); });
    const response = await request(server.getApp())
      .get('/files/123/download')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(500);
    expect(response.body).toStrictEqual({ error: 'Whoops' });
  });

  it('must return an error response when the authenticated user email is not found', async () => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: {} });
    const response = await request(server.getApp())
      .get('/files/123/download')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(400);
    expect(response.body).toStrictEqual({ error: 'Failed to find the user email in the request' });
  });
});
