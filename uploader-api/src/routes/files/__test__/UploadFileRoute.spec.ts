import request from 'supertest';

interface ServerWithApp { getApp(): Express.Application }

describe('The Upload file route', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const mockJwtVerifier = { verifyAccessToken: jest.fn() };

  const MockUploadService = jest.fn();
  const mockUploadService = { execute: jest.fn() };

  let server: ServerWithApp;

  beforeAll(() => {
    jest.mock('../../../adapters/Logger');

    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: { sub: OWNER_EMAIL } });
    jest.mock('@okta/jwt-verifier', () => jest.fn(() => mockJwtVerifier));

    MockUploadService.mockReturnValue(mockUploadService);
    jest.mock('../../../services/files/UploadFileService', () => MockUploadService);

    return import('./TestableServer').then(({ default: TestableServer }) => {
      server = new TestableServer();
    });
  });

  it('must return the created file when the service succeeds', async () => {
    mockUploadService.execute.mockResolvedValue({ id: 'fake-created-file' });
    const response = await request(server.getApp())
      .post('/files')
      .set('Authorization', 'Bearer fake-token')
      .send({ sourceUrl: 'http://fake-url.com/file.png', fileName: 'fake-name.png' });
    expect(MockUploadService).toHaveBeenCalledWith({
      sourceUrl: new URL('http://fake-url.com/file.png'),
      fileName: 'fake-name.png',
      ownerEmail: OWNER_EMAIL,
    });
    expect(response.body).toStrictEqual({ id: 'fake-created-file' });
  });

  it('must return an error response when the service fails', async () => {
    mockUploadService.execute.mockImplementation(async () => { throw new Error('Whoops'); });
    const response = await request(server.getApp())
      .post('/files')
      .set('Authorization', 'Bearer fake-token')
      .send({ sourceUrl: 'http://fake-url.com/file.png', fileName: 'fake-name.png' });
    expect(response.body).toStrictEqual({ error: 'Whoops' });
  });

  it('must return an error response when the authenticated user email is not found', async () => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: {} });
    const response = await request(server.getApp())
      .post('/files')
      .set('Authorization', 'Bearer fake-token')
      .send({ sourceUrl: 'http://fake-url.com/file.png', fileName: 'fake-name.png' })
      .expect(400);
    expect(response.body).toStrictEqual({ error: 'Failed to find the user email in the request' });
  });

  it('must return an error response the source URL is not provided', async () => {
    const response = await request(server.getApp())
      .post('/files')
      .set('Authorization', 'Bearer fake-token')
      .send({ fileName: 'fake-name.png' });

    expect(response.body).toStrictEqual({
      errors: [
        {
          location: 'body',
          msg: 'Source URL is required and must be valid!',
          param: 'sourceUrl',
        },
      ],
    });
  });
});
