import request from 'supertest';
import ServerWithApp from './IServerWithApp';

describe('The delete file route', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const MockDeleteService = jest.fn();
  const mockDeleteService = { execute: jest.fn() };

  const mockJwtVerifier = { verifyAccessToken: jest.fn() };

  let server: ServerWithApp;

  beforeAll(() => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: { sub: OWNER_EMAIL } });
    jest.mock('@okta/jwt-verifier', () => jest.fn(() => mockJwtVerifier));

    jest.mock('../../../adapters/Logger');

    MockDeleteService.mockReturnValue(mockDeleteService);
    jest.mock('../../../services/files/DeleteFileService', () => MockDeleteService);

    return import('./TestableServer').then(({ default: TestableServer }) => {
      server = new TestableServer();
    });
  });

  it('must properly invoke the service', async () => {
    mockDeleteService.execute.mockImplementation(async () => ({ id: 'fake-url' }));
    await request(server.getApp())
      .delete('/files/123')
      .set('Authorization', 'Bearer fake-token')
      .expect(204);
    expect(MockDeleteService).toHaveBeenCalledWith({ fileId: 123, ownerEmail: OWNER_EMAIL });
  });

  it('must return an error response when the service fails', async () => {
    mockDeleteService.execute.mockImplementation(async () => { throw new Error('Whoops'); });
    const response = await request(server.getApp())
      .delete('/files/123')
      .set('Authorization', 'Bearer fake-token')
      .expect(500);
    expect(response.body).toStrictEqual({ error: 'Whoops' });
  });

  it('must return an error response when the user is not authenticated', async () => {
    const response = await request(server.getApp())
      .delete('/files/123')
      .expect(401);
    expect(response.body).toStrictEqual({ error: 'You are not authorized to access this resource' });
  });

  it('must return an error response when the user authentication is invalid', async () => {
    mockJwtVerifier.verifyAccessToken.mockRejectedValue(new Error('Whoops'));
    const response = await request(server.getApp())
      .delete('/files/123')
      .set('Authorization', 'Bearer fake-token')
      .expect(401);
    expect(response.body).toStrictEqual({ error: 'You are not authorized to access this resource' });
  });

  it('must return an error response when the authenticated user email is not found', async () => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: {} });
    const response = await request(server.getApp())
      .delete('/files/123')
      .set('Authorization', 'Bearer fake-token')
      .expect(400);
    expect(response.body).toStrictEqual({ error: 'Failed to find the user email in the request' });
  });
});
