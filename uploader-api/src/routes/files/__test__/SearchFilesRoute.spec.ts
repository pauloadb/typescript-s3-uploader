import request from 'supertest';
import ServerWithApp from './IServerWithApp';

describe('The search files route', () => {
  const OWNER_EMAIL = 'fake-user@fake-domain.fake';

  const MockSearchService = jest.fn();
  const mockSearchService = { execute: jest.fn() };

  const mockJwtVerifier = { verifyAccessToken: jest.fn() };

  let server: ServerWithApp;

  beforeAll(() => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: { sub: OWNER_EMAIL } });
    jest.mock('@okta/jwt-verifier', () => jest.fn(() => mockJwtVerifier));

    jest.mock('../../../adapters/Logger');

    MockSearchService.mockReturnValue(mockSearchService);
    jest.mock('../../../services/files/SearchFilesService', () => MockSearchService);

    return import('./TestableServer').then(({ default: TestableServer }) => {
      server = new TestableServer();
    });
  });

  it('must return the paginated search result when the service succeeds', async () => {
    mockSearchService.execute.mockImplementation(async () => ({ id: 'fake-search-result' }));
    const response = await request(server.getApp())
      .get('/files?pageNumber=2&pageSize=24&filter=fake&sortBy=description&sortOrder=desc&statuses[]=CREATED&statuses[]=DELETED')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(MockSearchService).toHaveBeenCalledWith({
      filter: 'fake',
      ownerEmail: 'fake-user@fake-domain.fake',
      pageNumber: 2,
      pageSize: 24,
      sortBy: 'description',
      sortOrder: 'desc',
      statuses: ['CREATED', 'DELETED'],
    });
    expect(response.body).toStrictEqual({ id: 'fake-search-result' });
  });

  it('must return an error response when the service fails', async () => {
    mockSearchService.execute.mockImplementation(async () => { throw new Error('Whoops'); });
    const response = await request(server.getApp())
      .get('/files')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(500);
    expect(MockSearchService).toHaveBeenCalledWith({
      ownerEmail: 'fake-user@fake-domain.fake',
      pageNumber: 1,
      pageSize: 25,
      sortBy: 'id',
      sortOrder: 'asc',
    });
    expect(response.body).toStrictEqual({ error: 'Whoops' });
  });

  it('must return an error response when the authenticated user email is not found', async () => {
    mockJwtVerifier.verifyAccessToken.mockResolvedValue({ claims: {} });
    const response = await request(server.getApp())
      .get('/files')
      .set('Authorization', 'Bearer fake-token')
      .expect('Content-Type', /json/)
      .expect(400);
    expect(response.body).toStrictEqual({ error: 'Failed to find the user email in the request' });
  });
});
