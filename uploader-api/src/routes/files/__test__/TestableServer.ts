import Server from '../../../Server';
import ServerWithApp from './IServerWithApp';

export default class TestableServer extends Server implements ServerWithApp {
  getApp(): Express.Application { return this.app; }
}
