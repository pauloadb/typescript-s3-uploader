import BasicRouter from '../common/BasicRouter';
import DeleteFileRoute from './DeleteFileRoute';
import DownloadFileRoute from './DownloadFileRoute';
import SearchFilesRoute from './SearchFilesRoute';
import UploadFileRoute from './UploadFileRoute';

class FilesRouter extends BasicRouter {
  constructor() {
    super('/files');
  }

  setupRoutes(): void {
    new UploadFileRoute(this.router).register();
    new DownloadFileRoute(this.router).register();
    new SearchFilesRoute(this.router).register();
    new DeleteFileRoute(this.router).register();
  }
}

export default FilesRouter;
