import { NextFunction, Response, Router } from 'express';
import ApiError from '../../ApiError';
import handleValidations from '../../middlewares/handlers/handleValidations';
import validateAuthentication from '../../middlewares/security/validateAuthentication';
import validateFileId from '../../middlewares/validators/validateFileId';
import DeleteFileService from '../../services/files/DeleteFileService';
import BasicRoute from '../common/BasicRoute';
import AuthenticatedRequest from '../common/IAuthenticatedRequest';

export default class DeleteFileRoute extends BasicRoute {
  constructor(router: Router) {
    super(router, '/:fileId');
  }

  register(): void {
    this.router.delete(
      this.path,
      validateAuthentication,
      validateFileId,
      handleValidations,
      (req: AuthenticatedRequest, res: Response, next: NextFunction): void => {
        const { fileId } = req.params;

        const ownerEmail = req.jwt?.sub;
        if (!ownerEmail) {
          next(new ApiError('Failed to find the user email in the request', { status: 400 }));
          return;
        }

        new DeleteFileService({ fileId: Number.parseInt(fileId, 10), ownerEmail }).execute()
          .then(() => { res.status(204).send(); })
          .catch((err) => { next(err); });
      },
    );
  }
}
