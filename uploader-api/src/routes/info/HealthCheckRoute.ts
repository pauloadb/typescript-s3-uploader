import { Request, Response, Router } from 'express';
import BasicRoute from '../common/BasicRoute';

class HealthCheckRoute extends BasicRoute {
  constructor(router: Router) {
    super(router, '/healthcheck');
  }

  register(): void {
    this.router.get(
      this.path,
      (_: Request, res: Response): void => {
        res.status(204).send();
      },
    );
  }
}

export default HealthCheckRoute;
