import request from 'supertest';

interface ServerWithApp {
  getApp(): Express.Application
}

describe('The healcheck route', () => {
  let server: ServerWithApp;

  beforeAll(() => {
    jest.mock('../../../adapters/Logger');
    return import('../../../Server').then((module) => {
      const ExpressServer = module.default;
      class TestableServer extends ExpressServer implements ServerWithApp {
        getApp(): Express.Application { return this.app; }
      }
      server = new TestableServer();
    });
  });

  it('must return successful response with empty body', async () => {
    await request(server.getApp()).get('/info/healthcheck').send().expect(204);
  });
});
