import BasicRouter from '../common/BasicRouter';
import HealthCheckRoute from './HealthCheckRoute';

class InfoRouter extends BasicRouter {
  constructor() {
    super('/info');
  }

  setupRoutes(): void {
    new HealthCheckRoute(this.router).register();
  }
}

export default InfoRouter;
