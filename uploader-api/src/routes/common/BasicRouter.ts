import express, { Router } from 'express';

abstract class BasicRouter {
  public readonly router: Router = express.Router();

  constructor(public readonly rootPath: string) {
    this.setupRoutes();
  }

  abstract setupRoutes(): void;

  register(app: express.Application): void {
    app.use(this.rootPath, this.router);
  }
}

export default BasicRouter;
