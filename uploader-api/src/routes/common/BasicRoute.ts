import { Router } from 'express';

abstract class BasicRoute {
  constructor(protected router: Router, protected path: string) {}
  abstract register(): void;
}

export default BasicRoute;
