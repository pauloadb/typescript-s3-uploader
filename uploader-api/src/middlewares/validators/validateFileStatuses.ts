import { check } from 'express-validator';
import { FileStatus } from 'uploader-datamodel';
import Middleware from '../IMiddleware';

const validateFileStatuses: Middleware[] = [
  check('statuses').optional().isArray().withMessage('File statuses parameter must be an array'),
  check('statuses.*').optional().isIn(Object.values(FileStatus)).withMessage(`File statuses must contain only: ${Object.values(FileStatus).toString()}`),
];

export default validateFileStatuses;
