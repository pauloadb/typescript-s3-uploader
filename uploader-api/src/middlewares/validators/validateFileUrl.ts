import { body } from 'express-validator';
import Middleware from '../IMiddleware';

const validateFileUrl: Middleware = body('sourceUrl')
  .isURL({
    require_valid_protocol: true,
    require_host: true,
    protocols: ['http', 'https'],
  })
  .withMessage('Source URL is required and must be valid!');

export default validateFileUrl;
