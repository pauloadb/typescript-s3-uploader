import { query } from 'express-validator';

const validatePagination = [
  query('pageNumber').default('1').isInt({ min: 1 })
    .withMessage('Page number must be a number greater than or equal to 1')
    .toInt(),
  query('pageSize').default('25').isInt({ min: 1, max: 100 })
    .withMessage('Page size must be a number between 1 and 100')
    .toInt(),
];

export default validatePagination;
