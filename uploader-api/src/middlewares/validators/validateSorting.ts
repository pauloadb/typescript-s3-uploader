import { query } from 'express-validator';
import Middleware from '../IMiddleware';

const validateSorting: Middleware[] = [
  query('sortBy').default('id').isAlphanumeric().withMessage('Sorting attribute must be alphanumeric'),
  query('sortOrder').default('asc').isIn(['asc', 'desc']).withMessage('Sort order must be "asc" or "desc"'),
];

export default validateSorting;
