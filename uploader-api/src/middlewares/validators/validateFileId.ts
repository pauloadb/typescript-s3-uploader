import { param } from 'express-validator';

const validateFileId = param('fileId').isInt({ min: 1 })
  .withMessage('File ID must be numeric!');

export default validateFileId;
