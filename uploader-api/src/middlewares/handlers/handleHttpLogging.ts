import { Request, Response } from 'express';
import morgan = require('morgan');
import Logger from '../../adapters/Logger';
import Configuration from '../../config/Configuration';
import Environment from '../../Environment';
import Middleware from '../IMiddleware';

const logger = Logger.getInstance();
const config = Configuration.getInstance();

const stream: morgan.StreamOptions = {
  write: (message: string) => logger.http(message),
};

const skip = () => config.environment === Environment.Development;

const handleHttpLogging: Middleware = morgan<Request, Response>(
  ':method :url :status :res[content-length] - :response-time ms',
  { stream, skip },
);

export default handleHttpLogging;
