import { NextFunction, Request, Response } from 'express';
import Logger from '../../adapters/Logger';

import ApiError from '../../ApiError';

const logger = Logger.getInstance();

const handleError = (
  err: ApiError | Error,
  _: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  __: NextFunction,
) : void => {
  logger.error(err);

  if (err instanceof ApiError) {
    res.status(err.status);
  } else {
    res.status(500);
  }

  res.send({ error: err.message });
};

export default handleError;
