import { NextFunction, Request, Response } from 'express';

interface Middleware {
  (req: Request, res: Response, next: NextFunction) : void
}

export default Middleware;
