import OktaJwtVerifier from '@okta/jwt-verifier';
import { Request } from 'express';
import { getError } from 'uploader-utils';
import ApiError from '../../ApiError';
import Configuration from '../../config/Configuration';
import Middleware from '../IMiddleware';

const oktaJwtVerifier = new OktaJwtVerifier({
  issuer: `https://${Configuration.getInstance().okta.domain}/oauth2/default`,
});

const audience = 'api://default';

const validateAuthentication: Middleware = (req: Request, _, next) => {
  const authHeader = req.headers.authorization || '';

  const match = authHeader.match(/Bearer (.+)/);
  if (!match) {
    next(new ApiError('You are not authorized to access this resource', { status: 401 }));
    return;
  }

  const accessToken = match[1];
  oktaJwtVerifier.verifyAccessToken(accessToken, audience).then((jwt) => {
    Object.assign(req, { jwt: { sub: jwt.claims.sub } });
    next();
  }).catch((err) => {
    next(new ApiError('You are not authorized to access this resource', { status: 401, cause: getError(err) }));
  });
};

export default validateAuthentication;
